-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2018 at 09:33 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `itss`
--

-- --------------------------------------------------------

--
-- Table structure for table `itss_asset_conditions`
--

CREATE TABLE `itss_asset_conditions` (
  `id` int(11) NOT NULL,
  `conditionName` varchar(191) NOT NULL,
  `conditionDescription` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itss_asset_conditions`
--

INSERT INTO `itss_asset_conditions` (`id`, `conditionName`, `conditionDescription`) VALUES
(1, 'BAGUS', 'BAGUS'),
(2, 'RUSAK', 'RUSAK'),
(3, 'SANGAT BAGUS', 'SANGAT BAGUS');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `itss_asset_conditions`
--
ALTER TABLE `itss_asset_conditions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `itss_asset_conditions`
--
ALTER TABLE `itss_asset_conditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
