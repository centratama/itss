-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2018 at 09:33 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `itss`
--

-- --------------------------------------------------------

--
-- Table structure for table `itss_asset_location`
--

CREATE TABLE `itss_asset_location` (
  `id` int(11) NOT NULL,
  `locationName` varchar(191) NOT NULL,
  `locationDescription` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itss_asset_location`
--

INSERT INTO `itss_asset_location` (`id`, `locationName`, `locationDescription`) VALUES
(1, 'LANTAI 16', 'LANTAI 16'),
(2, 'LANTAI 19', 'LANTAI 19'),
(3, 'GUDANG', 'GUDANG'),
(4, 'TEMPAT LAIN', 'TEMPAT LAIN');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `itss_asset_location`
--
ALTER TABLE `itss_asset_location`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `itss_asset_location`
--
ALTER TABLE `itss_asset_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
