-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2018 at 09:34 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `itss`
--

-- --------------------------------------------------------

--
-- Table structure for table `itss_asset_transaction`
--

CREATE TABLE `itss_asset_transaction` (
  `id` int(11) NOT NULL,
  `id_asset` int(11) NOT NULL,
  `id_employee` varchar(191) NOT NULL,
  `receiving_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `givingBack_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `receiving_condition` int(11) NOT NULL,
  `givingBack_condition` int(11) NOT NULL,
  `description` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itss_asset_transaction`
--

INSERT INTO `itss_asset_transaction` (`id`, `id_asset`, `id_employee`, `receiving_date`, `givingBack_date`, `receiving_condition`, `givingBack_condition`, `description`) VALUES
(1, 1, 'CG000264', '2018-07-17 17:00:00', '2018-07-16 17:00:00', 1, 1, 'fuftiiupo;jlhcf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `itss_asset_transaction`
--
ALTER TABLE `itss_asset_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_asset` (`id_asset`),
  ADD KEY `id_employee` (`id_employee`),
  ADD KEY `description` (`description`),
  ADD KEY `givingBack_condition` (`givingBack_condition`),
  ADD KEY `receiving_condition` (`receiving_condition`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `itss_asset_transaction`
--
ALTER TABLE `itss_asset_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
