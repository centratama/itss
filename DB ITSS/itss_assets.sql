-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2018 at 09:33 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `itss`
--

-- --------------------------------------------------------

--
-- Table structure for table `itss_assets`
--

CREATE TABLE `itss_assets` (
  `idAsset` varchar(191) NOT NULL,
  `id_parent` varchar(191) DEFAULT NULL,
  `name` varchar(191) NOT NULL,
  `po_numbers` int(11) NOT NULL,
  `serial_number` varchar(191) NOT NULL,
  `prices` int(11) NOT NULL,
  `photo_asset` varchar(191) DEFAULT NULL,
  `descriptions` varchar(191) NOT NULL,
  `id_brands` int(11) NOT NULL,
  `id_vendors` int(11) NOT NULL,
  `id_types` int(11) NOT NULL,
  `id_conditions` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `id_locations` int(11) NOT NULL,
  `barcode` varchar(191) DEFAULT NULL,
  `id_company` varchar(191) NOT NULL,
  `buyingDates` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itss_assets`
--

INSERT INTO `itss_assets` (`idAsset`, `id_parent`, `name`, `po_numbers`, `serial_number`, `prices`, `photo_asset`, `descriptions`, `id_brands`, `id_vendors`, `id_types`, `id_conditions`, `id_status`, `id_locations`, `barcode`, `id_company`, `buyingDates`, `created_at`, `updated_at`) VALUES
('LAPTOPs-00001', NULL, 'gkhkh', 5757, '4656', 65675, 'planeta_kosmos_more_gorizont_103567_1920x1080.jpg', 'yutytgffyfuyf', 2, 1, 1, 1, 1, 1, NULL, '', '2018-07-30', '2018-07-29 22:22:56', '2018-07-29 22:22:56'),
('MAC-COMPUTER-00003', 'LAPTOPs-00001', 'KOMPUTER', 4326432, '473827423', 62347238, 'planeta_kosmos_more_gorizont_103567_1920x1080.jpg', 'EWRIWUEIRUWE', 1, 1, 2, 1, 1, 1, NULL, '{294EDB67-6819-43A6-962B-6C77997BF88D}', '2018-07-11', '2018-07-30 02:37:15', '2018-07-30 02:37:15'),
('MAC-LAPTOPs-00002', '', 'LAPTOP ASUS FFFF', 54457, '3453543', 57348, 'sunset_sea_rings_planet_90890_1920x1080.jpg', 'dfnklsjklfsdg', 1, 1, 1, 1, 1, 2, NULL, '{294EDB67-6819-43A6-962B-6C77997BF88D}', '2018-07-18', '2018-07-30 01:21:05', '2018-07-30 01:21:05'),
('MAC-LAPTOPs-00003', NULL, 'ACER', 7389573, '45676', 8744344, 'sunset_sea_rings_planet_90890_1920x1080.jpg', 'HGHHHJLIJKJKKLHL', 2, 1, 1, 1, 1, 1, NULL, '{294EDB67-6819-43A6-962B-6C77997BF88D}', '2018-07-18', '2018-07-30 03:50:50', '2018-07-30 03:50:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `itss_assets`
--
ALTER TABLE `itss_assets`
  ADD PRIMARY KEY (`idAsset`),
  ADD KEY `id_brands` (`id_brands`),
  ADD KEY `id_vendors` (`id_vendors`),
  ADD KEY `id_conditions` (`id_conditions`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_locations` (`id_locations`),
  ADD KEY `id_parent` (`id_parent`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
