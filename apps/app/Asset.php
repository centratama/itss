<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\CompanyModel;

class Asset extends Model
{
    //idStatus 1 = BORROW
    //idStatus 2 = RETURN
    //idStatus 3 = DISPOSAL
    protected $table="itss_assets";

    //getting all data
    public function getAllAsset()
    {
        //$data = Asset::where('id_status', 1)->get();
        $data = Asset::join('itss_asset_brands', 'id_brands', '=', 'itss_asset_brands.id')
        ->join('itss_asset_vendors', 'id_vendors' , '=', 'itss_asset_vendors.id')
        ->join('itss_asset_types', 'id_types' , '=', 'itss_asset_types.id')
        ->join('itss_asset_conditions', 'id_conditions' , '=', 'itss_asset_conditions.id')
        ->join('itss_asset_location', 'id_locations' , '=', 'itss_asset_location.id')
        ->select('itss_assets.*', 'itss_asset_brands.*', 'itss_asset_vendors.*', 'itss_asset_types.*', 'itss_asset_conditions.*', 'itss_asset_location.*')
        ->orderby('itss_assets.name', 'asc')
        ->orderby('itss_asset_brands.brandName', 'asc')
        ->orderby('itss_assets.idAsset', 'asc')
        ->orderby('itss_assets.serial_number', 'asc')
        ->get();
    	return $data;
    }

    public function getAllAsset11($idAsset)
    {
        //$data = Asset::where('id_status', 1)->get();
        $data = Asset::join('itss_asset_brands', 'id_brands', '=', 'itss_asset_brands.id')
        ->join('itss_asset_vendors', 'id_vendors' , '=', 'itss_asset_vendors.id')
        ->join('itss_asset_types', 'id_types' , '=', 'itss_asset_types.id')
        ->join('itss_asset_conditions', 'id_conditions' , '=', 'itss_asset_conditions.id')
        ->join('itss_asset_location', 'id_locations' , '=', 'itss_asset_location.id')
        ->select('itss_assets.*', 'itss_asset_brands.*', 'itss_asset_vendors.*', 'itss_asset_types.*', 'itss_asset_conditions.*', 'itss_asset_location.*')
        ->where('id_parent','=', NULL)
        ->where('idAsset', '!=', $idAsset)
        ->orderby('itss_assets.name', 'asc')
        ->orderby('itss_asset_brands.brandName', 'asc')
        ->orderby('itss_assets.idAsset', 'asc')
        ->orderby('itss_assets.serial_number', 'asc')
        ->get();
        return $data;
    }

    public function allAssetWhereParentNullAndNotThisIdTypeCreate($idType)
    {
        $data = Asset::join('itss_asset_brands', 'id_brands', '=', 'itss_asset_brands.id')
        ->join('itss_asset_vendors', 'id_vendors' , '=', 'itss_asset_vendors.id')
        ->join('itss_asset_types', 'id_types' , '=', 'itss_asset_types.id')
        ->join('itss_asset_conditions', 'id_conditions' , '=', 'itss_asset_conditions.id')
        ->join('itss_asset_location', 'id_locations' , '=', 'itss_asset_location.id')
        ->select('itss_assets.*', 'itss_asset_brands.*', 'itss_asset_vendors.*', 'itss_asset_types.*', 'itss_asset_conditions.*', 'itss_asset_location.*')
        ->where('id_parent','=', NULL)
        ->where("id_types",$idType)
        ->orderby('itss_assets.name', 'asc')
        ->orderby('itss_asset_brands.brandName', 'asc')
        ->orderby('itss_assets.idAsset', 'asc')
        ->orderby('itss_assets.serial_number', 'asc')
        ->pluck("idAsset", "name");
        return $data;
    }

    public function getAllAsset1()
    {
        $data = Asset::orderBy('created_at', 'asc')->get();
        return $data;
    }

    public function getAllAssetAvailable()
    {
        $data = Asset::join('itss_asset_brands', 'id_brands', '=', 'id')
        ->select('itss_assets.*', 'itss_asset_brands.*')->where('itss_assets.id_status', 1)->orderby('itss_assets.name', 'asc')->orderby('itss_asset_brands.brandName', 'asc')->orderby('itss_assets.idAsset', 'asc')->get();

        return $data;
    }

    public function getAllAssetActive()
    {
        $data = Asset::join('itss_asset_brands', 'id_brands', '=', 'id')
        ->select('itss_assets.*', 'itss_asset_brands.*')->where('itss_assets.id_status', 2)->orderby('itss_assets.name', 'asc')->orderby('itss_asset_brands.brandName', 'asc')->orderby('itss_assets.idAsset', 'asc')->get();

        return $data;
    }

    //getting all data where 
    public function getAllAssetNull($idAsset)
    {
        $data = Asset::where('id_parent','=', NULL)->where('idAsset', '!=', $idAsset)->get();
        return $data;
    }

    //GET ALL DATA WHERE
    public function getAllAssetWhere($idAsset)
    {
        $data = Asset::where('idAsset', $idAsset)->get();
        return $data;
    }

    public function getAllJoinAssets()
    {
        $sql = "SELECT  itss_assets.*, 
                        itss_asset_status.*,
                        itss_asset_vendors.*, 
                        itss_asset_brands.*, 
                        itss_asset_conditions.*, 
                        itss_asset_types.*

                FROM itss_assets
                    JOIN itss_asset_status
                        ON itss_asset_status.id = itss_assets.id_status
                    JOIN itss_asset_vendors
                        ON itss_asset_vendors.id = itss_assets.id_vendors
                    JOIN itss_asset_brands
                        ON itss_asset_brands.id = itss_assets.id_brands
                    JOIN itss_asset_conditions
                        ON itss_asset_conditions.id = itss_assets.id_conditions
                    JOIN itss_asset_types
                        ON itss_asset_types.id = itss_assets.id_types";
        $data = DB::SELECT($sql);
        return $data;
    }

    public function saveDataAssets($data, $type)
    {
        $assetName = $data[0]['assetName'];
        $idParent = $data[0]['idParent'];
        $idType = $data[0]['assetType'];
        $poNumber = $data[0]['poNumber'];
        $assetPrice = $data[0]['assetPrice'];
        $idVendor = $data[0]['assetVendor'];
        $idBrand = $data[0]['assetBrand'];
        $serialNumber = $data[0]['serialNumber'];
        $idConditions = $data[0]['assetCondition'];
        $idStatus = $data[0]['assetStatus'];
        $descriptions = $data[0]['assetDescription'];
        $idLocations = $data[0]['assetLocation'];
        $idCompany = $data[0]['company'];
        $macAdress = $data[0]['assetMacAdress'];
        $buyingDates = date('Y-m-d', strtotime($data[0]['assetBuyingDate']));
        $guaranteeDates = date('Y-m-d', strtotime($data[0]['assetGuaranteeDate']));

        $lastId = Asset::where('id_types', $idType)->where('id_company', $idCompany)->orderBy('created_at', 'desc')->first();
        //dd($lastId);
        //$lastId = $lastId->idAsset;

        $companyName = CompanyModel::where('id', $idCompany)->get();
        $companyName = $companyName[0]['name'];
        if($companyName == "PT MAC Sarana Djaya")
            {
                $companyCode = "MAC";
            }
            else if( $companyName == "PT Network Quality Indonesia")
            {
                $companyCode = "NQI";
            }
            else if( $companyName == "Centratama Group")
            {
                $companyCode = "CG";
            }

            else if( $companyName == "PT Centratama Telekomunikasi Indonesia, Tbk")
            {
                $companyCode = "CTI";
            }

            else if( $companyName == "PT Fastel Sarana Indonesia")
            {
                $companyCode = "FSI";
            }

            else if( $companyName == "PT Centratama Menara Indonesia")
            {
                $companyCode = "CMI";
            }


        if(!empty($lastId))
        {
            $lastId = $lastId->idAsset;
            
            $id = substr($lastId, -5);
            $id = (int)$id;
            $id = $id + 1;
            $id = str_pad($id, 5, '0', STR_PAD_LEFT);

            $idAsset = $companyCode."-".$type."-".$id;

        }

        else
        {
            $id = 1;
            $id = str_pad($id, 5, '0', STR_PAD_LEFT);

            $idAsset = $companyCode."-".$type."-".$id;

            //echo $idAsset;   
        }


        $asset = new Asset;

        $asset->idAsset = $idAsset;
        $asset->name = $assetName;
        $asset->id_parent = $idParent;
        $asset->po_numbers = $poNumber;
        $asset->serial_number = $serialNumber;
        $asset->prices = str_replace(".", "", $assetPrice);
        $asset->descriptions = $descriptions;
        $asset->id_brands = $idBrand;
        $asset->id_vendors = $idVendor;
        $asset->id_conditions = $idConditions;
        $asset->id_status = $idStatus;
        $asset->id_locations = $idLocations;
        $asset->id_types = $idType;
        $asset->id_company = $idCompany;
        $asset->buyingDates = $buyingDates;
        $asset->guarantee_date = $guaranteeDates;
        $asset->photo_asset = $idAsset.'.jpg';
        $asset->file_html = $idAsset.'.pdf';
        $asset->macAdress = $macAdress;

        $asset->save();

        return $idAsset;
    }

    public function updateDataAssets($data, $filename, $filenameBerlac)
    {
        //dd($data);
        $idAsset = $data[0]['idAsset'];
        $idParent = $data[0]['idParent'];
        $assetName = $data[0]['assetName'];
        $idType = $data[0]['assetType'];
        $poNumber = $data[0]['poNumber'];
        $assetPrice = $data[0]['assetPrice'];
        $idVendor = $data[0]['assetVendor'];
        $idBrand = $data[0]['assetBrand'];
        $serialNumber = $data[0]['serialNumber'];
        $idConditions = $data[0]['assetCondition'];
        $idStatus = $data[0]['assetStatus'];
        $descriptions = $data[0]['assetDescription'];
        $idLocations = $data[0]['assetLocation'];
        $macAdress = $data[0]['assetMacAdress'];

        $buyingDates = date('Y-m-d', strtotime($data[0]['assetBuyingDate']));
        if( (empty($filename)  OR $filename ==0) AND (empty($filenameBerlac)  OR $filenameBerlac ==0) )
        {
            $data = Asset::where('idAsset', $idAsset)->update([
                'name' => $assetName,
                'id_parent' => $idParent,
                'po_numbers' => $poNumber,
                'serial_number' => $serialNumber,
                'prices' => str_replace(".", "", $assetPrice),
                'descriptions' => $descriptions,
                'id_status' => $idStatus,
                'id_locations' => $idLocations,
                'id_types' => $idType,
                'buyingDates' => $buyingDates,
                'macAdress' => $macAdress,
            ]);
        }

        if( (!empty($filename)  OR $filename !=0) AND (!empty($filenameBerlac)  OR $filenameBerlac !=0) )
        {
            $data = Asset::where('idAsset', $idAsset)->update([
                'name' => $assetName,
                'id_parent' => $idParent,
                'po_numbers' => $poNumber,
                'serial_number' => $serialNumber,
                'prices' => str_replace(".", "", $assetPrice),
                'descriptions' => $descriptions,
                'id_status' => $idStatus,
                'id_locations' => $idLocations,
                'id_types' => $idType,
                'buyingDates' => $buyingDates,
                'file_html' => $idAsset.'.pdf',
                'photo_asset' => $idAsset.'.jpg',
                'macAdress' => $macAdress,
            ]);
        }

        if( !empty($filename)  OR $filename != 0 )
        {
            $data = Asset::where('idAsset', $idAsset)->update([
                'name' => $assetName,
                'id_parent' => $idParent,
                'po_numbers' => $poNumber,
                'serial_number' => $serialNumber,
                'prices' => str_replace(".", "", $assetPrice),
                'descriptions' => $descriptions,
                'id_status' => $idStatus,
                'id_locations' => $idLocations,
                'id_types' => $idType,
                'buyingDates' => $buyingDates,
                'photo_asset' => $idAsset.'.jpg',
                'macAdress' => $macAdress,
            ]);
        }

        if( !empty($filenameBerlac) OR $filenameBerlac != 0)
        {
            $data = Asset::where('idAsset', $idAsset)->update([
                'name' => $assetName,
                'id_parent' => $idParent,
                'po_numbers' => $poNumber,
                'serial_number' => $serialNumber,
                'prices' => str_replace(".", "", $assetPrice),
                'descriptions' => $descriptions,
                'id_status' => $idStatus,
                'id_locations' => $idLocations,
                'id_types' => $idType,
                'buyingDates' => $buyingDates,
                'file_html' => $idAsset.'.pdf',
                'macAdress' => $macAdress,
            ]);

        }

        return $data;
    }

    public function deleteAssetChild($idAsset)
    {

        $sql = "UPDATE itss_assets SET id_parent= NULL WHERE idAsset='{$idAsset}';";

        $data = DB::SELECT($sql);

        $asset = $sql = Asset::where('idAsset', $idAsset)->update([
                'id_status'            => 1,
            ]);

        return $asset;
    }

}
