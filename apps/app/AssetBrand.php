<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AssetBrand extends Model
{
    //
    //protected $table="itss_asset_brands";
    public function getAllAssetWhereBrand($idBrand)
    {
        //$data = Asset::all();
        $data = Asset::where('id_brands', $idBrand)->paginate(100);
        //$sql = "SELECT * FROM itss_assets";
        //$data = DB::SELECT($sql);
        return $data;
    }

    public function getAssetBrand()
    {
    	$sql = "SELECT * FROM itss_asset_brands";

    	$data = DB::SELECT($sql);

    	return $data;
    }

    public function getAssetBrandWhere($idBrand)
    {
        $sql = "SELECT * FROM itss_asset_brands WHERE id='{$idBrand}'";

        $data = DB::SELECT($sql);

        return $data;
    }

    public function updateDataBrand($data, $idBrand)
    {
        $brandName = $data[0]['brandName'];
        $brandDescription = $data[0]['brandDescription'];

        $sql = "UPDATE itss_asset_brands SET brandName='{$brandName}', brandDescription='{$brandDescription}' 
                WHERE id='{$idBrand}';";

        $data = DB::SELECT($sql);

        return $data;
    }

    public function saveDataBrand($data)
    {
        $brandName = $data[0]['brandName'];
        $brandDescription = $data[0]['brandDescription'];

        $sql = "INSERT INTO itss_asset_brands(brandName, brandDescription)
                VALUES
                ('{$brandName}', '{$brandDescription}');";

        $data = DB::SELECT($sql);

        return $data;
    }
}
