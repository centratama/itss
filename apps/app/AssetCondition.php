<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AssetCondition extends Model
{
    //
    protected $table = "itss_asset_conditions";

    public function getAssetCondition()
    {
    	$sql = "SELECT * FROM itss_asset_conditions";

    	$data = DB::SELECT($sql);

    	return $data;
    }

    //getting all data in itss_asset_status where $idStatus
    public function getAssetConditionWhere($idCondition)
    {
        $sql = "SELECT * FROM itss_asset_conditions WHERE id='{$idCondition}' ";

        $data = DB::SELECT($sql);

        return $data;
    }

    public function updateDataCondition($data, $idCondition)
    {
        //dd($data);
        $conditionName = $data[0]['conditionName'];
        $conditionStatus = $data[0]['status'];
        $conditionDescription = $data[0]['conditionDescription'];

        /*$sql = "UPDATE itss_asset_conditions SET conditionName='{$conditonName}', conditionDescription='{$conditionDescription}' 
                WHERE id='{$idCondition}';";

        $data = DB::SELECT($sql);*/

        $condition = AssetCondition::where('id', $idCondition)->update([
            "conditionName" => $conditionName,
            "conditionStatus" => $conditionStatus,
            "conditionDescription" => $conditionDescription,
        ]);

        return $condition;
    }

    public function saveDataCondition($data)
    {
        $conditionName = $data[0]['conditionName'];
        $conditionStatus = $data[0]['status'];
        $conditionDescription = $data[0]['conditionDescription'];

        $condition = new AssetCondition;

        $condition->conditionName = $conditionName;
        $condition->conditionStatus = $conditionStatus;
        $condition->conditionDescription = $conditionDescription;

        $condition->save();

        /*$sql = "INSERT INTO itss_asset_conditions(conditionName, conditionDescription)
                VALUES
                ('{$conditionName}', '{$conditionDescription}');";

        $data = DB::SELECT($sql);*/

        return $condition;
    }


}
