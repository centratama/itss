<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Asset;

class AssetEmployeeTransaction extends Model
{
    protected $table = "itss_asset_employeetransaction";
    //public $incrementing = false;

    public function getAllTransaction()
    {
        $sql = "SELECT  itss_asset_employeetransaction.*,
        itss_assets.*,
        itss_asset_conditions.*,
        itss_asset_status.*
        

        FROM itss_asset_employeetransaction
        inner JOIN itss_assets
        ON itss_assets.idAsset = itss_asset_employeetransaction.id_asset_et
        inner JOIN itss_asset_conditions
        ON itss_asset_conditions.id = itss_asset_employeetransaction.receiving_condition_et
        inner JOIN itss_asset_status
        ON itss_asset_status.id = itss_asset_employeetransaction.id_status_et
        WHERE itss_asset_employeetransaction.returningStatus=0";

        $data = DB::SELECT($sql);
        //dd($data);
        return $data;
        //data = AssetEmployeeTransaction::all();
        //return $data;
    }

    public function getAllTransactionWhere($idTransaction)
    {
        $data = AssetEmployeeTransaction::where('idTransaction_et', $idTransaction)->get();
        return $data;
    }

    public function getAllTransactionWhere01($idTransaction)
    {
        $data = AssetEmployeeTransaction::where('id_et', $idTransaction)->get();
        return $data;
    }

    public function getTransactionOfThisMonth()
    {
        $thismonth  = date('m');
        $thisYear   = date('Y');
        $record = AssetEmployeeTransaction::whereMonth('created_at', '=', date('m'))
        ->whereYear('created_at', '=', date('Y'))
        ->orderBy('created_at', 'desc')
        ->count();

        //dd($record);

        return $record;
    }

    public function saveAddTransactionEmployee($data, $idIFU=null)
    {
       // dd($data);
        $transactionRecord = $this->getTransactionOfThisMonth();
        if($transactionRecord == 0)
        {
            $number = 1;
        }

        else
        {
            $number = $transactionRecord + 1;
        }

        if($idIFU != null)
        {
            $idTransaction = $idIFU;
        }

        else
        {
            $idTransaction = $data[0]['idTransaction'];
        }

        
        $idAsset = $data[0]['idAsset'];
        $nikEmployee = $data[0]['employee'];
        $id_requester = $data[0]['requester'];
        $receivingDate = date('Y-m-d', strtotime($data[0]['receivingDate']));
        $givingBackDate = NULL;
        $receivingCondition = $data[0]['receivingCondition'];
        $givingBackCondition = NULL;
        $description = $data[0]['assetDescription'];
        $idLocation = $data[0]['assetLocation'];
        $idStatus = $data[0]['assetStatus'];

        $transaction = new AssetEmployeeTransaction;
        $transaction->returningStatus = 0;
        $transaction->id_et = $idTransaction."-".$number;
        $transaction->idTransaction_et = $idTransaction;
        $transaction->lineNumber_et = $number;
        $transaction->id_asset_et = $idAsset;
        $transaction->id_employee_et = $nikEmployee;
        $transaction->id_requester_et = $id_requester;
        $transaction->receiving_date_et = $receivingDate;
        $transaction->receiving_condition_et = $receivingCondition;
        $transaction->description_et = $description;
        $transaction->id_status_et = $idStatus;
        $transaction->id_location_et = $idLocation;

        $transaction->save();

        $asset = Asset::where('idAsset', $idAsset)->update(['id_status' => 2]);

        return $transaction;
    }

    public function saveAddTransaction1($data)
    {
        $transactionRecord = $this->getTransactionOfThisMonth();
        if($transactionRecord == 0)
        {
            $number = 1;
        }

        else
        {
            $number = $transactionRecord + 1;
        }

        $idAsset = $data[0]['idAsset'];
        $idParent = $data[0]['idParent'];
        $receivingCondition = $data[0]['receivingCondition'];
        $idStatus = $data[0]['assetStatus'];
        //echo $idParent;

        $sql = "UPDATE itss_assets SET id_parent='{$idParent}' WHERE idAsset='{$idAsset}';";
        $data = DB::SELECT($sql);


        $parentAsset = AssetEmployeeTransaction::where('id_asset_et', $idParent)->get();
        //dd($parentAsset);
        $idTransaction = $parentAsset[0]['idTransaction_et'];
        $nikEmployee = $parentAsset[0]['id_employee_et'];
        $id_requester = $parentAsset[0]['id_requester_et'];
        $receivingDate = date('Y-m-d');
        $givingBackDate = NULL;
        $givingBackCondition = NULL;
        $description = $parentAsset[0]['description_et'];
        $idLocation = $parentAsset[0]['id_location_et'];

        $transaction = new AssetEmployeeTransaction;
        $transaction->lineNumber_et = $number;
        $transaction->id_et = $idTransaction."-".$number;
        $transaction->idTransaction_et = $idTransaction;
        $transaction->id_asset_et = $idAsset;
        $transaction->id_parent_et = $idParent;
        $transaction->id_employee_et = $nikEmployee;
        $transaction->id_requester_et = $id_requester;
        $transaction->receiving_date_et = $receivingDate;
        $transaction->receiving_condition_et = $receivingCondition;
        $transaction->description_et = $description;
        $transaction->id_status_et = $idStatus;
        $transaction->id_location_et = $idLocation;

        $transaction->save();

        $asset = Asset::where('idAsset', $idAsset)->update(['id_status' => 2]);

        return $transaction;
    }

    //GET ALL DATA JOIN
    public function getAllHistory($idAsset)
    {
        $sql = "SELECT  itss_asset_employeetransaction.*,
        itss_assets.*,
        itss_asset_conditions.*,
        itss_asset_status.*
        

        FROM itss_asset_employeetransaction
        inner JOIN itss_assets
        ON itss_assets.idAsset = itss_asset_employeetransaction.id_asset_et
        inner JOIN itss_asset_conditions
        ON itss_asset_conditions.id = itss_asset_employeetransaction.receiving_condition_et
        inner JOIN itss_asset_status
        ON itss_asset_status.id = itss_asset_employeetransaction.id_status_et
        WHERE itss_asset_employeetransaction.id_asset_et='{$idAsset}'";

        $data = DB::SELECT($sql);
        //dd($data);
        return $data;
    }

    //RETURNING ASSET
    public function returningAsset($data)
    {
        $idTransaction = $data[0]['idTransaction'];
        $idAsset = $data[0]['idAsset'];
        $givingBackDate = $data[0]['givingBackDate'];
        $givingBackCondition = $data[0]['givingBackCondition'];
        $transaction = AssetEmployeeTransaction::where('id_et', $idTransaction)->update([
            'returningStatus' => 1,
            'givingBack_date_et' => date('Y-m-d', strtotime($givingBackDate)),
            'givingBack_condition_et' => $givingBackCondition,
        ]);
        $asset = Asset::where('idAsset', $idAsset)->update(['id_status' => 1]);

        return $transaction;
    }

}
