<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AssetLocation extends Model
{
    //
    public function getAssetLocation()
    {
    	$sql = "SELECT * FROM itss_asset_location";

    	$data = DB::SELECT($sql);

    	return $data;
    }

    public function getAssetLocationWhere($idLocation)
    {
        $sql = "SELECT * FROM itss_asset_location WHERE id='{$idLocation}'";

        $data = DB::SELECT($sql);

        return $data;
    }

    public function updateDataLocation($data, $idLocation)
    {
        $locationName = $data[0]['locationName'];
        $locationDescription = $data[0]['locationDescription'];

        $sql = "UPDATE itss_asset_location SET locationName='{$locationName}', locationDescription='{$locationDescription}' 
                WHERE id='{$idLocation}';";

        $data = DB::SELECT($sql);

        return $data;
    }

    public function saveDataLocation($data)
    {
        $locationName = $data[0]['locationName'];
        $locationDescription = $data[0]['locationDescription'];

        $sql = "INSERT INTO itss_asset_location(locationName, locationDescription)
                VALUES
                ('{$locationName}', '{$locationDescription}');";

        $data = DB::SELECT($sql);

        return $data;
    }

}
