<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AssetStatus extends Model
{
    //getting all data in itss_asset_status
    public function getAssetStatus()
    {
    	$sql = "SELECT * FROM itss_asset_status";

    	$data = DB::SELECT($sql);

    	return $data;
    }

    //getting all data in itss_asset_status where $idStatus
    public function getAssetStatusWhere($idStatus)
    {
        $sql = "SELECT * FROM itss_asset_status WHERE id='{$idStatus}' ";

        $data = DB::SELECT($sql);

        return $data;
    }

    public function updateDataStatus($data, $idStatus)
    {
        $statusName = $data[0]['statusName'];
        $status = $data[0]['status'];
        $statusDescription = $data[0]['statusDescription'];

        $sql = "UPDATE itss_asset_status SET statusName='{$statusName}', statusDescription='{$statusDescription}', status='{$status}'
                WHERE id='{$idStatus}';";

        $data = DB::SELECT($sql);

        return $data;
    }

    public function saveDataStatus($data)
    {
        $statusName = $data[0]['statusName'];
        $status = $data[0]['status'];
        $statusDescription = $data[0]['statusDescription'];

        $sql = "INSERT INTO itss_asset_status(statusName, statusDescription, status)
                VALUES
                ('{$statusName}', '{$statusDescription}', '{$status}');";

        $data = DB::SELECT($sql);

        return $data;
    }
}
