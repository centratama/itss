<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Asset;

class AssetTransaction extends Model
{
    public $incrementing = false;
    protected $table = "itss_asset_transaction";
    //
    public function getAllTransaction()
    {
        $data = AssetTransaction::all();
    	//$sql = "SELECT * FROM itss_asset_transaction";
    	//$data = DB::SELECT($sql);
        //dd($data);
        return $data;
    }

    /*public function getAllTransactionJoin()
    {
        $data = DB::table('itss_asset_transaction')
                ->join('itss_asset_employeetransaction', 'itss_asset_transaction.id', '=', 'itss_asset_employeetransaction.idTransaction_et')
                ->select('itss_asset_transaction.*', 'itss_asset_employeetransaction.*')->get();
                //dd($data);

        //echo $data[0]['idTransaction'];
        return $data;
    }*/

    public function getTransactionWhere($idTransaction){
        $data = AssetTransaction::where('id', $idTransaction)->get();
        //$sql = "SELECT * FROM itss_asset_transaction WHERE id='{$idTransaction}'";
        //$data = DB::SELECT($sql);
        return $data;
    }

    public function getTransactionWhereMonth($idTransaction)
    {

        $data = $this->getTransactionWhere($idTransaction);
        $date = strtotime($data[0]['receiving_date']);
        //echo "date ".$date;
        $month = date('m', $date);
        //echo "month ".$month;
        $year = date('Y', $date);
        //echo "year ".$year;
        //dd($data);
        $data = AssetTransaction::whereYear('receiving_date', $year)
        ->whereMonth('receiving_date', $month)
        ->orderBy('receiving_date')
        ->get();
        //$sql = "SELECT * FROM itss_asset_transaction WHERE id='{$idTransaction}'";

        //$data = DB::SELECT($sql);

        return $data;
    }

    public function getTransactionOfThisMonth()
    {
        $thismonth  = date('m');
        $thisYear   = date('Y');
        $record = AssetTransaction::whereMonth('created_at', '=', date('m'))
        ->whereYear('created_at', '=', date('Y'))
        ->orderBy('created_at', 'desc')
        ->count();

        //dd($record);

        return $record;
    }

    //GET ALL DATA JOIN
    public function getAllJoinTransaction()
    {
        $transaction = AssetTransaction::where('id_parent', '0')->where('givingBack_date', NULL)->get();

        //dd($transaction);

        return $transaction;
    }

    //GET ALL DATA JOIN
    public function getAllHistory($idAsset)
    {
        $sql = "SELECT  itss_assets.*,
        itss_asset_conditions.*, 
        itss_asset_transaction.*

        FROM itss_asset_transaction
        LEFT JOIN itss_assets
        ON itss_assets.idAsset = itss_asset_transaction.id_asset
        LEFT JOIN itss_asset_conditions
        ON itss_asset_conditions.id = itss_assets.id_conditions
        WHERE id_asset='{$idAsset}'";

        $data = DB::SELECT($sql);
        return $data;
    }

    public function saveDataTransaction($data)
    {
        $idAsset = $data[0]['idAsset'];
        $nikEmployee = $data[0]['employee'];
        $id_requester = $data[0]['requester'];
        $receivingDate = date('Y-m-d', strtotime($data[0]['receivingDate']));
        $givingBackDate = NULL;
        $receivingCondition = $data[0]['receivingCondition'];
        $givingBackCondition = NULL;
        $description = $data[0]['assetDescription'];
        $idLocation = $data[0]['assetLocation'];
        $idStatus = $data[0]['assetStatus'];

        $transactionRecord = $this->getTransactionOfThisMonth();
        if($transactionRecord == 0)
        {
            $number = 1;
            $number = str_pad($number, 5, '0', STR_PAD_LEFT);
            $thisMonth = date('m');    
            $thisYear = date('Y');
            $thisYear = substr($thisYear, -2);
            $companyCode = substr($idAsset, 0, strpos($idAsset, '-')); 
        }

        else
        {
            $number = $transactionRecord + 1;
            $number = str_pad($number, 5, '0', STR_PAD_LEFT);
            $thisMonth = date('m');          
            $thisYear = date('Y');
            $thisYear = substr($thisYear, -2);
            $companyCode = substr($idAsset, 0, strpos($idAsset, '-')); 
        }

        $idIFU = $companyCode.".PPABM ".$thisMonth.".".$thisYear.".".$number;

        $transaction = new AssetTransaction;

        $transaction->id = $idIFU;
        $transaction->id_asset = $idAsset;
        $transaction->id_employee = $nikEmployee;
        $transaction->id_requester = $id_requester;
        $transaction->receiving_date = $receivingDate;
        $transaction->receiving_condition = $receivingCondition;
        $transaction->description = $description;
        $transaction->id_status = $idStatus;
        $transaction->id_location = $idLocation;

        $transaction->save();

        $asset = Asset::where('idAsset', $idAsset)->update(['id_status' => 2]);

        //return $transaction;
        return $idIFU;
    }

    public function updateDataTransaction($data)
    {
        //dd($data);
        $idAsset = $data[0]['idAsset'];
        $idTransaction = $data[0]['idTransaction'];
        $nikEmployee = $data[0]['employee'];
        $nikRequester = $data[0]['requester'];
        $receivingDate = date('Y-m-d', strtotime($data[0]['receivingDate']));
        $givingBackDate = date('Y-m-d', strtotime($data[0]['givingBackDate']));
        $receivingCondition = $data[0]['receivingCondition'];
        $givingBackCondition = $data[0]['givingBackCondition'];
        $description = $data[0]['transactionDescription'];
        $idStatus = $data[0]['transactionStatus'];

        $transaction = AssetTransaction::where('id', $idTransaction)->get();

        if(!empty($givingBackCondition) AND !empty($givingBackDate))
        {
            $sql = AssetTransaction::where('id', $idTransaction)->update([
                'id_asset'             => $idAsset,
                'id_requester'         => $nikRequester,
                'id_employee'          => $nikEmployee,
                'id_status'            => $idStatus,
                'receiving_date'       => $receivingDate,
                'givingBack_date'      => $givingBackDate,
                'receiving_condition'  => $receivingCondition,
                'givingBack_condition' => $givingBackCondition,
                'description'          => $description,
            ]);

            if($idAsset != $transaction[0]['id_asset'])
            {
                $asset = Asset::where('idAsset', $transaction[0]['id_asset'])->update(['id_status' => 1]);
                $asset = Asset::where('idAsset', $idAsset)->update(['id_status' => 2]);
            }

            else
            {
                $asset = Asset::where('idAsset', $transaction[0]['id_asset'])->update(['id_status' => 1]);
            }

            return $sql;

        }

        else if(!empty($givingBackCondition))
        {
            $sql = AssetTransaction::where('id', $idTransaction)->update([
                'id_asset'             => $idAsset,
                'id_requester'         => $nikRequester,
                'id_employee'          => $nikEmployee,
                'id_status'            => $idStatus,
                'receiving_date'       => $receivingDate,
                'receiving_condition'  => $receivingCondition,
                'givingBack_condition' => $givingBackCondition,
                'description'          => $description,
            ]);

            if($idAsset != $transaction[0]['id_asset'])
            {
                $asset = Asset::where('idAsset', $transaction[0]['id_asset'])->update(['id_status' => 1]);
                $asset = Asset::where('idAsset', $idAsset)->update(['id_status' => 2]);
            }

            return $sql;
        }

        else if(!empty($givingBackDate))
        {
            $sql = AssetTransaction::where('id', $idTransaction)->update([
                'id_asset'             => $idAsset,
                'id_requester'         => $nikRequester,
                'id_employee'          => $nikEmployee,
                'id_status'            => $idStatus,
                'receiving_date'       => $receivingDate,
                'givingBack_date'      => $givingBackDate,
                'receiving_condition'  => $receivingCondition,
                'description'          => $description,
            ]);

            if($idAsset != $transaction[0]['id_asset'])
            {
                $asset = Asset::where('idAsset', $transaction[0]['id_asset'])->update(['id_status' => 1]);
                $asset = Asset::where('idAsset', $idAsset)->update(['id_status' => 2]);
            }

            $transaction = AssetTransaction::where('id', $idTransaction)->get();
            $asset = Asset::where('idAsset', $transaction[0]['id_asset'])->update(['id_status' => 1]);

            return $sql;
        }

        else if($idAsset != $transaction[0]['id_asset'])
        {
            $sql = AssetTransaction::where('id', $idTransaction)->update([
                'id_asset'             => $idAsset,
                'id_requester'         => $nikRequester,
                'id_employee'          => $nikEmployee,
                'id_status'            => $idStatus,
                'receiving_date'       => $receivingDate,
                'receiving_condition'  => $receivingCondition,
                'description'          => $description,
            ]);

            if($idAsset != $transaction[0]['id_asset'])
            {
                $asset = Asset::where('idAsset', $transaction[0]['id_asset'])->update(['id_status' => 1]);
                $asset = Asset::where('idAsset', $idAsset)->update(['id_status' => 2]);
            }

            return $sql;

        }
    }

    /*public function saveAddTransaction1($data)
    {
        $idAsset = $data[0]['idAsset'];
        $idParent = $data[0]['idParent'];
        $receivingCondition = $data[0]['receivingCondition'];
        $idStatus = $data[0]['assetStatus'];

        $sql = "UPDATE itss_assets SET id_parent='{$idParent}' WHERE idAsset='{$idAsset}';";
        $data = DB::SELECT($sql);


        $parentAsset = AssetTransaction::where('id_asset', $idParent)->get();

        //dd($parentAsset);
        $nikEmployee = $parentAsset[0]['id_employee'];
        $id_requester = $parentAsset[0]['id_requester'];
        $receivingDate = date('Y-m-d');
        $givingBackDate = NULL;
        $givingBackCondition = NULL;
        $description = $parentAsset[0]['description'];
        $idLocation = $parentAsset[0]['id_location'];

        $transaction = new AssetTransaction;

        $transaction->id_asset = $idAsset;
        $transaction->id_parent = $idParent;
        $transaction->id_employee = $nikEmployee;
        $transaction->id_requester = $id_requester;
        $transaction->receiving_date = $receivingDate;
        $transaction->receiving_condition = $receivingCondition;
        $transaction->description = $description;
        $transaction->id_status = $idStatus;
        $transaction->id_location = $idLocation;

        $transaction->save();

        $asset = Asset::where('idAsset', $idAsset)->update(['id_status' => 2]);

        return $transaction;
    }*/

}
