<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Asset;

class AssetTransactionDetail extends Model
{
    //public $incrementing = false;
    protected $table = "itss_asset_transaction_detail";
    public $incrementing = false;

    public function getTransactionOfThisMonth()
    {
        $thismonth  = date('m');
        $thisYear   = date('Y');
        $record = AssetTransactionDetail::whereMonth('created_at', '=', date('m'))
        ->whereYear('created_at', '=', date('Y'))
        ->orderBy('created_at', 'desc')
        ->count();

        return $record;
    }

    public function getTransactionOfThisTransaction($idTransaction)
    {
        $record = AssetTransactionDetail::where('id_transaction_header', $idTransaction)->count();
        return $record;
    }

    public function getAllTransactionWhere($idTransaction)
    {
        $transaction = AssetTransactionDetail::where('id_transaction_header', $idTransaction)->get();
        return $transaction;
    }

    public function getAllTransactionWhereParent($idTransaction)
    {
        $transaction = AssetTransactionDetail::where('id_transaction_header', $idTransaction)->where('id_parent', null)->get();
        return $transaction;
    }

    public function getAllTransactionWhereChild($idTransaction)
    {
        $transaction = AssetTransactionDetail::where('id_transaction_header', $idTransaction)->whereNotNull('id_parent')->get();
        return $transaction;
    }
    public function getAllTransactionWhere1($idTransaction)
    {
        $transaction = AssetTransactionDetail::where('id', $idTransaction)->get();
        return $transaction;
    }

    public function getAllTransaction()
    {
        $transaction = AssetTransactionDetail::all();
        return $transaction;
    }

    public function saveDataTransaction($data)
    {
        //dd($data);
        $id_asset = $data[0]['idAsset'];
        $id_transaction = $data[0]['idTransaction'];
        $id_condition = $data[0]['assetCondition'];
        $description = $data[0]['description'];

        $record = $this->getTransactionOfThisTransaction($id_transaction);

        if($record == 0)
        {
            $number = 1;
        }

        else
        {
            $number = $record+1;
        }

        $transaction = new AssetTransactionDetail;

        $transaction->id = $id_transaction."-".$number;
        $transaction->id_transaction_header = $id_transaction;
        $transaction->line_number = $number;
        $transaction->id_asset = $id_asset;
        $transaction->id_asset_condition = $id_condition;
        $transaction->description_detail = $description;

        $transaction->save();

        $checkAsset = Asset::where('id_parent', $id_asset)->get();

        foreach ($checkAsset as $check) {
            $number = $number + 1;
            $transaction = new AssetTransactionDetail;

            $transaction->id = $id_transaction."-".$number;
            $transaction->id_parent = $id_asset;
            $transaction->id_transaction_header = $id_transaction;
            $transaction->line_number = $number;
            $transaction->id_asset = $check->idAsset;
            $transaction->id_asset_condition = $check->id_conditions;
            $transaction->description_detail = $description;

            $transaction->save();   
        }

        $sql = Asset::where('idAsset', $id_asset)->update([
                'id_status'      => 2,
            ]);
        $sql2 = Asset::where('id_parent',$id_asset)->update([
                'id_status'      => 2,
            ]);

        return $transaction;
    }

    public function saveEditDataTransaction($data)
    {
        //dd($data);
        $id_asset = $data[0]['idAsset'];
        $id_transaction = $data[0]['idTransaction'];
        $id_condition = $data[0]['assetCondition'];
        $description = $data[0]['description'];

        $transaction = AssetTransactionDetail::where('id', $id_transaction)->update([
            "id_asset_condition" => $id_condition,
            "description_detail" => $description,
        ]);

        return $transaction;
    }

    public function deleteDataTransaction($idTransaction)
    {
        
        $transactionAsset = AssetTransactionDetail::where('id', $idTransaction)->get();
        $idTransactionHeader = $transactionAsset[0]->id_transaction_header;
        $idAsset = $transactionAsset[0]->id_asset;


        $checkAsset = Asset::where('id_parent', $idAsset)->get();

        $sql1 = Asset::where('idAsset', $idAsset)->update([
                'id_status'      => 1,
            ]);
        $sql2 = Asset::where('id_parent', $idAsset)->update([
                'id_status'      => 1,
            ]);

        $delete = AssetTransactionDetail::where('id_parent', $idAsset)->delete();

        //dd($transactionAsset);
        $transacttionDetail = AssetTransactionDetail::where('id', $idTransaction)->delete();

        return $idTransactionHeader;
    }

    public function updateDataTransaction1($data)
    {
        //dd($data);
        $idTransaction = $data[0]['idTransaction'];
        $nikEmployee = $data[0]['employee'];
        $nikRequester = $data[0]['requester'];
        $transactionDate = date('Y-m-d', strtotime($data[0]['transactionDate']));
        $description = $data[0]['assetDescription'];
        $id_location = $data[0]['assetLocation'];
        $id_status   = $data[0]['transactionStatus1'];
        $status   = $data[0]['transactionStatus2'];

        $sql = AssetTransactionDetail::where('id', $idTransaction)->update([
                'nik_requestedby'      => $nikRequester,
                'nik_undertakenby'     => $nikEmployee,
                'transactiondate'      => $transactionDate,
                'id_status'            => $id_status,
                'id_location'          => $id_location,
                'status'               => $status,
                'description'          => $description,
            ]);

        //dd($sql);

            return $sql;
    }

    //GET ALL DATA JOIN
    public function getAllHistory($idAsset)
    {
        $history = AssetTransactionDetail::where('id_asset', $idAsset)->get();

        return $history;
    }

}
