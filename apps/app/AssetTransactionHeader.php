<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Asset;
use App\AssetTransactionDetail;

class AssetTransactionHeader extends Model
{
    //public $incrementing = false;
    protected $table = "itss_asset_transaction_header";
    public $incrementing = false;

    public function getTransactionOfThisMonth()
    {
        $thismonth  = date('m');
        $thisYear   = date('Y');
        $record = AssetTransactionHeader::whereMonth('created_at', '=', date('m'))
        ->whereYear('created_at', '=', date('Y'))
        ->orderBy('created_at', 'desc')
        ->count();

        return $record;
    }

    public function getAllTransactionWhere($idTransaction)
    {
        $transaction = AssetTransactionHeader::where('id', $idTransaction)->get();
        return $transaction;
    }

    public function getAllTransaction()
    {
        $transaction = AssetTransactionHeader::orderByDesc('transactiondate')->get();
        return $transaction;
    }

    public function saveDataTransaction($data)
    {
        $nik_employee = $data[0]['employee'];
        $nik_requester = $data[0]['requester'];
        $transactionDate = date('Y-m-d', strtotime($data[0]['transactionDate']));
        $description = $data[0]['assetDescription'];
        $id_location = $data[0]['assetLocation'];
        $transaction_status1 = $data[0]['transactionStatus1'];
        $transaction_status2 = $data[0]['transactionStatus2'];

        $sqlEmployee = "SELECT * FROM employee WHERE number='{$nik_employee}'";
        $dataEmployee = DB::connection('mysql2')->SELECT($sqlEmployee);
        $id_branch = $dataEmployee[0]->id_branch;

        $sqlCompany = "SELECT * FROM branch WHERE id='{$id_branch}'";
        $dataCompany = DB::connection('mysql2')->SELECT($sqlCompany);

        $employeeName = $dataCompany[0]->name;

        if( $employeeName == "PT MAC Sarana Djaya")
        {
            $code = "MAC";
        }

        else if($employeeName == "PT Centratama Menara Indonesia")
        {
            $code = "CMI";
        }

        else if($employeeName == "PT Fastel Sarana Indonesia")
        {
            $code = "FSI";
        }

        $companyCode = $code;
        //echo "1".$companyCode;


        $transactionRecord = $this->getTransactionOfThisMonth();
        if($transactionRecord == 0)
        {
            $number = 1;
            $number = str_pad($number, 5, '0', STR_PAD_LEFT);
            $thisMonth = date('m');    
            $thisYear = date('Y');
            $thisYear = substr($thisYear, -2);
            $companyCode = $companyCode; 
            //echo "2".$companyCode;
        }

        else
        {
            $number = $transactionRecord + 1;
            $number = str_pad($number, 5, '0', STR_PAD_LEFT);
            $thisMonth = date('m');          
            $thisYear = date('Y');
            $thisYear = substr($thisYear, -2);
            $companyCode = $companyCode; 
            //echo "3".$companyCode;
        }
        //echo "4".$companyCode;

        $idIFU = $companyCode.".PPABM ".$thisMonth.".".$thisYear.".".$number;

        //echo $idIFU;

        //dd($dataCompany);

        $transaction = new AssetTransactionHeader;

        $transaction->id = $idIFU;
        $transaction->nik_requestedby = $nik_requester;
        $transaction->nik_undertakenby = $nik_employee;
        $transaction->id_status = $transaction_status1;
        $transaction->id_location = $id_location;
        $transaction->transactiondate = $transactionDate;
        $transaction->description = $description;
        $transaction->status = 0;

        $transaction->save();
        return $transaction;
    }

    public function returnDataTransaction($idTransaction)
    {
        $sql = AssetTransactionHeader::where('id', $idTransaction)->update([
                'id_status' => 4,
                'status'    => 3,
            ]);

        $transactionDetail = AssetTransactionDetail::where('id_transaction_header', $idTransaction)->get();
        //dd($transactionDetail);

        foreach ($transactionDetail as $data) {
            $asset = Asset::where('idAsset', $data->id_asset)->update([
                "id_status" => 1,
            ]);
        }

        return $sql;
    }

    public function closeDataTransaction($idTransaction)
    {
        $transaction = AssetTransactionHeader::where('id',$idTransaction)->update([
            'status' => 1,
        ]);

        $status = AssetTransactionHeader::where('id', $idTransaction)->get();
        if($status[0]->id_status == 4)
        {
             $detail = AssetTransactionDetail::where('id_transaction_header', $idTransaction)->get();
             foreach ($detail as $thisAsset) {
                 $id_condition = $thisAsset->id_asset_condition;
                 //$description = $thisAsset->description_detail;

                 $asset = Asset::where('idAsset', $thisAsset->id_asset)->update([
                    "id_conditions" => $id_condition,
                 ]);
             }
        }
        return $transaction;
    }

    public function cancelDataTransaction($idTransaction)
    {
        $transaction = AssetTransactionHeader::where('id',$idTransaction)->update([
            'status' => 2,
        ]);

        $transactionDetail = AssetTransactionDetail::where('id_transaction_header', $idTransaction)->get();
        foreach ($transactionDetail as $setup) {
            $asset = Asset::where('idAsset', $setup->id_asset)->update([
                'id_status' => 1,
            ]);
        }

        return $transaction;
    }

    public function updateDataTransaction1($data)
    {
        //dd($data);
        $idTransaction = $data[0]['idTransaction'];
        $nikEmployee = $data[0]['employee'];
        $nikRequester = $data[0]['requester'];
        $transactionDate = date('Y-m-d', strtotime($data[0]['transactionDate']));
        $description = $data[0]['assetDescription'];
        $id_location = $data[0]['assetLocation'];
        $id_status   = $data[0]['transactionStatus1'];
        $status   = $data[0]['transactionStatus2'];

        $sql = AssetTransactionHeader::where('id', $idTransaction)->update([
                'nik_requestedby'      => $nikRequester,
                'nik_undertakenby'     => $nikEmployee,
                'transactiondate'      => $transactionDate,
                'id_status'            => $id_status,
                'id_location'          => $id_location,
                'status'               => $status,
                'description'          => $description,
            ]);

        //dd($sql);

            return $sql;
    }

}
