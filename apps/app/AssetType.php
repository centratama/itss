<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AssetType extends Model
{
    protected $table="itss_asset_types";
    //
    public function getAssetType()
    {
    	$sql = "SELECT * FROM itss_asset_types";

    	$data = DB::SELECT($sql);

    	return $data;
    }

    public function getAssetTypeWhere($idType)
    {
        $sql = "SELECT * FROM itss_asset_types WHERE id='{$idType}'";

        $data = DB::SELECT($sql);

        return $data;
    }

    public function saveDataAssets($data)
    {
        $assetName = $data[0]['assetName'];
        $idType = $data[0]['assetType'];
        $poNumber = $data[0]['poNumber'];
        $assetPrice = $data[0]['assetPrice'];
        $idVendor = $data[0]['assetVendor'];
        $idBrand = $data[0]['assetBrand'];
        $serialNumber = $data[0]['serialNumber'];
        $idConditions = $data[0]['assetCondition'];
        $idStatus = $data[0]['assetStatus'];
        $descriptions = $data[0]['assetDescription'];
        $idLocations = $data[0]['assetLocation'];
        $buyingDates = date('Y-m-d', strtotime($data[0]['assetBuyingDate']));

        $sql = "INSERT INTO itss_assets(name, po_numbers, serial_number, prices, descriptions, id_brands, id_vendors, id_conditions, id_status, id_locations, id_types, buyingDates)
                VALUES
                ('{$assetName}', '{$poNumber}', '{$serialNumber}', '{$assetPrice}', '{$descriptions}',  '{$idBrand}', '{$idVendor}', '{$idConditions}', '{$idStatus}',  '{$idLocations}', '{$idType}', '{$buyingDates}');";

        $data = DB::SELECT($sql);

        return $data;
    }

    public function updateDataAssets($data)
    {
        $idAsset = $data[0]['idAsset'];
        $assetName = $data[0]['assetName'];
        $idType = $data[0]['assetType'];
        $poNumber = $data[0]['poNumber'];
        $assetPrice = $data[0]['assetPrice'];
        $idVendor = $data[0]['assetVendor'];
        $idBrand = $data[0]['assetBrand'];
        $serialNumber = $data[0]['serialNumber'];
        $idConditions = $data[0]['assetCondition'];
        $idStatus = $data[0]['assetStatus'];
        $descriptions = $data[0]['assetDescription'];
        $idLocations = $data[0]['assetLocation'];
        $buyingDates = date('Y-m-d', strtotime($data[0]['assetBuyingDate']));

        $sql = "UPDATE itss_assets SET id_parent='1', name='{$assetName}', po_numbers='{$poNumber}', serial_number='{$serialNumber}', prices='{$assetPrice}', descriptions='{$descriptions}', id_brands='{$idBrand}', id_vendors='{$idVendor}', id_conditions='{$idConditions}', id_status='{$idStatus}', id_locations='{$idLocations}', id_types='{$idType}', buyingDates='{$buyingDates}' WHERE idAsset='{$idAsset}';";

        $data = DB::SELECT($sql);

        return $data;
    }

    public function updateDataType($data, $idType)
    {
        $typeName = $data[0]['typeName'];
        $typeDescription = $data[0]['typeDescription'];

        $sql = "UPDATE itss_asset_types SET typeName='{$typeName}', typeDescription='{$typeDescription}' 
                WHERE id='{$idType}';";

        $data = DB::SELECT($sql);

        return $data;
    }

    public function saveDataType($data)
    {
        $typeName = $data[0]['typeName'];
        $typeDescription = $data[0]['typeDescription'];

        $sql = "INSERT INTO itss_asset_types(typeName, typeDescription)
                VALUES
                ('{$typeName}', '{$typeDescription}');";

        $data = DB::SELECT($sql);

        return $data;
    }

}
