<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AssetVendor extends Model
{
    //
    protected $table="itss_asset_vendors";

    public function getAllAssetWhereVendor($idVendor)
    {
        //$data = Asset::all();
        $data = Asset::where('id_vendors', $idVendor)->paginate(100);
        //$sql = "SELECT * FROM itss_assets";
        //$data = DB::SELECT($sql);
        return $data;
    }

    public function getAssetVendor()
    {
    	$sql = "SELECT * FROM itss_asset_vendors";

    	$data = DB::SELECT($sql);

    	return $data;
    }

    public function getAssetVendorWhere($idVendor)
    {
        $sql = "SELECT * FROM itss_asset_vendors WHERE id='{$idVendor}'";

        $data = DB::SELECT($sql);

        return $data;
    }

    public function updateDataVendor($data, $idVendor)
    {
        $vendorName = $data[0]['vendorName'];
        $vendorDescription = $data[0]['vendorDescription'];
        $vendorAdress = $data[0]['vendorAdress'];
        $vendorPhoneNumber = $data[0]['vendorPhoneNumber'];

        /*$sql = "UPDATE itss_asset_vendors SET vendorName='{$vendorName}', vendorDescription='{$vendorDescription}' 
                WHERE id='{$idVendor}';";

        $data = DB::SELECT($sql);*/

        $data = AssetVendor::where('id', $idVendor)->update([
            "vendorName" => $vendorName,
            "vendorDescription" => $vendorDescription,
            "vendorAdress" => $vendorAdress,
            "vendorPhoneNumber" => $vendorPhoneNumber,
        ]);

        return $data;
    }

    public function saveDataVendor($data)
    {
        $idVendor = $data[0]['idVendor'];
        $vendorName = $data[0]['vendorName'];
        $vendorDescription = $data[0]['vendorDescription'];
        $vendorAdress = $data[0]['vendorAdress'];
        $vendorPhoneNumber = $data[0]['vendorPhoneNumber'];

        $data = new AssetVendor;

        $data->id = $idVendor;
        $data->vendorDescription = $vendorDescription;
        $data->vendorAdress = $vendorAdress;
        $data->vendorName = $vendorName;
        $data->vendorPhoneNumber = $vendorPhoneNumber;

        $data->save();

        /*$sql = "INSERT INTO itss_asset_vendors(id, vendorName, vendorDescription)
                VALUES
                ('{$idVendor}', '{$vendorName}', '{$vendorDescription}');";

        $data = DB::SELECT($sql);*/

        return $data;
    }

}
