<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class CompanyModel extends Model
{
	protected $connection = "mysql2";
	protected $table = "branch";
    public function getAllCompany()
    {
        // $sql = "SELECT * FROM {$this->prefix()}_employee ORDER BY namalengkap ASC;";

        // return $data = DB::SELECT($sql);
        $sql = "SELECT * FROM branch";
        $data = DB::connection('mysql2')->SELECT($sql);

        return $data;
    }
}
