<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EmployeesModel extends Model
{
    public function getAllEmployee()
    {
        // $sql = "SELECT * FROM {$this->prefix()}_employee ORDER BY namalengkap ASC;";

        // return $data = DB::SELECT($sql);
        $now = date("Y-m-d H:i:s");
        $sql = "SELECT * FROM employee WHERE resigndate >= '{$now}' OR resigndate IS NULL ORDER BY fullname;";
        $data = DB::connection('mysql2')->SELECT($sql);

        return $data;

    }

    public function getAllEmployeeData()
    {
        // $sql = "SELECT * FROM {$this->prefix()}_employee ORDER BY namalengkap ASC;";

        // return $data = DB::SELECT($sql);
        $now = date("Y-m-d H:i:s");
        $sql = "SELECT * FROM employee WHERE resigndate >= '{$now}' OR resigndate IS NULL ORDER BY fullname;";
        $data = DB::connection('mysql2')->SELECT($sql);

        return $data;

    }

    // flag is_admin or is_regist
    public function getDataByFlag($string, $int)
    {
    	/*is_admin flag
    	0 -> Common User
    	1 -> Admin
    	2 -> Super Admin*/

    	$sql = "SELECT * FROM {$this->prefix()}_employee
    			WHERE {$string} = '{$int}'
    			ORDER BY nik ASC;";

    	return $data = DB::SELECT($sql);
    }

    public function getByNIK($nik)
    {
    	$sql = "SELECT * FROM employee WHERE number = '{$nik}';";
        $data = DB::connection('mysql2')->SELECT($sql);

        return $data;
    }

    public function getOtherByNIK($nik)
    {
        $sql = "SELECT nik FROM {$this->prefix()}_user_level
                WHERE 
		-- level = '1'
                -- AND 
		nik != '{$nik}';";
        $getOtherNIK = DB::SELECT($sql);

        $otherNIK = [];
        foreach ($getOtherNIK as $key => $value) {
            $otherNIK[] = $value->nik;
        }

        $data = DB::connection('mysql2')->table('employee')
                ->whereIn('number', $otherNIK)
                ->orderBy('fullname', 'asc')
                ->get();

        return $data;
    }

    public function getDepartmentEmployee($id)
    {
        $sql = "SELECT * FROM {$this->prefix()}_department
                WHERE id = '{$id}';";

        return $data = DB::SELECT($sql);   
    }

    public function getEmailPIC($case_type_value)
    {
        $sql = "SELECT b.nik FROM {$this->prefix()}_set_pic b WHERE b.case_type_value = '{$case_type_value}';";

        $data = DB::SELECT($sql);


        foreach ($data as $key => $value) {
            $nik[$key] = $value->nik;
        }

        $getPIC = DB::connection('mysql2')->table('employee')
                ->whereIn('number', $nik)
                ->orderBy('fullname', 'asc')
                ->get();

        foreach ($getPIC as $key => $value) {
            $emails[$key] = $value->email;
        }

        return $emails;
    }

    public function getPICCase($nik)
    {
        $sql = "SELECT case_type_value FROM {$this->prefix()}_set_pic WHERE nik = '{$nik}';";

        return DB::SELECT($sql);        
    }

    public function getAllSupport()
    {
        $sql = "SELECT nik FROM {$this->prefix()}_user_level
                -- WHERE level = '1'
                ORDER BY level;"
                ;
        $getOtherNIK = DB::SELECT($sql);

        $otherNIK = [];
        foreach ($getOtherNIK as $key => $value) {
            $otherNIK[] = $value->nik;
        }

        $data = DB::connection('mysql2')->table('employee')
                ->whereIn('number', $otherNIK)
                ->orderBy('fullname', 'asc')
                ->get();

        return $data;
    }

    public function getByEmail($email)
    {
        $sql = "SELECT * FROM employee WHERE email = '{$email}';";
        $data = DB::connection('mysql2')->SELECT($sql);

        return $data;
    }

}
