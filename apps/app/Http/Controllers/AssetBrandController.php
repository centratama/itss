<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AssetBrand;
use App\Asset;
use App\AssetType;
use App\AssetStatus;
use App\AssetVendor;
use App\EmployeesModel;
use App\CompanyModel;
use App\DepartmentsModel;
use App\AssetCondition;
use App\AssetLocation;
use App\AssetTransaction;
use App\DivisionsModel;
use App\SignReport;
use Alert;
use Illuminate\Support\Facades\Session;
use Redirect;

class AssetBrandController extends Controller
{

    public function __construct()
    {
    	$this->assetBrand = new AssetBrand;
        $this->assetType = new AssetType;
        $this->assetStatus = new AssetStatus;
        $this->assetVendor = new AssetVendor;
        $this->assetData = new Asset;
        $this->assetCondition = new AssetCondition;
        $this->assetLocation = new AssetLocation;
        $this->AssetTransaction = new AssetTransaction;
        $this->EmployeesModel = new EmployeesModel;
        $this->CompanyModel = new CompanyModel;
        $this->DepartmentsModel = new DepartmentsModel;
        $this->DivisionsModel = new DivisionsModel;
        $this->SignReport = new SignReport;
    }


    //VIEWING ASSET'S BRANDS
    public function viewBrands()
    {
        //checking if user logged in or not
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            return view('components.brands.brands', compact('data'))->with(Session::get('logged_in'));
        } 

        else 
        {
            return Redirect::to('/login');
        }
    }

    //CREATING ASSETS'S BRANDS
    public function createBrand()
    {
    	//checking if user logged in or not
        if (session()->has('logged_in'))
        {
        	//getting data from each model required
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            return view('components.brands.createBrands', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
        	//back to the login form
            return Redirect::to('/login');
        }
    }

    //SAVING BRANDS
    public function saveBrand(Request $request)
    {
        if($request->ajax())
        {
            $dataBrands= [$request->input()];
            $this->assetBrand->saveDataBrand($dataBrands);

            alert()->success('Assets created successfully', 'Congrats!');
            return $dataBrands;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

    //EDITING ASSETS'S BRANDS
    public function editBrand($idBrand)
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetBrand'] = $this->assetBrand->getAssetBrandWhere($idBrand);
            return view('components.brands.editBrands', compact('data'))->with('idBrand', $idBrand)->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //SAVING EDITED BRAND
    public function saveEditBrand(Request $request, $idBrand)
    {
        if($request->ajax())
        {
            $dataBrands= [$request->input()];
            $this->assetBrand->updateDataBrand($dataBrands, $idBrand);

            alert()->success('your update successfully', 'Congrats!');
            return $dataBrands;
        }

        else
        {
            alert()->success('Asset is not updated successfully', 'Sorry!');
        }
    }

    public function thisAsset($idBrand)
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetBrand->getAllAssetWhereBrand($idBrand);
            $data['assetDatas'] = $this->assetBrand->getAllAssetWhereBrand($idBrand);
            //$data['assetData'] = $this->assetData->getAllAsset1();
            //$data['assetDatas'] = $this->assetData->getAllAsset1();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();

            //dd($data['assetData']);
            return view('components.assets.assets', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    public function getListJson()
    {

        $nik = Session::get('logged_in')['nik'];

        // reverse label for level ticket
        $data = $this->assetBrand->getAssetBrand();

        foreach($data as $row) {

            $url_editBrand = url('/asset/brands/editBrand/'.$row->id);
            $url_thisAsset = url('/asset/brands/asset/'.$row->id);
            $view = "<html>
                        <a href='{$url_editBrand}'>
                            <i class='fa fa-pencil' style='font-size:15px;color:blue'></i>
                        </a>
                        |
                        <a href='{$url_thisAsset}'>
                            <i class='fa fa-link' style='font-size:15px;color:blue'></i>
                        </a>
                    </html>";

            $result['data'][] = array(
                    'idBrand' => $row->id,
                    'brandName' => $row->brandName,
                    'brandDesc' => $row->brandDescription,
                    'tool' => $view,
                    //'embeded' => $embeded,
                );
            //dd($result);
        }

        if(!empty($result)){
            //dd($result);
                return json_encode($result, JSON_UNESCAPED_SLASHES);
        } else {
            //dd($result);
            $result['data'][]= array(
                    'idBrand' => 'no data',
                    'brandName' => 'no data',
                    'brandDesc' => 'no data',
                    'tool' => 'no data',
                );
            return json_encode($result);
        }
    }
}
