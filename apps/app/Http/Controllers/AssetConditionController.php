<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\AssetType;
use App\AssetStatus;
use App\AssetVendor;
use App\AssetBrand;
use App\EmployeesModel;
use App\CompanyModel;
use App\DepartmentsModel;
use App\AssetCondition;
use App\AssetLocation;
use App\AssetTransaction;
use App\DivisionsModel;
use App\SignReport;
use Alert;
use Illuminate\Support\Facades\Session;
use Redirect;
use Mail;
use PDF;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class AssetConditionController extends Controller
{

    public function __construct()
    {
    	$this->assetType = new AssetType;
    	$this->assetStatus = new AssetStatus;
    	$this->assetVendor = new AssetVendor;
    	$this->assetBrand = new AssetBrand;
    	$this->assetData = new Asset;
    	$this->assetCondition = new AssetCondition;
    	$this->assetLocation = new AssetLocation;
        $this->AssetTransaction = new AssetTransaction;
        $this->EmployeesModel = new EmployeesModel;
        $this->CompanyModel = new CompanyModel;
        $this->DepartmentsModel = new DepartmentsModel;
        $this->DivisionsModel = new DivisionsModel;
        $this->SignReport = new SignReport;
    }

    //VIEW CONDITION
    public function viewCondition()
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();

            //dd($data['assetData']);
            return view('components.assetCondition.assetCondition', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    //CREATE CONDITION
    public function createCondition()
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //return to view with its data and its current session
            return view('components.assetCondition.createCondition', compact('data'))->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //SAVE CONDITION
    public function saveCondition(Request $request)
    {
        if($request->ajax())
        {
            $dataCondition= [$request->input()];
            //dd($dataAssets);
            $this->assetCondition->saveDataCondition($dataCondition);

            alert()->success('Saved successfully', 'Congrats!');
            return $dataCondition;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

    //EDIT CONDITION
    public function editCondition($idCondition)
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetCondition'] = $this->assetCondition->getAssetConditionWhere($idCondition);

            //return to view with its data and its current session
            return view('components.assetCondition.editCondition', compact('data'))->with('idCondition', $idCondition)->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //SAVE EDIT CONDITION
    public function saveEditCondition(Request $request, $idCondition)
    {
        if($request->ajax())
        {
            $dataCondition= [$request->input()];
            //dd($dataAssets);
            $this->assetCondition->updateDataCondition($dataCondition, $idCondition);

            alert()->success('your update successfully', 'Congrats!');
            return $dataCondition;
        }

        else
        {
            alert()->success('Asset is not updated successfully', 'Sorry!');
        }
    }

    
    
}
