<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AssetLocation;
use Alert;
use Illuminate\Support\Facades\Session;
use Redirect;

class AssetLocationController extends Controller
{

    public function __construct()
    {
    	$this->assetLocation = new AssetLocation;
    }

    //VIEWING ASSET'S LOCATION
    public function viewLocation()
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            return view('components.assetLocation.assetLocation', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    //CREATING ASSET'S LOCATION
    public function createLocation()
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //return to view with its data and its current session
            return view('components.assetLocation.createLocation', compact('data'))->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //EDITING ASSET'S LOCATION
    public function editLocation($idLocation)
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetLocation'] = $this->assetLocation->getAssetLocationWhere($idLocation);

            //return to view with its data and its current session
            return view('components.assetLocation.editLocation', compact('data'))->with('idLocation', $idLocation)->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //SAVING EDITED LOCATION
    public function saveEditLocation(Request $request, $idLocation)
    {
        if($request->ajax())
        {
            $dataLocation= [$request->input()];
            $this->assetLocation->updateDataLocation($dataLocation, $idLocation);

            alert()->success('your update successfully', 'Congrats!');
            return $dataLocation;
        }

        else
        {
            alert()->success('Asset is not updated successfully', 'Sorry!');
        }
    }

    //SAVING ASSET'S LOCATION
    public function saveLocation(Request $request)
    {
        if($request->ajax())
        {
            $dataLocation= [$request->input()];
            //dd($dataAssets);
            $this->assetLocation->saveDataLocation($dataLocation);

            alert()->success('Saved successfully', 'Congrats!');
            return $dataLocation;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

}
