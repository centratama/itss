<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AssetStatus;
use Alert;
use Illuminate\Support\Facades\Session;
use Redirect;

class AssetStatusController extends Controller
{
    public function __construct()
    {
        $this->assetStatus = new AssetStatus;
    }

    //VIEWING ASSET'S STATUS
    public function viewStatus()
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            return view('components.assetStatus.assetStatus', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    //CREATING STATUS
    public function createStatus()
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            return view('components.assetStatus.createStatus', compact('data'))->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //EDITING STATUS
    public function editStatus($idStatus)
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetStatus'] = $this->assetStatus->getAssetStatusWhere($idStatus);

            //return to view with its data and its current session
            return view('components.assetStatus.editStatus', compact('data'))->with('idStatus', $idStatus)->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //SAVING EDITED STATUS
    public function saveEditStatus(Request $request, $idStatus)
    {
        if($request->ajax())
        {
            $dataStatus= [$request->input()];
            //dd($dataAssets);
            $this->assetStatus->updateDataStatus($dataStatus, $idStatus);

            alert()->success('your update successfully', 'Congrats!');
            return $dataStatus;
        }

        else
        {
            alert()->success('Asset is not updated successfully', 'Sorry!');
        }
    }

    //SAVING ASSET'S STATUS
    public function saveStatus(Request $request)
    {
        if($request->ajax())
        {
            $dataStatus= [$request->input()];
            //dd($dataStatus);
            $this->assetStatus->saveDataStatus($dataStatus);

            alert()->success('Saved successfully', 'Congrats!');
            return $dataStatus;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

}
