<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AssetType;
use Alert;
use Illuminate\Support\Facades\Session;
use Redirect;

class AssetTypeController extends Controller
{

    public function __construct()
    {
    	$this->assetType = new AssetType;
    }

    //VIEW ASSETS LIST
    public function viewAssets()
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetType'] = $this->assetType->getAssetType();
            return view('components.assets.assets', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    //VIEWING ASSET'S TYPE
    public function viewType()
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetType'] = $this->assetType->getAssetType();

            //dd($data['assetData']);
            return view('components.assetType.assetType', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    //CREATING ASSET'S TYPE
    public function createType()
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //return to view with its data and its current session
            return view('components.assetType.createType', compact('data'))->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //EDITING ASSET'S TYPE
    public function editType($idType)
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetType'] = $this->assetType->getAssetTypeWhere($idType);
            return view('components.assetType.editType', compact('data'))->with('idType', $idType)->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //SAVING EDITED TYPE
    public function saveEditType(Request $request, $idType)
    {
        if($request->ajax())
        {
            $dataType= [$request->input()];
            //dd($dataAssets);
            $this->assetType->updateDataType($dataType, $idType);

            alert()->success('your update successfully', 'Congrats!');
            return $dataType;
        }

        else
        {
            alert()->success('Asset is not updated successfully', 'Sorry!');
        }
    }

    //SAVING ASSET'S TYPE
    public function saveType(Request $request)
    {
        if($request->ajax())
        {
            $dataType= [$request->input()];
            //dd($dataAssets);
            $this->assetType->saveDataType($dataType);

            alert()->success('Saved successfully', 'Congrats!');
            return $dataType;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }
}
