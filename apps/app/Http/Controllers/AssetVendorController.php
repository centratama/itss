<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AssetVendor;
use Alert;
use Illuminate\Support\Facades\Session;
use Redirect;

class AssetVendorController extends Controller
{
    public function __construct()
    {
        $this->assetVendor = new AssetVendor;
    }

    //VIEWING VENDOR
    public function viewVendors()
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            return view('components.vendors.vendors', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    //CREATING VENDOR
    public function createVendor()
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            return view('components.vendors.createVendors', compact('data'))->with(Session::get('logged_in'));
        }

        else 
        {
            return Redirect::to('/login');
        }
    }

    //EDITING VENDOR
    public function editVendor($idVendor)
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetVendor'] = $this->assetVendor->getAssetVendorWhere($idVendor);

            return view('components.vendors.editVendors', compact('data'))->with('idVendor', $idVendor)->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //SAVING EDITED VENDOR
    public function saveEditVendor(Request $request, $idVendor)
    {
        if($request->ajax())
        {
            $dataVendors= [$request->input()];
            $this->assetVendor->updateDataVendor($dataVendors, $idVendor);

            alert()->success('your update successfully', 'Congrats!');
            return $dataVendors;
        }

        else
        {
            alert()->success('Asset is not updated successfully', 'Sorry!');
        }
    }

    //SAVE VENDOR
    public function saveVendor(Request $request)
    {
        if($request->ajax())
        {
            $dataVendors= [$request->input()];
            $this->assetVendor->saveDataVendor($dataVendors);

            alert()->success('Assets created successfully', 'Congrats!');
            return $dataVendors;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

    public function thisAsset($idVendor)
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetVendor->getAllAssetWhereVendor($idVendor);
            $data['assetDatas'] = $this->assetVendor->getAllAssetWhereVendor($idVendor);
            //$data['assetData'] = $this->assetData->getAllAsset1();
            //$data['assetDatas'] = $this->assetData->getAllAsset1();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();

            //dd($data['assetData']);
            return view('components.assets.assets', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    public function getListJson()
    {

        $nik = Session::get('logged_in')['nik'];

        // reverse label for level ticket
        $data = $this->assetVendor->getAssetVendor();

        foreach($data as $row) {

            $url_editVendor = url('/asset/vendors/editVendor/'.$row->id);
            $url_thisAsset = url('/asset/vendors/asset/'.$row->id);
            $view = "<html>
                        <a href='{$url_editVendor}'>
                            <i class='fa fa-plus-circle' style='font-size:15px;color:blue'></i>
                        </a>
                        |
                        <a href='{$url_thisAsset}'>
                            <i class='fa fa-link' style='font-size:15px;color:blue'></i>
                        </a>
                    </html>";

            $result['data'][] = array(
                    'idVendor' => $row->id,
                    'vendorName' => $row->vendorName,
                    'vendorDesc' => $row->vendorDescription,
                    'tool' => $view,
                    //'embeded' => $embeded,
                );
            //dd($result);
        }

        if(!empty($result)){
            //dd($result);
                return json_encode($result, JSON_UNESCAPED_SLASHES);
        } else {
            //dd($result);
            $result['data'][]= array(
                    'idVendor' => 'no data',
                    'vendorName' => 'no data',
                    'vendorDesc' => 'no data',
                    'tool' => 'no data',
                );
            return json_encode($result);
        }
    }

}
