<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\AssetType;
use App\AssetStatus;
use App\AssetVendor;
use App\AssetBrand;
use App\EmployeesModel;
use App\CompanyModel;
use App\DepartmentsModel;
use App\AssetCondition;
use App\AssetLocation;
use App\AssetTransaction;
use App\AssetTransactionHeader;
use App\AssetTransactionDetail;
use App\DivisionsModel;
use App\SignReport;
use App\AssetEmployeeTransaction;
use Alert;
use Illuminate\Support\Facades\Session;
use Redirect;
use Mail;
use PDF;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class AssetsController extends Controller
{
    public $emailIT = "it@centratamagroup.com";
    // public $emailIT = "amarfd.apple@gmail.com";

    public function __construct()
    {
    	$this->assetType = new AssetType;
    	$this->assetStatus = new AssetStatus;
    	$this->assetVendor = new AssetVendor;
    	$this->assetBrand = new AssetBrand;
    	$this->assetData = new Asset;
    	$this->assetCondition = new AssetCondition;
    	$this->assetLocation = new AssetLocation;
        $this->AssetTransaction = new AssetTransaction;
        $this->EmployeesModel = new EmployeesModel;
        $this->CompanyModel = new CompanyModel;
        $this->DepartmentsModel = new DepartmentsModel;
        $this->DivisionsModel = new DivisionsModel;
        $this->SignReport = new SignReport;
        $this->AssetEmployeeTransaction = new AssetEmployeeTransaction;
        $this->AssetTransactionHeader = new AssetTransactionHeader;
        $this->AssetTransactionDetail = new AssetTransactionDetail;
    }

    //VIEW ASSETS LIST
    public function viewAssets()
    {
        if(Session()->has('logged_in'))
        {
            return view('components.assets.assets', compact('data'))->with(Session::get('logged_in'));
        } 

        else 
        {
            return Redirect::to('/login');
        }
    }

    //VIEW ASSETS LIST
    public function signReport()
    {
        if(Session()->has('logged_in'))
        {
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['allSign'] = $this->SignReport->getAllSign();
            return view('components.assets.verifiedby', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    //VIEW ASSETS LIST
    public function saveSignReport(Request $request)
    {
        if($request->ajax())
        {
            //dd($request->input());
            $data= [$request->input()];

            $this->SignReport->saveSignReport($data);
            

            alert()->success('Assets update successfully', 'Congrats!');
            return $data;
        }

        else
        {
            alert()->success('Asset is not updated successfully', 'Sorry!');
        }
    }

    //VIEW ASSETS LIST
    public function reportAssets()
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset();
            $data['assetDatas'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();

            //dd($data['assetData']);
            return view('components.assets.assetReport', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    //CREATE ASSETS
    public function createAssets()
    {
    	//checking if user logged in or not
        if (session()->has('logged_in'))
        {
        	//getting data from each model required
        	$data['assetData'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            //$data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetDataAvailable'] = $this->assetData->getAllAssetAvailable();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['company'] = $this->CompanyModel->getAllCompany();

            //return to view with its data and its current session
            return view('components.assets.createAssets', compact('data'))->with(Session::get('logged_in'));
        } 

        else 
        {
            return Redirect::to('/login');
        }
    }

    //EDIT ASSETS
    public function editAsset($idAsset)
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAssetWhere($idAsset);
            //$data['assetDataAvailable'] = $this->assetData->getAllAssetNull($idAsset);
            $data['assetDataAvailable'] = $this->assetData->getAllAsset11($idAsset);
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['company'] = $this->CompanyModel->getAllCompany();

            return view('components.assets.editAssets', compact('data'))->with('idAsset', $idAsset)->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //DETAIL ASSETS
    public function detailAsset($idAsset)
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAssetWhere($idAsset);
            $data['assetDatas'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['company'] = $this->CompanyModel->getAllCompany();
            //return to view with its data and its current session
            return view('components.assets.detailAssets', compact('data'))->with('idAsset', $idAsset)->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //EDIT TRANSACTION
    public function saveEditAsset(Request $request)
    {
        if($request->ajax())
        {
            //dd($request->input());
            $dataAssets= [$request->input()];

            if(empty($request->file('assetPhoto')))
            {
                $fileName = 0;
            }

            else
            {
                $idAsset = $request->input('idAsset');
                $fileName = $request->file('assetPhoto')->getClientOriginalName();
                $fileSize = $request->file('assetPhoto')->getClientSize();
                //dd($dataAssets);
                //$this->assetData->updateDataAssets($dataAssets);
                $request->file('assetPhoto')->move('/mainData/data/assets/itss/foto', $idAsset.'.jpg'); 
            }

            if(empty($request->file('assetBerlac')))
            {
                $fileNameBerlac = 0;
            }

            else
            {
                $idAsset = $request->input('idAsset');
                $fileNameBerlac = $request->file('assetBerlac')->getClientOriginalName();
                $fileSizeBerlac = $request->file('assetBerlac')->getClientSize();
                //dd($dataAssets);
                //$this->assetData->updateDataAssets($dataAssets);
                $request->file('assetBerlac')->move('/mainData/data/assets/itss/html', $idAsset.'.pdf'); 
            }

            $this->assetData->updateDataAssets($dataAssets, $fileName, $fileNameBerlac);
            

            alert()->success('Assets update successfully', 'Congrats!');
            return $dataAssets;
        }

        else
        {
            alert()->success('Asset is not updated successfully', 'Sorry!');
        }
    }

    //SAVE ASSETS
    public function saveAssets(Request $request)
    {
    	if($request->ajax())
    	{
            $dataAssets= [$request->input()];
            $type = AssetType::select('typeName')->where('id', $request->assetType)->get();
            $type = $type[0]['typeName'];

            $idAsset = $this->assetData->saveDataAssets($dataAssets,$type);

            //$fileName = NULL;
            //$fileNameBerlac = NULL;
            if($request->file('assetPhoto'))
            {
                //$idAsset = $request->input('idAsset');
                $fileName = $request->file('assetPhoto')->getClientOriginalName();
                $fileSize = $request->file('assetPhoto')->getClientSize();
                //$request->file('assetPhoto')->move('/mainData/data/assets/itss/foto', $fileName);
                $request->file('assetPhoto')->move('/mainData/data/assets/itss/foto', $idAsset.'.jpg'); 
            }

            if($request->file('assetBerlac'))
            {
                //$idAsset = $request->input('idAsset');
                $fileNameBerlac = $request->file('assetBerlac')->getClientOriginalName();
                $fileSizeBerlac = $request->file('assetBerlac')->getClientSize();
                $request->file('assetBerlac')->move('/mainData/data/assets/itss/html', $idAsset.'.pdf');
            }

            alert()->success('Assets created successfully', 'Congrats!');
            return $dataAssets;
    	}

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

    //VIEW TRANSACTION HISTORY
    public function viewTransactionHistory($idAsset)
    {
        if(Session()->has('logged_in'))
        {
            return view('components.assets.transactionHistory')->with('idAsset', $idAsset)->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

        //VIEW TRANSACTION HISTORY
    public function viewBerlacFile($fileName)
    {
        if(Session()->has('logged_in'))
        {
            return view('components.assets.assetBerlac')->with('fileName', $fileName)->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    public function addTransaction1($idAsset)
    {
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAssetNull($idAsset);
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();


            //dd($data['assetData']);


            //return to view with its data and its current session
            return view('components.assets.addTransaction1', compact('data'))->with('idAsset',$idAsset)->with(Session::get('logged_in'));
        } 
    }

    //SAVE TRANSACTION
    public function saveAddTransaction(Request $request)
    {
        if($request->ajax())
        {

            //dd($request->input());
            $dataTransaction= [$request->input()];
            //dd($dataTransaction);
            $this->AssetTransaction->saveAddTransaction1($dataTransaction);

            alert()->success('Assets created successfully', 'Congrats!');
            return $dataTransaction;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

    public function downloadPDF($idTransaction)
    {
        //getting data from each model required
        $data['assetData'] = $this->assetData->getAllAsset();
        $data['assetDatas'] = $this->assetData->getAllAsset();
        $data['assetType'] = $this->assetType->getAssetType();
        $data['assetStatus'] = $this->assetStatus->getAssetStatus();
        $data['assetVendor'] = $this->assetVendor->getAssetVendor();
        $data['assetBrand'] = $this->assetBrand->getAssetBrand();
        $data['assetCondition'] = $this->assetCondition->getAssetCondition();
        $data['assetLocation'] = $this->assetLocation->getAssetLocation();
        $data['employee'] = $this->EmployeesModel->getAllEmployee();
        $data['transactionData'] = $this->AssetTransaction->getTransactionWhere($idTransaction);
        $data['transactionDataMonth'] = $this->AssetTransaction->getTransactionWhereMonth($idTransaction);
        $data['department'] = $this->DepartmentsModel->getAllDept();
        $data['division'] = $this->DivisionsModel->getAllDiv();
        $data['allSign'] = $this->SignReport->getAllSign();

        //dd($data['transactionData']);
        //$data['employeeData'] = $this->EmployeesModel->getAllEmployee();
        //$data['idTransaction'] = $idTransaction;
            if($data['transactionDataMonth']->isEmpty())
            {
                $number = 1;
                //echo $number;
                $number = str_pad($number, 5, '0', STR_PAD_LEFT);
                //echo $number;
                $month = date('m');
                    
                $year = date('Y');
                $year = substr($year, -2);

                $companyCode = $data['transactionData'][0]->id_asset;
                $companyCode = substr($companyCode, 0, strpos($companyCode, '-')); 
               
            }

            else
            {
                $i = 1;

                foreach ($data['transactionDataMonth'] as $checkingNumber) {
                    if($checkingNumber->id == $idTransaction)
                    {
                        $number = $i;
                    }
                    $i++;
                }
                $number = str_pad($number, 5, '0', STR_PAD_LEFT);

                $date = strtotime($data['transactionDataMonth'][0]->receiving_date);
                $month = date('m', $date);          
                $year = date('Y', $date);
                $companyCode = $data['transactionData'][0]->id_asset;
                $companyCode = substr($companyCode, 0, strpos($companyCode, '-')); 
            }

            $idIFU = $companyCode.".PPABM ".$month.".".$year.".".$number;

            $data['idIFU'] = $idIFU;


        $pdf = PDF::loadView('components.assets.assetReportDownload',  array('data' => $data))->setPaper('A4');
        return $pdf->download('transaction.pdf');
    }

    public function depedency($id)
    {
        $data = Asset::where("idAsset",$id)->get();

        $condition = AssetCondition::where('id', $data[0]->id_conditions)->pluck("id","conditionName");


        //dd($condition);
        return json_encode($condition);
    }

    public function depedencyCreateAsset($idType)
    {
        //$data = Asset::where("id_types",$idType)->pluck("idAsset", "name");

        $data = $this->assetData->allAssetWhereParentNullAndNotThisIdTypeCreate($idType);

        //$condition = AssetType::where('id', $data[0]->id_conditions)->pluck("id","conditionName");


        //dd($condition);
        return json_encode($data);
    }

    public function getListJson()
    {
        /*$data['assetData'] = $this->assetData->getAllAsset1();
        $data['assetDatas'] = $this->assetData->getAllAsset1();
        $data['assetType'] = $this->assetType->getAssetType();
        $data['assetStatus'] = $this->assetStatus->getAssetStatus();
        $data['assetVendor'] = $this->assetVendor->getAssetVendor();
        $data['assetBrand'] = $this->assetBrand->getAssetBrand();
        $data['assetCondition'] = $this->assetCondition->getAssetCondition();
        $data['assetLocation'] = $this->assetLocation->getAssetLocation();
        $data['employee'] = $this->EmployeesModel->getAllEmployee();*/

        $nik = Session::get('logged_in')['nik'];

        // reverse label for level ticket
        $data = $this->assetData->getAllAsset1();
        //dd($data[0]->created_at);
        foreach($data as $row) {
            /*$embeded = array();
            foreach ($data as $row2) {
                $url_delete = url('/asset/child/deleteAssetChild/'.$row2->idAsset);
                $brand = $this->assetBrand->getAssetBrandWhere($row2->id_brands);
                $vendor = $this->assetVendor->getAssetVendorWhere($row2->id_vendors);
                if($row->idAsset == $row2->id_parent){
                    $embeded[] = array(

                        'idAsset' => $row2->idAsset,
                        'name' => $row2->name,
                        'serial_number' => $row2->serial_number,
                        'brand' => $brand[0]->brandName,
                        'tool' =>   "<html>
                                        <a href='{$url_delete}'>
                                            <i class='fa fa-close' style='font-size:15px;color:red'></i>
                                        </a>
                                    </html>",
                        //'vendor' => $vendor[0]->vendorName,
                    );
                }
            }*/
            //dd($embeded);


            $url_add = url('/asset/addTransaction/'.$row->idAsset);
            $url_edit = url('/asset/editAsset/'.$row->idAsset);
            $url_detail = url('/asset/detailAsset/'.$row->idAsset);
            $url_history = url('/asset/transactionHistory/'.$row->idAsset);
            $url_berlac = url('/asset/berlacAsset/'.$row->file_html);
            $view = "<html>
                    <div class='dropdown'>
                      <button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                        tools
                      </button>
                      <ul class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
                        <li><a href='{$url_edit}' class='dropdown-item'>
                            EDIT
                        </a></li>
                        <li><a href='{$url_detail}' class='dropdown-item'>
                            DETAIL
                        </a></li>
                        <li><a href='{$url_history}' class='dropdown-item'>
                            HISTORY
                        </a></li>
                        <li><a href='{$url_berlac}' class='dropdown-item'>
                            BERLAC
                        </a></li>
                      </ul>
                    </div>
                    </html>";
           
            $brand = $this->assetBrand->getAssetBrandWhere($row->id_brands);
            $vendor = $this->assetVendor->getAssetVendorWhere($row->id_vendors);
            $result['data'][] = array(
                    'idAsset' => $row->idAsset,
                    'id_parent' => $row->id_parent,
                    'name' => $row->name,
                    'serial_number' => $row->serial_number,
                    'brand' => $brand[0]->brandName,
                    'vendor' => $vendor[0]->vendorName,
                    'created_at' => $row->created_at->format('d M Y - H:i:s'),
                    'tool' => $view,
                    //'embeded' => $embeded,
                );
            //dd($result);
        }

        if(!empty($result)){
            //dd($result);
                return json_encode($result, JSON_UNESCAPED_SLASHES);
        } else {
            //dd($result);
            $result['data'][]= array(
                    'idAsset' => 'no data',
                    'name' => 'no data',
                    'serial_number' => 'no data',
                    'brand' => 'no data',
                    'vendor' => 'no data',
                    'tool' => 'no data',
                );
            return json_encode($result);
        }
    }

}
