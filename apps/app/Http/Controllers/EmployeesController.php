<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeesModel;
use App\DepartmentsModel;
use App\PositionsModel;

class EmployeesController extends Controller
{
    //
    function __construct()
    {
    	$this->EmployeesModel = new EmployeesModel;
    	$this->DepartmentsModel = new DepartmentsModel;
    	$this->PositionsModel = new PositionsModel;
    }

    public function getListJson()
    {
    	$data = $this->EmployeesModel->getAllEmployee();

    	$no = 0;
        foreach ($data as $row) {
        	// $getDepartment = $this->DepartmentsModel->getById($row->iddept);
        	// $getPosition = $this->PositionsModel->getById($row->idposition);
            $getDepartment = $this->DepartmentsModel->getById($row->id_organization);
            $getPosition = $this->PositionsModel->getById($row->id_job);
        	if (empty($getDepartment)) {
        		$department = "";
        	} else {
        		$department = $getDepartment[0]->department;
                $job = $getDepartment[0]->job;
        	}

        	if (empty($getPosition)) {
        		$position = "";
        	} else {
        		$position = $getPosition[0]->description;
        	}

            $url_view = url('/employee/view')."/".$row->number;
            $view = "<a href='{$url_view}' class='btn btn-warning'><i class='fa fa-search'></i></a>";
            $result['data'][] = array(
                    'no' => "<b>".++$no.".</b>",
                    'nik' => $row->number,
                    'name' => $row->fullname,
                    'position' => $position,
                    'department' => $job,
                    'email' => $row->email,
                    'view' => $view,

            );
        }

        if(!empty($result)){
                echo json_encode($result);
        }
    }
}
