<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Session;
use Alert;
use Redirect;
use Session;
use App\TicketsModel;
use App\EmployeesModel;
use App\SysLookupValModel;

class HomeController extends Controller
{
    //
    public function __construct()
    {
    	$this->TicketsModel = new TicketsModel;
        $this->SysLookupValModel = new SysLookupValModel;
        $this->EmployeesModel = new EmployeesModel;
    }

    public function index()
    {
    	 if (session()->has('logged_in')) {
    	 	if (in_array(Session::get('logged_in')['user_role'], array('ADMIN', 'SUPPORT'))) {
                $data['officer'] = $this->EmployeesModel->getAllSupport();
                $data['count'] = $this->TicketsModel->getCountByOfficer();
                
    	 		return view('components.home', compact('data'));
    	 	} else {
                $nik = session('logged_in')['nik'];
                $data['tickets'] = $this->TicketsModel->getDataByPIC($nik);
                $data['case_type'] = $this->SysLookupValModel->getLookup('CASE_TYPE');
                $data['ticket_status'] = $this->SysLookupValModel->getLookup('TICKET_STATUS');
                $data['ticket_type'] = $this->SysLookupValModel->getLookup('TICKET_TYPE');
                $arrLabel = array(
                    '' => '',
                    0 => 'label label-danger',
                    1 => 'label label-warning',
                    2 => 'label label-success',
                );

    	 		return view('components.users-home', compact('data', 'arrLabel'));
    	 	}
    	 } else {
    	 	return Redirect::to('/login');
    	 }
    	// dd($this->session);die();
    }
}
