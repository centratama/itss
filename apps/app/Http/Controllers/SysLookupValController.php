<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SysLookupValModel;

class SysLookupValController extends Controller
{
    //
    function __construct()
    {
    	$this->SysLookupValModel = new SysLookupValModel;
    }

    public function getLookup($string)
    {
    	$dataLookup = $this->SysLookupValModel->getLookup($string);

    	return $dataLookup;
    }

    public function getLookupById($string, $val)
    {
        $dataLookup = $this->SysLookupValModel->getLookupById($string, $val);

        return $dataLookup;   
    }

    public function getListJson()
    {
        $data = $this->SysLookupValModel->getAll();

        $no = 0;
        foreach ($data as $row) {
            $url_view = url('/parameter/view')."/".$row->id;
            $view = "<a href='{$url_view}' class='btn btn-warning'><i class='fa fa-search'></i></a>";
            $result['data'][] = array(
                    'no' => "<b>".++$no.".</b>",
                    'lookup_type' => $row->lookup_type,
                    'lookup_value' => $row->lookup_value,
                    'lookup_desc' => $row->lookup_desc,
                    'view' => $view,

            );
        }

        if(!empty($result)){
                echo json_encode($result);
        }
    }
}
