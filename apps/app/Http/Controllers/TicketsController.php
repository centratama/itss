<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TicketsModel;
use App\SysLookupValModel;
use App\EmployeesModel;
use App\DepartmentsModel;
use Alert;
use Illuminate\Support\Facades\Session;
use Redirect;
use Mail;

class TicketsController extends Controller
{
    //

    public $emailIT = "it@centratamagroup.com";
    // public $emailIT = "amarfd.apple@gmail.com";

    public function __construct()
    {
    	$this->SysLookupValModel = new SysLookupValModel;
    	$this->TicketsModel = new TicketsModel;
        $this->EmployeesModel = new EmployeesModel;
        $this->DepartmentsModel = new DepartmentsModel;
    }

    public function getListAssets()
    {
        //$asset = Asset::all():
        return view('components/createAssets');
    }

    public function create()
    {
        if (session()->has('logged_in')) {

            $data['ticket_type'] = $this->SysLookupValModel->getLookup('TICKET_TYPE');
            $data['case_type'] = $this->SysLookupValModel->getLookup('CASE_TYPE');
            $data['level'] = $this->SysLookupValModel->getLookup('TICKET_LEVEL');
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            
            return view('components.tickets.create', compact('data'))->with(Session::get('logged_in'));
        } else {

            return Redirect::to('/login');
        }
    }

    public function store(Request $request)
    {
        $ticketId = $this->generateTicketId();
        $name = explode("#", $ticketId);
        // $dir = "/var/www/html/itss/attachments/";
        $dir = "/mainData/data/apps/itss/attachments/";
        $fileName = "";

        if ($request->ajax()) {
            if ($request->file('file') != NULL) {
                $fileName = $name[1].'.'.$request->file('file')->getClientOriginalExtension();
                $request->file('file')->move($dir, $fileName);
            }

            $input = [$fileName, $request->input()];

            $cc = $this->emailIT;
            $subject = "[TT {$ticketId}] {$request->input('subject')}";
            $message = $request->input('message');
            $problem_type = $this->SysLookupValModel->getLookupById('CASE_TYPE', $request->input('problem_type'));

            if (in_array(Session::get('logged_in')['user_role'], array("SUPPORT"))) {
                $getInformer = $this->EmployeesModel->getByNIK($request->input('informer'));
                $getDept = $this->DepartmentsModel->getById($getInformer[0]->id_organization);
                // $getDept = $this->EmployeesModel->getDepartmentEmployee($getInformer[0]->iddept);

                $assigned = Session::get('logged_in')['email'];
                $informer = $getInformer[0]->fullname;

                $data = [
                    "to" => $assigned,
                    "toName" => Session::get('logged_in')['namalengkap'],
                    "subject" => $subject,
                    "informer" => $informer,
                    "informerMail" => $getInformer[0]->email,
                    "cc" => $cc,
                    "case_type" => $problem_type[0]->lookup_desc,
                    "message" => $message,
                    "ticket_id" => $ticketId,
                    "department" =>$getDept[0]->job,
                ];

                $this->sendmail($data, "ticket-created");

            } else {
                $getInformer = $this->EmployeesModel->getByNIK(Session::get('logged_in')['nik']);
                $getDept = $this->DepartmentsModel->getById($getInformer[0]->id_organization);
                // $getDept = $this->EmployeesModel->getDepartmentEmployee($getInformer[0]->iddept);
                $to = Session::get('logged_in')['email'];

                /*
                * Get Email Case PIC
                * @param case_type i.e mail, application, etc
                * @return array mail
                */
                $getMailPIC = $this->EmployeesModel->getEmailPIC($request->input('problem_type'));
                // 
                $data = [
                    "to" => $to,
                    "toName" => Session::get('logged_in')['namalengkap'],
                    "subject" => $subject,
                    "informer" => $getInformer[0]->fullname,
                    "informerMail" => $to,
                    "cc" => $getMailPIC,
                    "case_type" => $problem_type[0]->lookup_desc,
                    "message" => $message,
                    "ticket_id" => $ticketId,
                    "department" =>$getDept[0]->job,
                ];

                $this->sendmail($data, 'ticket-created');
            }

            $save = $this->TicketsModel->saveData($ticketId, $input);

            $data = array('status' => 200, 'ticket_id' => $name[1]);
            alert()->success('Tickets created successfully', 'Congrats!');
            
            return $data;
            
        }
    }

    private function generateTicketId()
    {
        $no = 1;
        $time = "#".date("ymdhs");
        $lastId = $this->TicketsModel->getLastId();
        if (empty($lastId)) {
            $attrId = array($time, $no);
        } else {
            $attrLastId = explode("-", $lastId[0]->id);
            $counter = intval($attrLastId[1]) + 1;
            $attrId = array($time, $counter);
        }
        
        $newId = implode("-", $attrId);

        return $newId;
    }

    public function getListJson()
    {
        $nik = Session::get('logged_in')['nik'];
        if (Session::get('logged_in')['user_role'] == 'SUPPORT') {
            $data = $this->TicketsModel->getAllDataByAssigned($nik);
        } elseif (Session::get('logged_in')['user_role'] == 'ADMIN') {
            $getCaseValPIC = $this->EmployeesModel->getPICCase($nik);
            if (!empty($getCaseValPIC)) {
                foreach ($getCaseValPIC as $key => $value) {
                    $case_value[$key] = $value->case_type_value;
                }
                $values = implode(",", $case_value);
                $data = $this->TicketsModel->getDataByCase($values);
            } else {
                $data = $this->TicketsModel->getAllData();
            }
        } else {
            $data = $this->TicketsModel->getAllData();
        }

        $no = 0;

        $arrLabel = array(
                    0 => 'label label-danger',
                    1 => 'label label-warning',
                    2 => 'label label-success',
                );

        // reverse label for level ticket
        $arrKeys = array_keys($arrLabel);
        $arrValues = array_values($arrLabel);
        $reverseValues = array_reverse($arrValues);
        $reverseArrLabel = array_combine($arrKeys, $reverseValues);

        foreach($data as $row) {
            $getCaseType = $this->SysLookupValModel->getLookupById('CASE_TYPE', $row->case_type);
            $getTicketStatus = $this->SysLookupValModel->getLookupById('TICKET_STATUS', $row->status);
            $getLevel = $this->SysLookupValModel->getLookupById('TICKET_LEVEL', $row->level);

            $status_value = $getTicketStatus[0]->lookup_value;
            $ticket_status = "<span class='{$arrLabel[$status_value]}'>".$getTicketStatus[0]->lookup_desc."</span>";

            $level_value = $getLevel[0]->lookup_value;
            if ($row->level === NULL) {
                $ticket_level = "<span class=''><i>Undefined</i></span>";
            } else {
                $ticket_level = "<span class='{$reverseArrLabel[$level_value]}'>".$getLevel[0]->lookup_desc."</span>";
            }

            $id = explode("#", $row->id);
            $url_view = url('/ticket/view')."/".$id[1];
            $view = "<a href='{$url_view}' class='btn btn-warning'><i class='fa fa-search'></i></a>";

            $result['data'][] = array(
                    'no' => "<b>".++$no.".</b>",
                    'ticket_id' => "<b>".$row->id."</b>",
                    'case_type' => $getCaseType[0]->lookup_desc,
                    'subject' => $row->subject,
                    'status' => $ticket_status,
                    'pic' => $row->namalengkap,
                    'level' => $ticket_level,
                    'created_at' => $row->created_at,
                    'view' => $view,

                );
        }

        if(!empty($result)){
                echo json_encode($result);
        } else {
            $result['data'][] = array(
                    'no' => "no-data",
                    'ticket_id' => "no-data",
                    'case_type' => "no-data",
                    'subject' => "no-data",
                    'status' => "no-data",
                    'pic' => "no-data",
                    'level' => "no-data",
                    'created_at' => "no-data",
                    'view' => "no-data"
                );
            echo json_encode($result);
        }
    }

    public function view($id)
    {
        if (session()->has('logged_in')) {
            $data['ticket'] = $this->TicketsModel->getDataById($id);
            $data['case_type'] = $this->SysLookupValModel->getLookup('CASE_TYPE');
            $data['ticket_status'] = $this->SysLookupValModel->getLookup('TICKET_STATUS');
            $data['ticket_level'] = $this->SysLookupValModel->getLookup('TICKET_LEVEL');
            $data['admin'] = $this->EmployeesModel->getAllSupport();
            $data['other_crew'] = $this->EmployeesModel->getOtherByNIK(Session::get('logged_in')['nik']);
            $data['history'] = $this->TicketsModel->getHistory($id);
            $data['estimation_type'] = $this->SysLookupValModel->getLookup('ESTIMATION_TYPE');
            if ($data['ticket'][0]->is_external) {
                $trans_id = $data['ticket'][0]->ticket_trans_id;
                $data['trans_external'] = $this->TicketsModel->getTransExternal($trans_id);
            }


            $arrLabel = array(
                    0 => 'label label-success',
                    1 => 'label label-warning',
                    2 => 'label label-danger',
                );

            // reverse label for level ticket
            $arrKeys = array_keys($arrLabel);
            $arrValues = array_values($arrLabel);
            $reverseValues = array_reverse($arrValues);
            $reverseArrLabel = array_combine($arrKeys, $reverseValues);

            // Assigned to
            $someone = $data['ticket'][0]->assigned_to;
            if ($someone != NULL) {
                 $credentials= $this->EmployeesModel->getByNIK($someone);
                 $getPerson = $credentials[0]->fullname;
             } else {
                $getPerson = "<u><i>Undefined</i></u>";
             }

            // Button update disable if already have level
             // dd($data['ticket']);
             $isDisabled = '';
            if ($data['ticket'][0]->assigned_to != NULL && in_array(Session::get('logged_in')['user_role'], array('ADMIN', 'SUPPORT'))) {
                if ($data['ticket'][0]->assigned_to != Session::get('logged_in')['nik']) {
                    $isDisabled = 'disabled';
                }
            }

            // Span Level
            $level = $data['ticket'][0]->level;
            if ($level === NULL) {
                $levelLabel = "<u><i>Undefined</i></u>";
            } else {
                $levelDesc = $data['ticket_level'][$level]->lookup_desc;
                $levelLabel = "<span class='$arrLabel[$level]' style='display:block;'>$levelDesc</span>";
            }
            
            // iframe for attachments
            $filename = explode("#", $data['ticket'][0]->attachment);
            $getExtensionFile = explode(".", $data['ticket'][0]->attachment);
            if ($data['ticket'][0]->attachment != NULL) {
                //$fileDir = "/laravel/attachments/".$filename[0];
                $fileDir = "/attachments/".$filename[0];
                if (in_array($getExtensionFile[1], array('png', 'jpeg', 'jpg'))) {
                    $iframe = "<div class='thumbnail'>";
                    $iframe .= "<a href='{$fileDir}' target='_blank'>";
                    $iframe .= "<img src='{$fileDir}' alt='{$filename[0]}'>";
                    $iframe .= "</a>";
                    $iframe .= "</div>";    
                } else {
                    $iframe = "<iframe src='{$fileDir}' width='80%' height='500px'></iframe>";
                }
            } else {
                $iframe = "<span class='label label-primary'>There is no an attachment for this ticket.</span>";
            }

            return view('components.tickets.view', compact('data', 'iframe', 'levelLabel', 'isDisabled', 'getPerson', 'reverseArrLabel'));    
        } else {
            return Redirect::to('/login');
        }
    }

    public function update($id, Request $request)
    {
        if ($request->ajax()) {
            $type = $request->input('type');
            $getTicket = $this->TicketsModel->getDataById($id);
            $getInformer = $this->EmployeesModel->getByNIK($getTicket[0]->created_by);
            
            // Attributes for sendmail
            $data = [];
            $data['informer'] = $getInformer;
            $data['ticket'] = $getTicket;
            $data['to'] = $getInformer[0]->email;
            $data['cc'] = $this->emailIT;
	    if ($getInformer[0]->email == NULL) {
		$data['to'] = $this->emailIT;
            }
            // 
// dd($request->input());
            // Assigned ticket by admin
            if ($type == "assigned") {
                $level = $request->input('level');
                $assigned_to = $request->input('assigned_to');

                $attr = array(
                        "level" => $level,
                        "assigned_to" => $assigned_to,
                    );

                $getCrew = $this->EmployeesModel->getByNIK($assigned_to);
                $getAdmin = $this->EmployeesModel->getByNIK(Session::get('logged_in')['nik']);
                
                $data['subject'] = "[TT {$getTicket[0]->id}] Assigned to {$getCrew[0]->fullname} by {$getAdmin[0]->fullname}";
                $data['crew'] = $getCrew;
                $data['bcc'] = $getCrew[0]->email;
                $this->sendmail($data, 'ticket-assigned');
                $updated = $this->TicketsModel->assignedTicket($id, $attr);

            // Handover ticket by current officer to others
            } else if ($type == "handover") {
                $other_crew = $request->input('other_crew');
                $reason = $request->input('reason');

                $attr = array(
                        "other_crew" => $other_crew,
                        "reason" => $reason,
                    );

                $getCrew = $this->EmployeesModel->getByNIK($other_crew);
                $getCurrent = $this->EmployeesModel->getByNIK(Session::get('logged_in')['nik']);
                
                $data['crew'] = $getCrew;
                $data['subject'] = "[TT {$getTicket[0]->id}] Handover Ticket to {$getCrew[0]->fullname} by {$getCurrent[0]->fullname}";
                $data['bcc'] = $getCrew[0]->email;
                $updated = $this->TicketsModel->handoverTicket($id, $attr);
                $this->sendmail($data, 'ticket-handover');
            
            // Close ticket by current officer
            } else if ($type == "close-ticket") {
                $desc = $request->input('message');
// dd($request->input());
                $attr = array(
                        "message" => $desc,
                    );
                $role = Session::get('logged_in')['user_role'];
                if ($role == "ADMIN") {
                    $attr['level'] = $request->input('level');
                    $attr['nik'] = Session::get('logged_in')['nik'];
                }

                $getCrew = $this->EmployeesModel->getByNIK(Session::get('logged_in')['nik']);

                $data['subject'] = "[TT {$getTicket[0]->id}] Ticket closed.";
                $data['crew'] = $getCrew;

                $this->sendmail($data, 'ticket-close');
                $updated = $this->TicketsModel->closeTicket($id, $attr, $role);

            // Bring this issue to external
            } else {
                $estimation = $request->input('estimation');
                $estimation_type = $request->input('estimation_type');
                $external_side = $request->input('external_side');
                $reason = $request->input('reason');
                $trans_id = $request->input('trans_id');

                $attr = array(
                        "estimation" => $estimation,
                        "estimation_type" => $estimation_type,
                        "external_side" => $external_side,
                        "reason" => $reason,
                        "trans_id" => $trans_id,
                    );

                $updated = $this->TicketsModel->handoverTicketToExternal($id, $attr);

            }

            $data = array('status' => 200);
            alert()->success($updated, 'Congrats!');
            
            return $data;
        }
    }

    public function sendmail($data, $layout)
    {
        // dd($data);
        $to = $data['to'];
        $subject = $data['subject'];
        $cc = $data['cc'];
        Mail::send('emails.'.$layout, ['data' => $data], 
            function($message) use ($data)
            {
                $message->to($data['to']);
                $message->cc($data['cc']);
                $message->subject($data['subject']);
                if (array_key_exists("bcc", $data)) {
                    $message->cc($data['bcc']);
                }
            });
    }

}
