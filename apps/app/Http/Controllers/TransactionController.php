<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\AssetType;
use App\AssetStatus;
use App\AssetVendor;
use App\AssetBrand;
use App\EmployeesModel;
use App\CompanyModel;
use App\DepartmentsModel;
use App\AssetCondition;
use App\AssetLocation;
use App\AssetTransaction;
use App\DivisionsModel;
use App\SignReport;
use App\AssetEmployeeTransaction;

use App\AssetTransactionHeader;
use App\AssetTransactionDetail;

use Alert;
use Illuminate\Support\Facades\Session;
use Redirect;
use Mail;
use PDF;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class TransactionController extends Controller
{
    public function __construct()
    {
    	$this->assetType = new AssetType;
        $this->EmployeesModel = new EmployeesModel;
        $this->assetStatus = new AssetStatus;
        $this->assetCondition = new AssetCondition;
        $this->assetLocation = new AssetLocation;


        $this->assetVendor = new AssetVendor;
        $this->assetBrand = new AssetBrand;
        $this->assetData = new Asset;

        $this->AssetTransaction = new AssetTransaction;
        $this->AssetEmployeeTransaction = new AssetEmployeeTransaction;
        $this->AssetTransactionHeader = new AssetTransactionHeader;
        $this->AssetTransactionDetail = new AssetTransactionDetail;
        
        $this->CompanyModel = new CompanyModel;
        $this->DepartmentsModel = new DepartmentsModel;
        $this->DivisionsModel = new DivisionsModel;
        $this->SignReport = new SignReport;
    }

    //VIEW TRANSACTION LIST
    public function viewTransaction()
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset();
            //$data['transactionJoin'] = $this->AssetTransaction->getAllTransactionJoin();
            $data['assetDatas'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['asset'] = $this->assetData->getAllAsset1();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['transactionData'] = $this->AssetTransaction->getAllTransaction();
            $data['assetEmployee'] = $this->AssetEmployeeTransaction->getAllTransaction();
            //$data['transactionHistory'] = $this->AssetTransaction->getAllHistory();
            //dd($data['assetDataActive']);
            //dd($data['transactionData']);
            //dd($data['assetDatass']);
            return view('components.assets.transactions', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    //VIEW TRANSACTION LIST
    public function viewTransaction1()
    {
        if(Session()->has('logged_in'))
        {

            return view('components.assets.transactions2', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    //ADD TRANSACTION
    public function createTransaction()
    {
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['assetDataAvailable'] = $this->assetData->getAllAssetAvailable();

            //return to view with its data and its current session
            return view('components.assets.createTransaction', compact('data'))->with(Session::get('logged_in'));
        } 
    }

    //ADD TRANSACTION BARU
    public function createTransaction1()
    {
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['assetDataAvailable'] = $this->assetData->getAllAssetAvailable();

            //return to view with its data and its current session
            return view('components.assets.createTransactionHeader', compact('data'))->with(Session::get('logged_in'));
        } 
    }

    //CREATE TRANSACTION DETAIL
    public function createTransactionDetail($idTransaction)
    {
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['assetDataAvailable'] = $this->assetData->getAllAssetAvailable();
            $data['assetDataActive'] = $this->assetData->getAllAssetActive();
            $data['transactionHeader'] = $this->AssetTransactionHeader->getAllTransactionWhere($idTransaction);

            //return to view with its data and its current session
            return view('components.assets.createTransactionDetail', compact('data'))->with('idTransaction', $idTransaction)->with(Session::get('logged_in'));
        }
    }

    //SAVE TRANSACTION DETAIL
    public function saveTransactionDetail(Request $request)
    {
        if($request->ajax())
        {
            $dataTransaction= [$request->input()];

            $transactionHeader = $this->AssetTransactionDetail->saveDataTransaction($dataTransaction);
            alert()->success('Assets created successfully', 'Congrats!');
            return $dataTransaction;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

    //SAVE EDIT
    public function saveEditTransactionDetail(Request $request)
    {
        if($request->ajax())
        {
            $dataTransaction= [$request->input()];

            $transactionHeader = $this->AssetTransactionDetail->saveEditDataTransaction($dataTransaction);
            alert()->success('Assets created successfully', 'Congrats!');
            return $dataTransaction;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

    //SAVE TRANSACTION BARU
    public function saveTransaction1(Request $request)
    {
        if($request->ajax())
        {

            //dd($request->input());
            $dataTransaction= [$request->input()];
            //dd($dataTransaction);

            $transactionHeader = $this->AssetTransactionHeader->saveDataTransaction($dataTransaction);
            alert()->success('Assets created successfully', 'Congrats!');
            return $dataTransaction;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

    //SAVE TRANSACTION BARU
    public function closeTransaction1($idTransaction)
    {
        if($idTransaction)
        {

            $transactionHeader = $this->AssetTransactionHeader->closeDataTransaction($idTransaction);
            alert()->success('Transaction is closed successfully', 'Congrats!');
            return view('components.assets.transactions2')->with(Session::get('logged_in'));
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

    public function cancelTransaction1($idTransaction)
    {
        if($idTransaction)
        {

            $transactionHeader = $this->AssetTransactionHeader->cancelDataTransaction($idTransaction);
            alert()->success('Transaction is closed successfully', 'Congrats!');
            return view('components.assets.transactions2')->with(Session::get('logged_in'));
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

    //EDIT TRANSACTION
    public function editTransaction1($idTransaction)
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['transactionData'] = $this->AssetTransactionHeader->getAllTransactionWhere($idTransaction);

            $status = $data['transactionData'][0]->status;

            if($status == 0)
            {
                return view('components.assets.editTransaction3', compact('data'))->with('idTransaction', $idTransaction)->with(Session::get('logged_in'));
            }

            else
            {
                return view('components.assets.editTransaction4', compact('data'))->with('idTransaction', $idTransaction)->with(Session::get('logged_in'));
                alert()->success('Sorry, you cannot edit this transaction', 'sorry!');
            }

            
        } 
        else 
        {
            return Redirect::to('/login');
        } 
    }

    //RETURN TRANSACTION
    public function returnTransaction($idTransaction)
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $this->AssetTransactionHeader->returnDataTransaction($idTransaction);

            $url = url('transaction');
            return Redirect::to($url);
        } 
        else 
        {
            return Redirect::to('/login');
        } 
    }

    //SAVE TRANSACTION
    public function saveTransaction(Request $request)
    {
        if($request->ajax())
        {

            //dd($request->input());
            $dataTransaction= [$request->input()];
            //dd($dataTransaction);
            $idIFU = $this->AssetTransaction->saveDataTransaction($dataTransaction);

            $data = $this->AssetEmployeeTransaction->saveAddTransactionEmployee($dataTransaction, $idIFU);


            alert()->success('Assets created successfully', 'Congrats!');
            return $dataTransaction;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }


    //EDIT TRANSACTION
    public function editTransaction($idTransaction)
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset1();
            $data['assetDatas'] = $this->assetData->getAllAsset1();
            $data['assetDataAvailable'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['transactionData'] = $this->AssetTransaction->getTransactionWhere($idTransaction);
            $data['transactionDataMonth'] = $this->AssetTransaction->getTransactionWhereMonth($idTransaction);
            $data['department'] = $this->DepartmentsModel->getAllDept();
            $data['division'] = $this->DivisionsModel->getAllDiv();

            return view('components.assets.editTransaction', compact('data'))->with('idTransaction', $idTransaction)->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        } 
    }

    //EDIT TRANSACTION 2
    public function editTransaction2($idTransaction)
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset1();
            $data['assetDatas'] = $this->assetData->getAllAsset1();
            $data['assetDataAvailable'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['transactionData'] = $this->AssetEmployeeTransaction->getAllTransactionWhere01($idTransaction);
            $data['department'] = $this->DepartmentsModel->getAllDept();
            $data['division'] = $this->DivisionsModel->getAllDiv();

            return view('components.assets.editTransaction2', compact('data'))->with('idTransaction', $idTransaction)->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        } 
    }

    //EDIT TRANSACTION 2
    public function editTransactionDetail($idTransaction, $idTransactionHeader)
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset1();
            $data['assetDatas'] = $this->assetData->getAllAsset1();
            $data['assetDataAvailable'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['transactionHeader'] = $this->AssetTransactionHeader->getAllTransactionWhere($idTransactionHeader);
            $data['transactionData'] = $this->AssetTransactionDetail->getAllTransactionWhere1($idTransaction);
            $data['department'] = $this->DepartmentsModel->getAllDept();
            $data['division'] = $this->DivisionsModel->getAllDiv();
            //echo $idTransactionHeader;
            //dd($data['assetDataAvailable']);
            $status = $data['transactionHeader'][0]->status;

            if($status == 0)
            {
                return view('components.assets.editTransactionDetail', compact('data'))->with('idTransaction', $idTransaction)->with('idTransactionHeader', $idTransactionHeader)->with(Session::get('logged_in'));
            }

            else
            {
                return view('components.assets.editTransactionDetail2', compact('data'))->with('idTransaction', $idTransaction)->with('idTransactionHeader', $idTransactionHeader)->with(Session::get('logged_in'));
            }
        } 
        else 
        {
            return Redirect::to('/login');
        } 
    }

    //DELETE
    public function deleteTransactionDetail($idTransaction)
    {
        if(Session()->has('logged_in'))
        {

            $idTransactionHeader = $this->AssetTransactionDetail->deleteDataTransaction($idTransaction);

            $data['assetData'] = $this->assetData->getAllAsset1();
            $data['assetDatas'] = $this->assetData->getAllAsset1();
            $data['assetDataAvailable'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['transactionData'] = $this->AssetTransactionHeader->getAllTransactionWhere($idTransactionHeader);
            $data['department'] = $this->DepartmentsModel->getAllDept();
            $data['division'] = $this->DivisionsModel->getAllDiv();
            //dd($data['employee']);

            $url = url('transaction/detailTransaction/'.$idTransactionHeader);
            return Redirect::to($url);
            //view('components.assets.transactionDetail', compact('data'))->with('idTransaction', $idTransaction)->with(Session::get('logged_in'));
            alert()->success('Assets created successfully', 'Congrats!');
        } 
        else 
        {
            return Redirect::to('/login');
        } 
    }

       //EDIT TRANSACTION 2
    public function returningAsset(Request $request)
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $dataTransaction= [$request->input()];
            $this->AssetEmployeeTransaction->returningAsset($dataTransaction);

            return $dataTransaction;
        } 
        else 
        {
            return Redirect::to('/login');
        } 
    }

    //SAVE EDIT TRANSACTION
    public function saveEditTransaction(Request $request)
    {
        if($request->ajax())
        {
            //dd($request->input());
            $dataTransaction= [$request->input()];

            $this->AssetTransaction->updateDataTransaction($dataTransaction);
            
            alert()->success('Assets update successfully', 'Congrats!');
            return $dataTransaction;
        }

        else
        {
            alert()->success('Asset is not updated successfully', 'Sorry!');
        }
    }

    //SAVE EDIT TRANSACTION
    public function saveEditTransaction1(Request $request)
    {
        if($request->ajax())
        {
            //dd($request->input());
            $dataTransaction= [$request->input()];

            $this->AssetTransactionHeader->updateDataTransaction1($dataTransaction);
            
            alert()->success('Assets update successfully', 'Congrats!');
            return $dataTransaction;
        }

        else
        {
            alert()->success('Asset is not updated successfully', 'Sorry!');
        }
    }

    //SAVE EDIT TRANSACTION
    public function detailTransaction($idTransaction)
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset1();
            $data['assetDatas'] = $this->assetData->getAllAsset1();
            $data['assetDataAvailable'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['transactionData'] = $this->AssetTransactionHeader->getAllTransactionWhere($idTransaction);
            $data['department'] = $this->DepartmentsModel->getAllDept();
            $data['division'] = $this->DivisionsModel->getAllDiv();

            //dd($data['employee']);

            return view('components.assets.transactionDetail', compact('data'))->with('idTransaction', $idTransaction)->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        } 
    }

    //SIGN REPORT
    public function signReport()
    {
        if(Session()->has('logged_in'))
        {
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['allSign'] = $this->SignReport->getAllSign();
            return view('components.assets.verifiedby', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    //VIEW ASSETS LIST
    public function saveSignReport(Request $request)
    {
        if($request->ajax())
        {
            //dd($request->input());
            $data= [$request->input()];

            $this->SignReport->saveSignReport($data);
            

            alert()->success('Assets update successfully', 'Congrats!');
            return $data;
        }

        else
        {
            alert()->success('Asset is not updated successfully', 'Sorry!');
        }
    }

    //VIEW ASSETS LIST
    public function reportAssets()
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset();
            $data['assetDatas'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();

            //dd($data['assetData']);
            return view('components.assets.assetReport', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    //PRINT DOCUMENT
    public function printTransactionReport($idTransaction)
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset1();
            $data['assetDatas'] = $this->assetData->getAllAsset1();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['transactionHeader'] = $this->AssetTransactionHeader->getAllTransactionWhere($idTransaction);
            $data['transactionDetailParent'] = $this->AssetTransactionDetail->getAllTransactionWhereParent($data['transactionHeader'][0]->id);
            $data['transactionDetailChild'] = $this->AssetTransactionDetail->getAllTransactionWhereChild($data['transactionHeader'][0]->id);
            $data['department'] = $this->DepartmentsModel->getAllDept();
            $data['division'] = $this->DivisionsModel->getAllDiv();
            $data['allSign'] = $this->SignReport->getAllSign();

            //dd($data['transactionHeader']);
            //dd($data['assetType']);
            //dd($data['assetData']);
            //dd($data['transactionDetail']);
            //dd($data['transactionHeader'][0]->id);
            //dd($data['transactionDetailChild']);

            return view('components.assets.assetReport', compact('data'))->with('idTransaction', $idTransaction)->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        } 
    }

    //SAVE TRANSACTION
    public function addTransactionEmployee($idTransaction, $idEmployee)
    {
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['assetDataAvailable'] = $this->assetData->getAllAssetAvailable();

            //return to view with its data and its current session
            return view('components.assets.addTransaction', compact('data'))->with('idTransaction', $idTransaction)->with('idEmployee', $idEmployee)->with(Session::get('logged_in'));
        } 
    }

    //SAVE TRANSACTION
    public function saveAddTransactionEmployee(Request $request)
    {
        if($request->ajax())
        {

            //dd($request->input());
            $dataTransaction= [$request->input()];

            //dd($dataTransaction);
            $this->AssetEmployeeTransaction->saveAddTransactionEmployee($dataTransaction);

            alert()->success('Assets created successfully', 'Congrats!');
            return $dataTransaction;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }


    //SAVE TRANSACTION
    public function saveAddTransaction(Request $request)
    {
        if($request->ajax())
        {

            //dd($request->input());
            $dataTransaction= [$request->input()];
            //dd($dataTransaction);
            $this->AssetEmployeeTransaction->saveAddTransaction1($dataTransaction);

            alert()->success('Assets created successfully', 'Congrats!');
            return $dataTransaction;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

    //END LOCATION

    public function downloadPDF($idTransaction)
    {
         //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset1();
            $data['assetDatas'] = $this->assetData->getAllAsset1();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();
            $data['transactionHeader'] = $this->AssetTransactionHeader->getAllTransactionWhere($idTransaction);
            $data['transactionDetailParent'] = $this->AssetTransactionDetail->getAllTransactionWhereParent($data['transactionHeader'][0]->id);
            $data['transactionDetailChild'] = $this->AssetTransactionDetail->getAllTransactionWhereChild($data['transactionHeader'][0]->id);
            $data['department'] = $this->DepartmentsModel->getAllDept();
            $data['division'] = $this->DivisionsModel->getAllDiv();
            $data['allSign'] = $this->SignReport->getAllSign();



        $pdf = PDF::loadView('components.assets.assetReportDownload',  array('data' => $data))->setPaper('A4');
        return $pdf->download('transaction.pdf');
    }


    public function getListJson()
    {

        $nik = Session::get('logged_in')['nik'];
        $data = $this->AssetTransactionHeader->getAllTransaction();

        foreach($data as $row) {

            $location = $this->assetLocation->getAssetLocationWhere($row->id_location);
            $status = $this->assetStatus->getAssetStatusWhere($row->id_status);
            $url_detail = url('/transaction/detailTransaction/'.$row->id);
            $url_edit = url('/transaction/editTransaction1/'.$row->id);
            $url_close = url('/transaction/closeTransaction1/'.$row->id);
            $url_cancel = url('/transaction/cancelTransaction1/'.$row->id);
            $url_print = url('/transaction/transactionReport/'.$row->id);
            $url_return = url('/transaction/returnTransaction/'.$row->id);
            $employee = $this->EmployeesModel->getByNIK($row->nik_undertakenby);
            $employeename = $employee[0]->fullname;

            $requester = $this->EmployeesModel->getByNIK($row->nik_requestedby);
            $requestername = $employee[0]->fullname;

            if($row->status == 0)
            {
                $statusDocument = "draft";
                $view = "<html>
                <div class='dropdown'>
                  <button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                    tools
                  </button>
                  <ul class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
                    <li><a href='{$url_edit}' class='dropdown-item'>
                        EDIT
                    </a></li>
                    <li><a href='{$url_detail}' class='dropdown-item'>
                        DETAIL
                    </a></li>
                    <li><a href='{$url_close}' class='dropdown-item'>
                        CLOSE
                    </a></li>
                    <li><a href='{$url_cancel}' class='dropdown-item'>
                        CANCEL
                    </a></li>
                  </ul>
                </div>
                </html>";
            }

            else if($row->status == 1)
            {
                $statusDocument = "close";
                $view = "<html>
                <div class='dropdown'>
                  <button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                    tools
                  </button>
                  <ul class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
                    <li><a href='{$url_edit}' class='dropdown-item'>
                        VIEW
                    </a></li>
                    <li><a href='{$url_detail}' class='dropdown-item'>
                        DETAIL
                    </a></li>
                    <li><a href='{$url_print}' class='dropdown-item'>
                        PRINT
                    </a></li>
                  </ul>
                </div>
                </html>";
            }

            else if($row->status == 2)
            {
                $statusDocument = "cancel";
                $view = "<html>
                <div class='dropdown'>
                  <button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                    tools
                  </button>
                  <ul class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
                    <li><a href='{$url_edit}' class='dropdown-item'>
                        VIEW
                    </a></li>
                    <li><a href='{$url_detail}' class='dropdown-item'>
                        DETAIL
                    </a></li>
                    <li><a href='{$url_print}' class='dropdown-item'>
                        PRINT
                    </a></li>
                  </ul>
                </div>
                </html>";
            }

            else if($row->status == 3)
            {
                $statusDocument = "RETURN";
                $view = "<html>
                <div class='dropdown'>
                  <button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                    tools
                  </button>
                  <ul class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
                    <li><a href='{$url_edit}' class='dropdown-item'>
                        VIEW
                    </a></li>
                    <li><a href='{$url_detail}' class='dropdown-item'>
                        DETAIL
                    </a></li>
                    <li><a href='{$url_print}' class='dropdown-item'>
                        PRINT
                    </a></li>
                  </ul>
                </div>
                </html>";
            }

            

            $result['data'][] = array(
                'id' => $row->id,
                'nik_requestedby' => $row->nik_requestedby."<br>".$requestername,
                'nik_undertakenby' => $row->nik_undertakenby."<br>".$employeename,
                'transactionStatus' => $status[0]->statusName,
                'documentStatus' => $statusDocument,
                'location' => $location[0]->locationName,
                'transactiondate' => $row->transactiondate,
                'description' => $row->description,
                'tools' => $view
                    //'embeded' => $embeded,
            );
        }

        if(!empty($result)){
            //dd($result);
            return json_encode($result, JSON_UNESCAPED_SLASHES);
        } else {
            //dd($result);
            $result['data'][]= array(
                'id' => "no data",
                'nik_requestedby' => "no data",
                'nik_undertakenby' => "no data",
                'transactionStatus' => "no data",
                'documentStatus' => "no data",
                'location' => "no data",
                'transactiondate' => "no data",
                'description' => "no data",
                'tools' => "no data",
            );
            return json_encode($result);
        }
    }

    public function getListJsonDetail($idTransaction)
    {

        $nik = Session::get('logged_in')['nik'];

        // reverse label for level ticket
        $data = $this->AssetTransactionDetail->getAllTransactionWhere($idTransaction);
        //echo $idTransaction;
        //dd($data);


        foreach($data as $row) {
            $condition = $this->assetCondition->getAssetConditionWhere($row->id_asset_condition);
            $asset = $this->assetData->getAllAssetWhere($row->id_asset);
            $brand = $this->assetBrand->getAssetBrandWhere($asset[0]->id_brands);
            $status = $this->AssetTransactionHeader->getAllTransactionWhere($idTransaction);
            $status = $status[0]->status;

            //dd($asset);

            $url_edit = url('/transaction/editTransactionDetail/'.$row->id.'/'.$row->id_transaction_header);
            $url_delete = url('/transaction/deleteTransactionDetail/'.$row->id);

            if($row->id_parent == NULL)
            {

                if($status == 1 OR $status == 2)
                {
                    $view = "<html>
                    <a href='{$url_edit}'>
                    <i class='fa fa-pencil' style='font-size:15px;color:blue'></i>
                    </a>
                    </html>";
                }

                else
                {
                     $view = "<html>
                    <a href='{$url_edit}'>
                    <i class='fa fa-pencil' style='font-size:15px;color:blue'></i>
                    </a>
                    <a href='{$url_delete}'>
                    <i class='fa fa-trash' style='font-size:15px;color:red'></i>
                    </a>
                    </html>";
                }
            }

            else
            {
                $view = "<html>
                <a href='{$url_edit}'>
                <i class='fa fa-pencil' style='font-size:15px;color:blue'></i>
                </a>
                </html>";  
            }

            
            

            $result['data'][] = array(
                'id' => $row->line_number,
                'id_asset' => $row->id_asset,
                'asset_name' => $asset[0]->name,
                'id_parent' => $row->id_parent,
                'brand_name' => $brand[0]->brandName,
                'asset_condition' => $condition[0]->conditionName,
                'description' => $row->description_detail,
                'tools' => $view
                    //'embeded' => $embeded,
            );
        }

        if(!empty($result)){
            //dd($result);
            return json_encode($result, JSON_UNESCAPED_SLASHES);
        } else {
            //dd($result);
            $result['data'][]= array(
                'id' => 'no data',
                'id_asset' => 'no data',
                'asset_name' => 'no data',
                'id_parent' => 'no data',
                'brand_name' => 'no data',
                'asset_condition' => 'no data',
                'description' => 'no data',
                'tools' => 'no data'
            );
            return json_encode($result);
        }
    }

    public function getListJsonHistory($idAsset)
    {

        $nik = Session::get('logged_in')['nik'];

        // reverse label for level ticket
        $transactionDetail = $this->AssetTransactionDetail->getAllHistory($idAsset);
        //$idTransaction = $transactionDetail[0]->id_transaction_header;

        $number = 1;
        

        foreach($transactionDetail as $row) {

            $transactionHeader = $this->AssetTransactionHeader->getAllTransactionWhere($row->id_transaction_header);

            $employee = $this->EmployeesModel->getByNIK($transactionHeader[0]->nik_undertakenby);
            $employeename = $employee[0]->fullname;
            $transactiondate = $transactionHeader[0]->transactiondate;
            $statusDocument = $transactionHeader[0]->status;
            if($statusDocument == 0)
            {
                $statusDocumentName = "DRAFT";
            }

            else if($statusDocument == 1)
            {
                $statusDocumentName = "CLOSE";
            }

            else
            {
                $statusDocumentName = "CANCEL";
            }
            $idLocation = $transactionHeader[0]->id_location;
            $assetLocation = $this->assetLocation->getAssetLocationWhere($idLocation);
            $idCondition = $row->id_asset_condition;
            $assetCondition = $this->assetCondition->getAssetConditionWhere($idCondition);

            $result['data'][] = array(
                'number' => "user ke-".$number,
                'nik' => $transactionHeader[0]->nik_undertakenby,
                'employee' => $employeename,
                'documentStatus' => $statusDocumentName,
                'assetCondition' => $assetCondition[0]->conditionName,
                'assetLocation' => $assetLocation[0]->locationName,
                'transactionDate' => $transactiondate,
                    //'embeded' => $embeded,
            );
            $number++;
        }

        if(!empty($result)){
            return json_encode($result, JSON_UNESCAPED_SLASHES);
        } else {
            $result['data'][]= array(
                'number' => 'no data',
                'nik' => 'no data',
                'employee' => 'no data',
                'assetCondition' => 'no data',
                'assetLocation' => 'no data',
                'transactionDate' => 'no data',
            );
            return json_encode($result);
        }
    }


}
