<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Session\Store;
use App\UsersModel;
use App\EmployeesModel;
use Alert;
use Redirect;
use Validator;

class UsersController extends Controller
{
    //

    function __construct()
    {
    	$this->UsersModel = new UsersModel;
        $this->EmployeesModel = new EmployeesModel;
    	$this->session = Session::get('logged_in');
    }

    public function actionRegister(Request $request)
    {

    	if ($request->ajax()) {
    		$data = array(
    			"nik" => $request->nik,
    			"username" => $request->username,
    			"password" => $request->password,
    			"confirm_pass" => $request->confirm_pass
    			);

    		// validasi nik terdaftar atau tidak
    		// $getEmployee = $this->UsersModel->validateNIK($data['nik']);
            $getEmployee = $this->EmployeesModel->getByNIK($data['nik']);
    		if (empty($getEmployee)) {
    			alert()->error("NIK tidak terdaftar.", "Error");
    		} else {
    			if ($data['password'] != $data['confirm_pass']) {
    				alert()->error("Password does not match.", "Error");
    			} else {
    				// create user
    				$userCreated = $this->UsersModel->createUser($data, $getEmployee);
    				if ($userCreated == false) {
    					alert()->error("You have already registered.", "Error");
    				} else {
    					alert()->success("You have successfully registered.", "Success");
    				}
    			}
    		}
    		
    		return $data;
    	}
    }

    public function doLogin (Request $request)
    {
    	$data = $request->input('users');
    	$credentials = $this->UsersModel->validateLogin($data);
    	// dd($credentials);
    	if (empty($credentials)) {
    		alert()->error("These credentials do not match our records", "error");
    	} else {
            $getDataEmployee = $this->EmployeesModel->getByNIK($credentials[0]->nik);
    		Session::put('logged_in.id', $credentials[0]->id);
    		Session::put('logged_in.app_name', $credentials[0]->app_name);
    		Session::put('logged_in.username', $credentials[0]->username);
    		Session::put('logged_in.password', $credentials[0]->password);
    		Session::put('logged_in.user_role', $credentials[0]->user_role);
    		Session::put('logged_in.nik', $credentials[0]->nik);
    		Session::put('logged_in.plain', $credentials[0]->plain);
    		Session::put('logged_in.namalengkap', $getDataEmployee[0]->fullname);
    		Session::put('logged_in.email', $getDataEmployee[0]->email);
    		// Session::put('logged_in.id_position', $credentials[0]->id_position);
    		// Session::put('logged_in.position', $credentials[0]->position);
    		// Session::put('logged_in.id_dept', $credentials[0]->id_dept);
    		// Session::put('logged_in.department', $credentials[0]->department);
            Session::save();
        }
    		return Redirect::to('/');
    }

    public function doLogout(Request $request)
    {
    	$request->session()->flush();

    	return Redirect::to('/login');
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(),[
                'email' => 'required',
                'password' => 'required',
                'password_confirmation' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('password/reset')
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $email = $request->input('email');
            $password = $request->input('password');
            $password_confirmation = $request->input('password_confirmation');

            $attr = $request->all();

            if ($password != $password_confirmation) {
                alert()->error("Password does not match.", "Error");
            } else {
                $getEmployee = $this->EmployeesModel->getByEmail($email);
                if (!empty($getEmployee)) {
                    $getUser = $this->UsersModel->getByNIK($getEmployee[0]->number);
		    $attr['nik'] = $getEmployee[0]->number;

                    if (empty($getUser)) {
                        alert()->error("Tidak ada pengguna dengan NIK tersebut.", "Error");
                    } else {
                        // $isRegist = $getDataEmployee[0]->is_regist;

                        // if ($isRegist == 1) {
                            $this->UsersModel->resetPassword($attr);
                            alert()->success("You have successfully change your password.", "Success");
                        // } else {
                            // alert()->error("You have not registered yet.", "Error");
                        // }
                    }
                } else {
                    alert()->error("Email does not registered.", "Error");
                }
            }

            return redirect('password/reset');
        }
    }
}
