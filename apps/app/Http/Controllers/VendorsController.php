<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\AssetType;
use App\AssetStatus;
use App\AssetVendor;
use App\AssetBrand;
use App\EmployeesModel;
use App\DepartmentsModel;
use App\AssetCondition;
use App\AssetLocation;
use App\assetTransaction;
use Alert;
use Illuminate\Support\Facades\Session;
use Redirect;
use Mail;

class VendorsController extends Controller
{
    public $emailIT = "it@centratamagroup.com";
    // public $emailIT = "amarfd.apple@gmail.com";

    public function __construct()
    {
        $this->assetType = new AssetType;
        $this->assetStatus = new AssetStatus;
        $this->assetVendor = new AssetVendor;
        $this->assetBrand = new AssetBrand;
        $this->assetData = new Asset;
        $this->assetCondition = new AssetCondition;
        $this->assetLocation = new AssetLocation;
        $this->assetTransaction = new assetTransaction;
        $this->EmployeesModel = new EmployeesModel;
        $this->DepartmentsModel = new DepartmentsModel;
    }


    //VIEW VENDOR
    public function viewVendors()
    {
        if(Session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            $data['employee'] = $this->EmployeesModel->getAllEmployee();

            //dd($data['assetData']);
            return view('components.vendors.vendors', compact('data'))->with(Session::get('logged_in'));
        } 
        else 
        {
            return Redirect::to('/login');
        }
    }

    //CREATE VENDOR
    public function createVendor()
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            $data['assetData'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendor();
            $data['assetBrand'] = $this->assetBrand->getAssetBrand();
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();

            //return to view with its data and its current session
            return view('components.vendors.createVendors', compact('data'))->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //EDIT VENDOR
    public function editVendor($idVendor)
    {
        //checking if user logged in or not
        if (session()->has('logged_in'))
        {
            //getting data from each model required
            //$data['assetData'] = $this->assetData->getAllAssetWhere();
            $data['assetDatas'] = $this->assetData->getAllAsset();
            $data['assetType'] = $this->assetType->getAssetType();
            $data['assetStatus'] = $this->assetStatus->getAssetStatus();
            $data['assetVendor'] = $this->assetVendor->getAssetVendorWhere($idVendor);
            $data['assetCondition'] = $this->assetCondition->getAssetCondition();
            $data['assetLocation'] = $this->assetLocation->getAssetLocation();
            //return to view with its data and its current session
            return view('components.vendors.editVendors', compact('data'))->with('idVendor', $idVendor)->with(Session::get('logged_in'));
        } 

        //if user not logged in yet
        else 
        {
            //back to the login form
            return Redirect::to('/login');
        }
    }

    //SAVE EDIT VENDOR
    public function saveEditVendor(Request $request, $idVendor)
    {
        if($request->ajax())
        {
            $dataVendors= [$request->input()];
            //dd($dataAssets);
            $this->assetVendor->updateDataVendor($dataVendors, $idVendor);

            alert()->success('your update successfully', 'Congrats!');
            return $dataVendors;
        }

        else
        {
            alert()->success('Asset is not updated successfully', 'Sorry!');
        }
    }

    //SAVE VENDOR
    public function saveVendor(Request $request)
    {
        if($request->ajax())
        {
            $dataVendors= [$request->input()];
            //dd($dataAssets);
            $this->assetVendor->saveDataVendor($dataVendors);

            alert()->success('Assets created successfully', 'Congrats!');
            return $dataVendors;
        }

        else
        {
            alert()->success('Asset is not created successfully', 'Sorry!');
        }
    }

    public function getListJson()
    {

        $nik = Session::get('logged_in')['nik'];

        // reverse label for level ticket
        $data = $this->assetVendor->getAssetVendor();

        foreach($data as $row) {

            $url_editVendor = url('/asset/vendors/editVendor/'.$row->id);
            $view = "<html>
                        <a href='{$url_editVendor}'>
                            <i class='fa fa-pencil' style='font-size:15px;color:blue'></i>
                        </a>
                    </html>";

            $result['data'][] = array(
                    'idVendor' => $row->id,
                    'vendorName' => $row->vendorName,
                    'vendorDesc' => $row->vendorDescription,
                    'tool' => $view,
                    //'embeded' => $embeded,
                );
            //dd($result);
        }

        if(!empty($result)){
            //dd($result);
                return json_encode($result, JSON_UNESCAPED_SLASHES);
        } else {
            //dd($result);
            $result['data'][]= array(
                    'idVendor' => 'no data',
                    'vendorName' => 'no data',
                    'vendorDesc' => 'no data',
                    'tool' => 'no data',
                );
            return json_encode($result);
        }
    }

}
