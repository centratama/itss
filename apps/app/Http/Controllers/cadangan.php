 public function getListJson()
    {
        /*$data['assetData'] = $this->assetData->getAllAsset1();
        $data['assetDatas'] = $this->assetData->getAllAsset1();
        $data['assetType'] = $this->assetType->getAssetType();
        $data['assetStatus'] = $this->assetStatus->getAssetStatus();
        $data['assetVendor'] = $this->assetVendor->getAssetVendor();
        $data['assetBrand'] = $this->assetBrand->getAssetBrand();
        $data['assetCondition'] = $this->assetCondition->getAssetCondition();
        $data['assetLocation'] = $this->assetLocation->getAssetLocation();
        $data['employee'] = $this->EmployeesModel->getAllEmployee();*/

        $nik = Session::get('logged_in')['nik'];

        // reverse label for level ticket
        $data = $this->assetData->getAllAsset1();

        $arrLabel = array(
                    0 => 'idAsset',
                    1 => 'name',
                    2 => 'serial_number',
                    3 => 'brand',
                    4 => 'vendor',
                );

        foreach($data as $row) {
            /*$embeded = array();
            foreach ($data as $row2) {
                $url_delete = url('/asset/child/deleteAssetChild/'.$row2->idAsset);
                $brand = $this->assetBrand->getAssetBrandWhere($row2->id_brands);
                $vendor = $this->assetVendor->getAssetVendorWhere($row2->id_vendors);
                if($row->idAsset == $row2->id_parent){
                    $embeded[] = array(

                        'idAsset' => $row2->idAsset,
                        'name' => $row2->name,
                        'serial_number' => $row2->serial_number,
                        'brand' => $brand[0]->brandName,
                        'tool' =>   "<html>
                                        <a href='{$url_delete}'>
                                            <i class='fa fa-close' style='font-size:15px;color:red'></i>
                                        </a>
                                    </html>",
                        //'vendor' => $vendor[0]->vendorName,
                    );
                }
            }*/
            //dd($embeded);


            $url_add = url('/asset/addTransaction/'.$row->idAsset);
            $url_edit = url('/asset/editAsset/'.$row->idAsset);
            $url_detail = url('/asset/detailAsset/'.$row->idAsset);
            $url_history = url('/asset/transactionHistory/'.$row->idAsset);
            $url_berlac = url('http://assets.centratamagroupdev.com/itss/'.$row->file_html);
            $view = "<html>
                        <a href='{$url_add}'>
                            <i class='fa fa-plus-circle' style='font-size:15px;color:blue'></i>
                        </a>
                        |
                        <a href='{$url_edit}'>
                            <i class='fa fa-pencil' style='font-size:15px;color:blue'></i>
                        </a>
                        |
                        <a href='{$url_detail}'>
                            <i class='fa fa-search' style='font-size:15px;color:blue'></i>
                        </a>
                        |
                        <a href='{$url_history}'>
                            <i class='fa fa-book' style='font-size:15px;color:blue'></i>
                        </a>
                        |
                        <a href='{$url_berlac}'>
                            <i class='fa fa-link' style='font-size:15px;color:blue'></i>
                        </a>
                        </html>";
            $brand = $this->assetBrand->getAssetBrandWhere($row->id_brands);
            $vendor = $this->assetVendor->getAssetVendorWhere($row->id_vendors);
            $result['data'][] = array(
                    'idAsset' => $row->idAsset,
                    'name' => $row->name,
                    'serial_number' => $row->serial_number,
                    'brand' => $brand[0]->brandName,
                    'vendor' => $vendor[0]->vendorName,
                    'tool' => $view,
                    //'embeded' => $embeded,
                );
            //dd($result);
        }

        if(!empty($result)){
            //dd($result);
                return json_encode($result, JSON_UNESCAPED_SLASHES);
        } else {
            //dd($result);
            $result['data'][]= array(
                    'idAsset' => 'no data',
                    'name' => 'no data',
                    'serial_number' => 'no data',
                    'brand' => 'no data',
                    'vendor' => 'no data',
                    'tool' => 'no data',
                );
            return json_encode($result);
        }
    }