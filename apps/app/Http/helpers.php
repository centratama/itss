<?php

function cleanString($string)
{
	$patterns = array();
	$patterns[0] = "/'/";

	$replacements = array();
	$replacements[0] = "";

	$cleanString = preg_replace($patterns, $replacements, $string);

	return $cleanString;
}