<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SignReport extends Model
{

    //idStatus 1 = BORROW
    //idStatus 2 = RETURN
    //idStatus 3 = DISPOSAL
    protected $table="itss_asset_employesign";

    public function getAllSign()
    {
        $data = SignReport::all();
        return $data;
    }

    public function saveSignReport($data)
    {
        $verifiedby = $data[0]['verifiedby'];
        $acknowledgedby = $data[0]['acknowledgedby'];

        $data = SignReport::where('id', 3)
            ->update(['verifiedby' => $verifiedby, 'acknowledgedby' => $acknowledgedby]);

        return $data;
    }

}
