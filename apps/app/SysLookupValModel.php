<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SysLookupValModel extends Model
{
    //
    public function getLookup($string)
    {
    	$sql = "SELECT * FROM sys_lookup_values WHERE lookup_type = '{$string}';";

    	$data = DB::SELECT($sql);

    	return $data;
    }

    public function getLookupById($string, $val)
    {
    	$sql = "SELECT * FROM sys_lookup_values WHERE lookup_type = '{$string}' AND lookup_value = '{$val}';";

    	$data = DB::SELECT($sql);

    	return $data;	
    }

    function getAll()
    {
        $sql = "SELECT * FROM sys_lookup_values;";

        $data = DB::SELECT($sql);

        return $data;   
    }

}
