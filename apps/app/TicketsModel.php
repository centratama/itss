<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EmployeesModel;
use DB;
use Session;

class TicketsModel extends Model
{
    //
    public function getLastId()
    {
    	$sql = "SELECT id FROM {$this->prefix()}_tickets ORDER BY created_at DESC LIMIT 1;";

    	$data = DB::SELECT($sql);

    	return $data;
    }

    public function saveData($ticketId, $data)
    {
    	$nik = $data[1]['nik'];
    	$subject = cleanString($data[1]['subject']);
    	$message = cleanString($data[1]['message']);
    	$ticket_type = $data[1]['ticket_type'];
    	$case_type = $data[1]['problem_type'];
        $level = $data[1]['level'];

        // if ticket created by Support or Admin
        $informer = $data[1]['informer'];
    	// 

        if (empty($data[0])) {
            $fileName = "NULL";
        } else {
            $fileName = "'".$data[0]."'";
        }

        if (in_array(Session::get('logged_in')['user_role'], array("SUPPORT", "ADMIN"))) {
            $created_by = $informer;
        } else {
            $created_by = $nik;
        }

            
        $sql = "INSERT INTO {$this->prefix()}_tickets 
                (id, ticket_type, case_type, subject, message, attachment, created_at, created_by)
                VALUES
                ('{$ticketId}', '{$ticket_type}', '{$case_type}', '{$subject}', '{$message}', {$fileName}, NOW(), '{$created_by}');";

        if (DB::INSERT($sql)) {
            
            $sql = "INSERT INTO {$this->prefix()}_tickets_trans
                    (ticket_id, start_date) VALUES ('{$ticketId}', NOW());";

            DB::INSERT($sql);
        }

        // Tambahan jika yang buat SUPPORT
        if (Session::get('logged_in')['user_role'] == "SUPPORT") {
            $sql = "UPDATE {$this->prefix()}_tickets SET
                    level = '{$level}', status = '1', 
                    updated_at = NOW()
                    WHERE id = '{$ticketId}';";
            DB::UPDATE($sql);

            $sql = "UPDATE {$this->prefix()}_tickets_trans SET
                    assigned_to = '{$nik}', start_date = NOW()
                    WHERE ticket_id = '{$ticketId}' AND is_active = '1';";
            DB::UPDATE($sql);            
        }
        // 

    	return true;
    }

    public function getAllData()
    {
        $sql = "SELECT a.*, b.*, c.assigned_to, c.start_date, c.finish_date FROM {$this->prefix()}_tickets a,
                {$this->prefix()}_employee b,
                {$this->prefix()}_tickets_trans c 
                WHERE a.ticket_type = '0'
                AND a.created_by = b.nik
                AND a.id = c.ticket_id
                AND c.is_active = 1
                ORDER BY a.status ASC, a.created_at DESC;";

        $data = DB::SELECT($sql);

        return $data;
    }

    public function getDataById($id)
    {
        $employee = new EmployeesModel;

        $sql = "SELECT a.*, c.* FROM {$this->prefix()}_tickets a,
                -- {$this->prefix()}_employee b,
                {$this->prefix()}_tickets_trans c 
                WHERE a.ticket_type = '0'
                -- AND a.created_by = b.nik
                AND a.id = c.ticket_id
                AND a.id = '#{$id}'
                AND c.is_active = 1 
                -- AND c.description IS NULL 
                ORDER BY a.created_at DESC;";

        $data = DB::SELECT($sql);

        $informer = $employee->getByNIK($data[0]->created_by);

        if (empty($informer)) {
            $data[0]->informer = NULL;
        } else {
            $data[0]->informer = $informer[0]->fullname;
        }
        
        return $data;   
    }

    // TODO: Update this query by last status
    public function getDataByPIC($nik)
    {
        $employee = new EmployeesModel;
           
        $sql = "SELECT a.*, c.* FROM {$this->prefix()}_tickets a 
                INNER JOIN {$this->prefix()}_tickets_trans c ON a.id = c.ticket_id
                -- LEFT JOIN {$this->prefix()}_employee d ON c.assigned_to = d.nik
                WHERE a.created_by = '$nik' 
                AND c.is_active = 1
                -- AND c.description IS NULL
                ORDER BY a.created_at ASC;";

        $data = DB::SELECT($sql);

        foreach ($data as $key => $value) {
            $informer = $employee->getByNIK($value->assigned_to);

            if (empty($informer)) {
                $data[$key]->pic = NULL;
            } else {
                $data[$key]->pic = $informer[0]->fullname;
            }    
        }

        return $data;
    }

    // TODO: so messy for an update and handover ticket
    public function updateData($post)
    {
        $id = $post['id'];

        if ($post['type'] == 'assigned') {
            $level = $post['level'];
            $assigned_to = $post['assigned_to'];

            $sql = "UPDATE {$this->prefix()}_tickets_trans SET
                    assigned_to = '{$assigned_to}', start_date = NOW()
                    WHERE ticket_id = '{$id}';";
            DB::UPDATE($sql);

            $sql = "UPDATE {$this->prefix()}_tickets SET
                    level = '{$level}', status = 1, updated_at = NOW()
                    WHERE id = '{$id}';";

            DB::UPDATE($sql);

            $message = "Tickets updated successfully";

        } else if ($post['type'] == 'handover') { // if type = handover
            $other_crew = $post['other_crew'];
            $reason = cleanString($post['reason']);

            $sql = "UPDATE {$this->prefix()}_tickets SET
                    updated_at = NOW()
                    WHERE id = '{$id}';";
            DB::UPDATE($sql);

            $sql = "UPDATE {$this->prefix()}_tickets_trans SET
                    is_active = '0', description = '{$reason}', finish_date = NOW()
                    WHERE ticket_id = '{$id}' AND is_active = 1;";
            DB::UPDATE($sql);

            $sql = "INSERT INTO {$this->prefix()}_tickets_trans
                    (ticket_id, assigned_to, start_date)
                    VALUES ('{$id}', '{$other_crew}', NOW());";
            DB::INSERT($sql);

            $message = "Tickets updated successfully";

        } else { // Close ticket
            $desc = cleanString($post['message']);

            $sql = "UPDATE {$this->prefix()}_tickets SET
                    status = '2', updated_at = NOW()
                    WHERE id = '{$id}';";
            DB::UPDATE($sql);

            $sql = "UPDATE {$this->prefix()}_tickets_trans SET
                    description = '{$desc}', finish_date = NOW()
                    WHERE ticket_id = '{$id}'
                    AND is_active = '1';";
            DB::UPDATE($sql);

            $message = "Tickets diverted successfully";
        }

        return $message; 
    }

    public function getAllDataByAssigned($nik)
    {
        $employee = new EmployeesModel;

        $sql = "SELECT a.*, c.assigned_to, c.start_date, c.finish_date FROM {$this->prefix()}_tickets a,
                {$this->prefix()}_tickets_trans c 
                WHERE a.ticket_type = '0'                
                AND a.id = c.ticket_id
                AND c.is_active = '1'
                AND c.assigned_to = '{$nik}'
                ORDER BY a.status ASC, a.level DESC, a.created_at DESC;";

        $data = DB::SELECT($sql);

        // Get Informer
        foreach ($data as $key => $value) {
            $getInformer = $employee->getByNIK($value->created_by);
            if (empty($getInformer)) {
                $data[$key]->namalengkap = NULL;
            } else {
                $data[$key]->namalengkap = $getInformer[0]->fullname;    
            }
        }
        // 

        return $data;
    }

    public function getHistory($ticket_id)
    {
        $employee = new EmployeesModel;

        $sql = "SELECT a.*, c.external_side_name external FROM {$this->prefix()}_tickets_trans a
                LEFT JOIN {$this->prefix()}_tickets_trans_ext c ON a.ticket_trans_id = c.trans_id
                WHERE a.ticket_id = '#{$ticket_id}'
                AND is_active IN ('1', '0')
                AND a.assigned_to IS NOT NULL
                ORDER BY a.start_date;";
        $data['list'] = DB::SELECT($sql);
// print_r($data);die;
        foreach ($data['list'] as $key => $value) {
            if (empty($value->assigned_to)) {
                $data['list'][$key]->namalengkap = "";
            } else {
                $getOfficer = $employee->getByNIK($value->assigned_to);
                $data['list'][$key]->namalengkap = $getOfficer[0]->fullname;
            }
        }
        $data['count'] = count($data['list']);

        return $data;
    }

    function assignedTicket($ticket_id, $post)
    {
        $level = $post['level'];
        $assigned_to = $post['assigned_to'];

        $sql = "UPDATE {$this->prefix()}_tickets_trans SET
                assigned_to = '{$assigned_to}', start_date = NOW()
                WHERE ticket_id = '#{$ticket_id}';";
        DB::UPDATE($sql);

        $sql = "UPDATE {$this->prefix()}_tickets SET
                level = '{$level}', status = 1, updated_at = NOW()
                WHERE id = '#{$ticket_id}';";

        DB::UPDATE($sql);

        $message = "Trouble-Ticket updated successfully";

        return $message;
    }

    function handoverTicket($ticket_id, $post)
    {
        $other_crew = $post['other_crew'];
        $reason = cleanString($post['reason']);

        $sql = "UPDATE {$this->prefix()}_tickets SET
                updated_at = NOW()
                WHERE id = '#{$ticket_id}';";
        DB::UPDATE($sql);

        $sql = "UPDATE {$this->prefix()}_tickets_trans SET
                is_active = '0', description = '{$reason}', finish_date = NOW()
                WHERE ticket_id = '#{$ticket_id}' AND is_active = 1;";
        DB::UPDATE($sql);

        $sql = "INSERT INTO {$this->prefix()}_tickets_trans
                (ticket_id, assigned_to, start_date)
                VALUES ('#{$ticket_id}', '{$other_crew}', NOW());";
        DB::INSERT($sql);

        $message = "Trouble-Ticket updated successfully";

        return $message;
    }

    function closeTicket($ticket_id, $post, $role)
    {
        $desc = cleanString($post['message']);

        $sql = "SELECT is_external FROM {$this->prefix()}_tickets
                WHERE id = '#{$ticket_id}';";
        $ticket = DB::SELECT($sql);

        // START -- IF EXTERNAL AND CLOSE TICKET
        if ($ticket[0]->is_external == 1) {
            $sql = "SELECT ticket_trans_id FROM {$this->prefix()}_tickets_trans
                    WHERE ticket_id = '#{$ticket_id}'
                    AND is_active = '1';";
            $getTransId = DB::SELECT($sql);

            $sql = "UPDATE {$this->prefix()}_tickets_trans_ext SET
                    status = '2', updated_at = NOW()
                    WHERE trans_id = '{$getTransId[0]->ticket_trans_id}';";
            DB::UPDATE($sql);
        }
        // END --

        // Close by Support
        if ($role == "SUPPORT") {
            # code...
            $sql = "UPDATE {$this->prefix()}_tickets_trans SET
                    description = '{$desc}', finish_date = NOW()
                    WHERE ticket_id = '#{$ticket_id}'
                    AND is_active = '1';";
            DB::UPDATE($sql);
        
            $sql = "UPDATE {$this->prefix()}_tickets SET
                    status = '2', updated_at = NOW()
                    WHERE id = '#{$ticket_id}';";
            DB::UPDATE($sql);

         } elseif ($role == "ADMIN") {
            $nik = $post['nik'];
            $level = $post['level'];
            
            $sql = "UPDATE {$this->prefix()}_tickets_trans SET
                    description = '{$desc}', finish_date = NOW(), assigned_to = '{$nik}'
                    WHERE ticket_id = '#{$ticket_id}'
                    AND is_active = '1';";
            DB::UPDATE($sql);

            $sql = "UPDATE {$this->prefix()}_tickets SET
                    status = '2', updated_at = NOW(), level = '{$level}'
                    WHERE id = '#{$ticket_id}';";
            DB::UPDATE($sql);
        }

        $message = "Trouble-Ticket closed successfully";

        return $message;
    }

    function handoverTicketToExternal($ticket_id, $post)
    {
        $estimation = $post['estimation'];
        $estimation_type = $post['estimation_type'];
        $external_side = cleanString($post['external_side']);
        $reason = cleanString($post['reason']);
        $trans_id = $post['trans_id'];

        if ($estimation_type == 0) {
            $finished_at = "DATE_ADD(created_at,INTERVAL {$estimation} HOUR)";
        } else {
            $finished_at = "DATE_ADD(created_at,INTERVAL {$estimation} DAY)";
        }

        $sql = "UPDATE {$this->prefix()}_tickets SET
                is_external = '1', 
                updated_at = NOW()
                WHERE id = '#{$ticket_id}';";
        DB::UPDATE($sql);

        $sql = "INSERT INTO {$this->prefix()}_tickets_trans_ext
                (trans_id, external_side_name, description, estimation_type, estimation_desc, created_at, finished_at)
                VALUES ('{$trans_id}', '{$external_side}', '{$reason}', '{$estimation_type}', '{$estimation}', NOW(), {$finished_at});";
        DB::INSERT($sql);

        $message = "Trouble-Ticket diverted successfully to {$external_side}";

        return $message;
    }

    function getTransExternal($trans_id)
    {
        $sql = "SELECT * FROM {$this->prefix()}_tickets_trans_ext
                WHERE trans_id = '{$trans_id}';";

        return DB::SELECT($sql);


    }

    function getCountByOfficer()
    {
        $sql = "SELECT COUNT(a.id) count, b.assigned_to FROM {$this->prefix()}_tickets a 
                INNER JOIN {$this->prefix()}_tickets_trans b ON a.id = b.ticket_id
                -- INNER JOIN {$this->prefix()}_employee c ON b.assigned_to = c.nik
                WHERE a.status = '1' 
                AND b.is_active = '1' 
                -- AND c.is_admin = '1'
                GROUP BY b.assigned_to";
        $data['progress'] = DB::SELECT($sql);

        $sql = "SELECT COUNT(a.id) count, b.assigned_to FROM {$this->prefix()}_tickets a 
                INNER JOIN {$this->prefix()}_tickets_trans b ON a.id = b.ticket_id
                -- INNER JOIN {$this->prefix()}_employee c ON b.assigned_to = c.nik
                WHERE a.status = '2' 
                AND b.is_active = '1' 
                -- AND c.is_admin = '1'
                GROUP BY b.assigned_to";
        $data['closed'] = DB::SELECT($sql);

        return $data;
    }

    public function getDataByCase($values = [])
    {
        $employee = new EmployeesModel;

        $sql = "SELECT a.*, c.assigned_to, c.start_date, c.finish_date FROM {$this->prefix()}_tickets a,
                -- {$this->prefix()}_employee b,
                {$this->prefix()}_tickets_trans c 
                WHERE a.ticket_type = '0'
                -- AND a.created_by = b.nik
                AND a.id = c.ticket_id
                AND c.is_active = 1
                AND a.case_type IN ({$values})
                ORDER BY a.status ASC, a.created_at DESC;";

        $data = DB::SELECT($sql);

        foreach ($data as $key => $value) {
            $getEmployee = $employee->getByNIK($value->created_by);
            if (empty($getEmployee)) {
                $data[$key]->namalengkap = NULL;
            } else {
                $data[$key]->namalengkap = $getEmployee[0]->fullname;    
            }
        }

        return $data;
    }
}
