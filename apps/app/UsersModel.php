<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class UsersModel extends Model
{
    //
	// validate NIK for Register User
    public function validateNIK($nik)
    {
    	$sql = "SELECT * FROM itss_employee WHERE nik = '{$nik}';";

    	$getUser = DB::SELECT($sql);

    	if (!empty($getUser)) {
    		$data = $getUser;
    	} else {
    		$data = [];
    	}

    	return $data;
    }

    // after validate NIK, we create a new user
    public function createUser($dataRegist, $dataEmployee)
    {
    	$username = $dataRegist['username'];
    	$password = $dataRegist['password'];
    	$nik = $dataRegist['nik'];
    	$crypt_password = bcrypt($password);

        $sql = "SELECT * FROM itss_user_level WHERE nik = '{$nik}';";
        $isAdmin = DB::SELECT($sql);

        if (empty($isAdmin)) {
            $user_role = "USER";
        } else {
            if ($isAdmin[0]->level == 1) {
                $user_role = "SUPPORT";
            } else {
                $user_role = "ADMIN";
            }
        }

    	// if ($dataEmployee[0]->is_admin == 0) {
    	// 	$user_role = "USER";
    	// } else if ($dataEmployee[0]->is_admin == 1) {
    	// 	$user_role = "SUPPORT";
    	// } else {
     //        $user_role = "ADMIN";
     //    }

    	// validate user existing
    	$sql = "SELECT * FROM users WHERE nik = '{$nik}';";
    	$getUser = DB::SELECT($sql);

    	if (empty($getUser)) {
    		// $sql = "UPDATE itss_employee SET is_regist = '1' WHERE nik = '{$nik}';";

    		// DB::UPDATE($sql);

    		// INSERT USER
    		$sql = "INSERT INTO users (username, password, user_role, nik, plain, created_at)
    				VALUES ('{$username}', '{$crypt_password}', '{$user_role}', '{$nik}', md5('{$password}'), NOW());";

    		DB::INSERT($sql);

    		$status = true;
    	} else {
    		$status = false;
    	}
    	
    	return $status;
    }


    public function validateLogin($data)
    {
    	$username = $data['username'];
    	$password = $data['password'];

    	$sql = "SELECT
    			a.*
                -- , b.namalengkap, b.email, c.id id_position, c.nama position, d.id id_dept, d.nama department
    			FROM users a
                -- , itss_employee b, itss_position c, itss_department d
    			WHERE 
    				-- a.nik = b.nik 
    				-- AND b.idposition = c.id 
    				-- AND c.iddept = d.id 
    				-- AND
                    a.username = '{$username}' 
    				AND a.plain = md5('{$password}');";

    	$credentials = DB::SELECT($sql);

    	return $credentials;
    }

    function resetPassword($post)
    {
        $nik = $post['nik'];
        $password = $post['password'];
        $password_confirmation = $post['password_confirmation'];

        $sql = "UPDATE users SET password = '".bcrypt($password)."',
                plain = '".md5($password)."'
                WHERE nik = '{$nik}';";

        DB::UPDATE($sql);

        return true;
    }

    function getByNIK($nik)
    {
        $sql = "SELECT * FROM users WHERE nik = '{$nik}';";
        $getUser = DB::SELECT($sql);

        return $getUser;
    }
}