@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		<span style="float: right;">
			<!-- LINK ADD Brand -->
			<a href="{{ url('/') }}/asset/condition/createCondition" class="btn btn-primary"><i class="fa fa-plus"></i> Add Condition</a>
		</span>
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Conditon</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive table-striped">
						<thead>
							<th>#</th>
							<th>Condition ID</th>
							<th>Condition's Names</th>
							<th>Status</th>
							<th>Description</th>
							<th>Tools</th>
						</thead>
						@foreach($data['assetCondition'] as $data)
						<tr>
							<td></td>
							<td>{{$data->id}}</td>
							<td>{{$data->conditionName}}</td>
							<td>
								@if($data->conditionStatus == 0)
									GOOD
								@else
									NOT GOOD
								@endif
							</td>
							<td>{{$data->conditionDescription}}</td>
							<td>
								<a href="{{ url('/asset/condition/editCondition/'.$data->id) }}">EDIT</a>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
		</div>
	</div>
</div>	
@endsection
