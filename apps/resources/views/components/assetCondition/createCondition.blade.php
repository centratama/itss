@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Create a New Condition</center></div>
				<div class="panel-body">
					<legend>Open a New Condition</legend>
					<div>
					<!-- INSERT DATA -->
					<div style="" id="form-problem">
						<p>Please fill in the form below to create New Condition</p>

						<!-- TABLE INPUT DATA ASSET -->
						<table class="table table-bordered">
							<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

							<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

							{{ csrf_field()}}
									
							<!-- BRAND'S NAME -->
							<tr>
								<th>Condition's Name :</th>
								<td><input class="form-control" type="text" name="condition[conditionName]" id="conditionName" value="">
								</td>
							</tr>

							<tr>
								<th>Status :</th>
								<td>
									<select class="form-control" name="status" id="status">
										<option value="">--Choose--</option>
										<option value="0" selected>GOOD</option>
										<option value="1">NOT GOOD</option>
									</select>
								</td>
							</tr>

							<!-- DESCRIPTION -->
							<tr>
								<th>Description :</th>
								<td>
									<textarea class="form-control input-lg" name="condition[conditionDescription]" id="conditionDescription" rows="10"></textarea>
								</td>
							</tr>

							<!-- SUBMIT BUTTON -->
							<tr>
								<td colspan="2" align="right">
									<input type="reset" name="reset" value="Reset" class="btn btn-danger">
									<button name="submit" class="btn btn-primary" id="btn-submit-problem">Add</button>
								</td>
							</tr>
						</table>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<script type="text/javascript">
</script>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#btn-submit-problem').click(function(e){

			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();
			
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('conditionName', ($("#conditionName").val()));
			formData.append('status', ($("#status").val()));
			formData.append('conditionDescription', ($("#conditionDescription").val()));

			//SEND DATA TO ROUTES
			var url = "{{url('/')}}/asset/condition/saveCondition";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/asset/condition";
				}
			});
		});
	});
</script>
<!-- end -->
@endsection
