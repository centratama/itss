@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Edit Asset's Condition</center></div>
				<div class="panel-body">
					<legend>Edit Asset's Condition</legend>
					<div>
					<!-- EDIT DATA -->
					<div style="" id="form-problem">
						<p>Please fill in the form below to edit asset's Condition</p>

						<!-- TABLE INPUT DATA ASSET -->
						<table class="table table-bordered">
							<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

							<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

							{{ csrf_field()}}

							<input class="form-control" type="text" name="condition[idCondition" id="idCondition" value="{{ $data['assetCondition'][0]->id }}">
									
							<!-- BRAND'S NAME -->
							<tr>
								<th>Condition's Name :</th>
								<td>
									<input class="form-control" type="text" name="condition[conditionName]" id="conditionName" value="{{ $data['assetCondition'][0]->conditionName }}">
								</td>
							</tr>

							<tr>
								<th>Status :</th>
								<td>
									<select class="form-control" name="status" id="status">
										@if($data['assetCondition'][0]->conditionStatus == 0)
											<option value="0" selected>GOOD</option>
											<option value="1">NOT GOOD</option>
										@else
											<option value="0">GOOD</option>
											<option value="1" selected>NOT GOOD</option>
										@endif
									</select>
								</td>
							</tr>

							<!-- BRAND'S NAME -->
							<tr>
								<th>Condition's Description :</th>
								<td>
									<input class="form-control" type="text" name="condition[conditionDescription]" id="conditionDescription" value="{{ $data['assetCondition'][0]->conditionDescription }}">
								</td>
							</tr>

							<!-- SUBMIT BUTTON -->
							<tr>
								<td colspan="2" align="right">
									<input type="reset" name="reset" value="Reset" class="btn btn-danger">
									<button name="submit" class="btn btn-primary" id="btn-submit-problem">Save</button>
								</td>
							</tr>
						</table>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<script type="text/javascript">
</script>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#btn-submit-problem').click(function(e){

			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('conditionName', ($("#conditionName").val()));
			formData.append('status', ($("#status").val()));
			formData.append('conditionDescription', ($("#conditionDescription").val()));
			
			//SEND DATA TO ROUTES
			var url = "{{url('/asset/condition/saveEditCondition/'. $idCondition) }}";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/asset/condition";
				}
			});
		});
	});
</script>
<!-- end -->
@endsection
