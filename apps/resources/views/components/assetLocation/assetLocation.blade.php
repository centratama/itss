@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		<span style="float: right;">
			<!-- LINK ADD Brand -->
			<a href="{{ url('/') }}/asset/location/createLocation" class="btn btn-primary"><i class="fa fa-plus"></i> Add Location</a>
		</span>
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Location</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive table-striped">
						<thead>
							<th>#</th>
							<th>Location ID</th>
							<th>Location's Names</th>
							<th>Description</th>
							<th>Tools</th>
						</thead>
						@foreach($data['assetLocation'] as $data)
						<tr>
							<td></td>
							<td>{{$data->id}}</td>
							<td>{{$data->locationName}}</td>
							<td>{{$data->locationDescription}}</td>
							<td>
								<a href="{{ url('/asset/location/editLocation/'.$data->id) }}">EDIT</a>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
		</div>
	</div>
</div>	
@endsection
