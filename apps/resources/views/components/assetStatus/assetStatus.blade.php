@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		<span style="float: right;">
			<!-- LINK ADD Brand -->
			<a href="{{ url('/') }}/asset/status/createStatus" class="btn btn-primary"><i class="fa fa-plus"></i> Add Status</a>
		</span>
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List of Transaction Status</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive table-striped" id="table-tickets">
						<thead>
							<th>#</th>
							<th>Status ID</th>
							<th>Status Names</th>
							<th>Description</th>
							<th>Status</th>
							<th>Tools</th>
						</thead>
						@foreach($data['assetStatus'] as $data)
						<tr>
							<td></td>
							<td>{{$data->id}}</td>
							<td>{{$data->statusName}}</td>
							<td>{{$data->statusDescription}}</td>
							<td>
								@if( $data->status == 1 )
									OUT
								@elseif( $data->status == 0 )
									IN
								@else
									DISPOSAL
								@endif
							</td>
							<td>
								<a href="{{ url('/asset/status/editStatus/'.$data->id) }}">EDIT</a>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
		</div>
	</div>
</div>	
@endsection
