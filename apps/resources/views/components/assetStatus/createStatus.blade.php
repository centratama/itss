@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Create a New Status</center></div>
				<div class="panel-body">
					<legend>Open a New Status</legend>
					<div>
					<!-- INSERT DATA -->
					<div style="" id="form-problem">
						<p>Please fill in the form below to create New Status</p>

						<!-- TABLE INPUT DATA ASSET -->
						<table class="table table-bordered">
							<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

							<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

							{{ csrf_field()}}
									
							<!-- BRAND'S NAME -->
							<tr>
								<th>Status's Name :</th>
								<td><input class="form-control" type="text" name="status[statusName]" id="statusName" value="">
								</td>
							</tr>

							<tr>
								<th>Status :</th>
								<td>
									<select class="form-control" name="status" id="status">
										<option value="">--Choose--</option>
										<option value="0" selected>IN</option>
										<option value="1">OUT</option>
										<option value="2">DISPOSAL</option>
									</select>
								</td>
							</tr>

							<!-- DESCRIPTION -->
							<tr>
								<th>Description :</th>
								<td>
									<textarea class="form-control input-lg" name="status[statusDescription]" id="statusDescription" rows="10"></textarea>
								</td>
							</tr>

							<!-- SUBMIT BUTTON -->
							<tr>
								<td colspan="2" align="right">
									<input type="reset" name="reset" value="Reset" class="btn btn-danger">
									<button name="submit" class="btn btn-primary" id="btn-submit-problem">Add Status</button>
								</td>
							</tr>
						</table>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<script type="text/javascript">
</script>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#btn-submit-problem').click(function(e){

			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();
			
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('statusName', ($("#statusName").val()));
			formData.append('status', ($("#status").val()));
			formData.append('statusDescription', ($("#statusDescription").val()));

			//SEND DATA TO ROUTES
			var url = "{{url('/')}}/asset/status/saveStatus";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/asset/status";
				}
			});
		});
	});
</script>
<!-- end -->
@endsection
