@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		<span style="float: right;">
			<!-- LINK ADD Brand -->
			<a href="{{ url('/') }}/asset/types/createType" class="btn btn-primary"><i class="fa fa-plus"></i> Add Types</a>
		</span>
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Types</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive table-striped">
						<thead>
							<th>#</th>
							<th>Type ID</th>
							<th>Type's Names</th>
							<th>Description</th>
							<th>Tools</th>
						</thead>
						@foreach($data['assetType'] as $data)
						<tr>
							<td></td>
							<td>{{$data->id}}</td>
							<td>{{$data->typeName}}</td>
							<td>{{$data->typeDescription}}</td>
							<td>
								<a href="{{ url('/asset/types/editType/'.$data->id) }}">EDIT</a>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
		</div>
	</div>
</div>	
@endsection
