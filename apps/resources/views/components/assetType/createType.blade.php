@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Create a New Types</center></div>
				<div class="panel-body">
					<legend>Open a New Types</legend>
					<div>
					<!-- INSERT DATA -->
					<div style="" id="form-problem">
						<p>Please fill in the form below to create New Types</p>

						<!-- TABLE INPUT DATA ASSET -->
						<table class="table table-bordered">
							<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

							<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

							{{ csrf_field()}}
									
							<!-- BRAND'S NAME -->
							<tr>
								<th>Type's Name :</th>
								<td><input class="form-control" type="text" name="type[typeName]" id="typeName" value="">
								</td>
							</tr>

							<!-- DESCRIPTION -->
							<tr>
								<th>Description :</th>
								<td>
									<textarea class="form-control input-lg" name="type[typeDescription]" id="typeDescription" rows="10"></textarea>
								</td>
							</tr>

							<!-- SUBMIT BUTTON -->
							<tr>
								<td colspan="2" align="right">
									<input type="reset" name="reset" value="Reset" class="btn btn-danger">
									<button name="submit" class="btn btn-primary" id="btn-submit-problem">Add Type</button>
								</td>
							</tr>
						</table>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<script type="text/javascript">
</script>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#btn-submit-problem').click(function(e){

			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();
			
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('typeName', ($("#typeName").val()));
			formData.append('typeDescription', ($("#typeDescription").val()));

			//SEND DATA TO ROUTES
			var url = "{{url('/')}}/asset/types/saveType";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/asset/types";
				}
			});
		});
	});
</script>
<!-- end -->
@endsection
