@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Add More Item For {{ $idAsset }}</center></div>
				<div class="panel-body">
					<legend></legend>
					<div>
					<!-- INSERT DATA -->
					<div style="" id="form-problem">
						<p>Please fill in the form below to add more item for {{ $idAsset }}</p>
							<!-- TABLE INPUT DATA ASSET -->
							<table class="table table-bordered">
								<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

								<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

								{{ csrf_field()}}

								<!-- ID ASSETS -->
								<input type="hidden" name="asset[idParent]" value="{{ $idAsset }}" id="idParent">
								<input id="idList" value="{{ $i=1 }}" type="hidden" />

								<!-- ASSET -->
								<tr>
									<th>ASSET:</th>
									<td>
										<select class="form-control" name="asset[idAsset]" id="idAsset">
											<option value="">--Choose--</option>
											@foreach ($data['assetData'] as $assetData)
											<option value="{{$assetData->idAsset}}">{{$assetData->idAsset}}</option>
											@endforeach
										</select>
									</td>
								</tr>

								<!-- STATUS ASSET -->
								<tr>
									<th>STATUS :</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="asset[assetStatus]" id="assetStatus">
											<option value="">--Choose--</option>
											@foreach ($data['assetStatus'] as $assetStatus)
											<option value="{{$assetStatus->id}}">
												{{$assetStatus->statusName}}
											</option>
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- RECEIVING CONDITIONS -->
								<tr>
									<th>Reciving Condition : </th>
									<td>
										<!-- Getting the data from model AssetCondition -->
										<select class="form-control" name="asset[receivingCondition]" id="receivingCondition">
											<option value="">--Choose--</option>
											@foreach ($data['assetCondition'] as $assetCondition)
											<option value="{{$assetCondition->id}}">{{$assetCondition->conditionDescription}}</option>
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>	
								<!-- SUBMIT BUTTON -->
								<tr>
									<td colspan="2" align="right">
										<input type="reset" name="reset" value="Reset" class="btn btn-danger">
										<button name="submit" class="btn btn-primary" id="btn-submit-problem">Add Asset</button>
									</td>
								</tr>
							</table>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<script type="text/javascript">
</script>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#btn-submit-problem').click(function(e){

			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();


			
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('idParent', ($("#idParent").val()));
			formData.append('idAsset', ($("#idAsset").val()));
			formData.append('receivingCondition', ($("#receivingCondition").val()));
			formData.append('assetStatus', ($("#assetStatus").val()));
			//SEND DATA TO ROUTES
			var url = "{{url('/')}}/asset/saveAddTransaction";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/asset";
				}
			});
		});
	});

	function tambahListChild() {
	   var idList = document.getElementById("idList").value;
	   var duplicate;
	   var dataList = idList - 1 + 2;

	   duplicate = 
	   "<div id='srow" + idList + "'>" +
	   "<a href='#' style=\"color:#3399FD;\" onclick='hapusElemen(\"#srow" + idList + "\"); return false;'>Hapus</a>" +
	   "<label class='sr-only'>Daftar Child</label>" +
	   "<select name='daftarChild[]' class='form-control'>" +
	   @foreach ( $data['assetData'] as $assetData )
	   "<option value=''>--Choose--</option>" +
	   "<option value='{{ $assetData->idAsset }}'> {{ $assetData->name }}</option>" +
	   @endforeach
	   "</select>" +
	   "</div>";

	   $("#divListChild").append(duplicate);
	   idList = (idList-1) + 2;
	   document.getElementById("idList").value = idList;
	}

	function hapusElemen(idPaket) {
		$(idPaket).remove();
	}

</script>
<!-- end -->
@endsection
