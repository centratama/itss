@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>FILE BERLAC FOR ID ASSET {{ $fileName }}</center></div>
			<div class="panel-body" >
				<legend>File Berlac {{ $fileName }}</legend>
				<div>
					<object data="http://assets.centratamagroupdev.com/html/{!! $fileName !!}" type="application/pdf" width="100%" style="height: 500px">
					</object>
				</div>
			</div>
		</div>
	</div>
	<!-- end -->
	@endsection
