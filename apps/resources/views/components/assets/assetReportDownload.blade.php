@extends('layouts.apps1')
@section('content')

<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 14 (filtered)">
<style>
<!--
/* Font Definitions */
@page {
	margin : 7px;
}
body{
	margin :7px;
}
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:Vijaya;
	panose-1:2 11 6 4 2 2 2 2 2 4;}
@font-face
	{font-family:"Iskoola Pota";
	panose-1:2 11 5 2 4 2 4 2 2 3;}
@font-face
	{font-family:"Trebuchet MS";
	panose-1:2 11 6 3 2 2 2 2 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
h1
	{mso-style-link:"Heading 1 Char";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	page-break-after:avoid;
	font-size:9.0pt;
	font-family:"Arial","sans-serif";
	font-weight:normal;
	text-decoration:underline;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-link:"Header Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Footer Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoSubtitle, li.MsoSubtitle, div.MsoSubtitle
	{mso-style-link:"Subtitle Char";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	text-justify:inter-ideograph;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Arial","sans-serif";
	color:black;
	font-weight:bold;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Balloon Text Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:36.0pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:36.0pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoIntenseQuote, li.MsoIntenseQuote, div.MsoIntenseQuote
	{mso-style-link:"Intense Quote Char";
	margin-top:10.0pt;
	margin-right:46.8pt;
	margin-bottom:14.0pt;
	margin-left:46.8pt;
	line-height:115%;
	border:none;
	padding:0cm;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	color:#4F81BD;
	font-weight:bold;
	font-style:italic;}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-link:"Balloon Text";
	font-family:"Tahoma","sans-serif";}
span.HeaderChar
	{mso-style-name:"Header Char";
	mso-style-link:Header;}
span.FooterChar
	{mso-style-name:"Footer Char";
	mso-style-link:Footer;}
span.IntenseQuoteChar
	{mso-style-name:"Intense Quote Char";
	mso-style-link:"Intense Quote";
	color:#4F81BD;
	font-weight:bold;
	font-style:italic;}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-link:"Heading 1";
	font-family:"Arial","sans-serif";
	text-decoration:underline;}
span.SubtitleChar
	{mso-style-name:"Subtitle Char";
	mso-style-link:Subtitle;
	font-family:"Arial","sans-serif";
	color:black;
	font-weight:bold;}
.MsoChpDefault
	{font-family:"Calibri","sans-serif";}
.MsoPapDefault
	{margin-bottom:10.0pt;
	line-height:115%;}
 /* Page Definitions */
 @page WordSection1
	{size:21.0cm 841.95pt;
	margin:42.55pt 42.45pt 70.9pt 42.55pt;}
div.WordSection1
	{page:WordSection1;}
@page WordSection2
	{size:21.0cm 841.95pt;
	margin:42.55pt 42.45pt 70.9pt 42.55pt;}
div.WordSection2
	{page:WordSection2;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>
<div class="row">
	@if(isset($idTransaction))
	<a href="{{ url('/transaction/downloadTransactionPDF/'.$idTransaction) }}"><button>DOWNLOAD</button></a>
	@endif
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class=WordSection1> 
			<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width=690 style='width:517.4pt;margin-left:5.4pt;border-collapse:collapse;border:none'>
				<tr style='height:18.4pt'>
				<td width=265 rowspan=2 valign=top style='width:4.0cm;padding:0cm 5.4pt 0cm 5.4pt;
				height:18.4pt'>
				<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><span style='color:#1F497D'><img width=241 height=30 src="{{ url('/assets/logo/logo.png') }}"></span></p>

				</td>
				<td width=425 colspan=2 valign=top style='width:315.95pt;padding:0cm 5.4pt 0cm 5.4pt;
				height:18.4pt'>
				<p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
				text-align:right;line-height:normal'><b><span style='font-size:16.0pt'>IFU</span></b></p>
				<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
				normal'><b><span style='font-size:14.0pt;font-family:"Vijaya","sans-serif"'>INFRASTRUCTURE
				UNDERTAKING</span></b></p>
				</td>
				</tr>

				<tr style='height:15.05pt'>
				<td width=145 valign=top style='width:108.8pt;padding:0cm 5.4pt 0cm 5.4pt;
				height:15.05pt'>
				<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
				normal'><span style='font-size:9.0pt'>IFU No. (</span><span lang=IN
				style='font-size:9.0pt'>Date</span><span style='font-size:9.0pt'>)</span></p>
				</td>
				<td width=249 valign=top style='width:186.4pt;padding:0cm 5.4pt 0cm 5.4pt;
				height:15.05pt'>
				<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
				normal'><b><span lang=IN style='font-size:9.0pt'>
					: {{ $data['transactionHeader'][0]->id }}
				

				</span></b></p>
				</td>
				<td style='border:none;padding:0cm 0cm 0cm 0cm' width=9><p class='MsoNormal'>&nbsp;</td>
				</tr>
<tr style='height:12.95pt'>
	<td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt;
	height:12.95pt'>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	normal'>
	@foreach($data['assetStatus'] as $assetStatus)
	@if($data['transactionHeader'][0]->id_status == $assetStatus->id)
		<span lang=IN style='font-size:5.0pt'>
			<input type="checkbox" checked>{{ $assetStatus->statusName }}
		</span>
	@else
		<span lang=IN style='font-size:5.0pt'>
			<input type="checkbox">{{ $assetStatus->statusName }}
		</span>
	@endif
	@endforeach
	</p>
	</td>
	<td width=145 valign=top style='width:108.8pt;padding:0cm 5.4pt 0cm 5.4pt;
	height:12.95pt'>
	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	normal'><span style='font-size:9.0pt'>Request no (Score)</span></p>
</td>
<td width=249 valign=top style='width:186.4pt;padding:0cm 5.4pt 0cm 5.4pt;
height:12.95pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:9.0pt'>: pyy-003 (<b>#.#</b>)</span></p>
</td>
<td style='border:none;padding:0cm 0cm 0cm 0cm' width=9><p class='MsoNormal'>&nbsp;</td>
</tr>
</table>

<div style='border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 1.0pt 0cm'>

	<p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:6.0pt;
	margin-left:0cm;border:none;padding:0cm'><b><span style='font-size:9.0pt;
	line-height:115%'>USER</span></b><span style='font-size:9.0pt;line-height:115%'>,
		the term User within this document refer to the employee of PT. Centratama Telekomunikasi
	Indonesia and subsidiaries with following profiles</span></p>

</div>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=680
style='width:18.0cm;margin-left:5.4pt;border-collapse:collapse;border:none'>
<tr>
	<td width=104 style='width:77.9pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:8.75pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span style='font-size:8.0pt'> 
Corporate Profile</span></p>
</td>
<td width=225 valign=top style='width:168.85pt;border:none;border-bottom:
solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:8.0pt'></span>
</p>
</td>
<td width=104 style='width:77.9pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:8.75pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span style='font-size:8.0pt'> 
Network Profile</span></p>
</td>
<td width=202 colspan=2 style='width:151.8pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'></p>
</td>
</tr>

<tr>
	<td width=104 style='width:77.9pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:8.75pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span style='font-size:8.0pt'> 
Name</span></p>
</td>
<td width=225 valign=top style='width:168.85pt;border:none;border-bottom:
solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:8.0pt'>: 
@foreach($data['employee'] as $employee)
@if($data['transactionHeader'][0]->nik_undertakenby == $employee->number)
{{ $employee->fullname }}
@endif
@endforeach</span>
</p>
</td>
<td width=104 style='width:77.9pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:8.75pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span style='font-size:8.0pt'>PAR</span></p>
</td>
<td width=202 colspan=2 style='width:151.8pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'>: - </p>
</td>
</tr>
<tr>
	<td width=104 style='width:77.9pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:8.75pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span style='font-size:8.0pt'> 
EID</span></p>
</td>
<td width=225 valign=top style='width:168.85pt;border:none;border-bottom:
solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:8.0pt'>: {{$data['transactionHeader'][0]->nik_undertakenby}}</span></p>
</td>
<td width=104 style='width:77.9pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:8.75pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span lang=IN style='font-size:8.0pt'>Username</span></p>
</td>
<td width=202 colspan=2 style='width:151.8pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span lang=IN style='font-size:8.0pt'>
							@foreach($data['employee'] as $employee)
								@if($data['transactionHeader'][0]->nik_undertakenby == $employee->number)
									<?php $username = $employee->email;
										  $result = substr($username, 0, strpos($username, '@'));?>
									: {{ $result }}
								@endif
							@endforeach 
</span></p>
</td>
</tr>
<tr>
	<td width=104 style='width:77.9pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:8.75pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span style='font-size:8.0pt'> 
Department</span></p>
</td>
<td width=225 valign=top style='width:168.85pt;border:none;border-bottom:
solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:8.0pt'>@foreach($data['employee'] as $employee)
								@if($data['transactionHeader'][0]->nik_undertakenby == $employee->number)
									@foreach($data['department'] as $department)
										@if($employee->id_organization == $department->id)
										: {{ $department->name }}
										@endif
									@endforeach
								@endif
							@endforeach</span></p>
</td>
<td width=104 style='width:77.9pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:8.75pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span style='font-size:8.0pt'>Email</span></p>
</td>
<td width=202 colspan=2 style='width:151.8pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span lang=IN style='font-size:8.0pt'>
						@foreach($data['employee'] as $employee)
								@if($data['transactionHeader'][0]->nik_undertakenby == $employee->number)
									: {{ $employee->email }}
								@endif
						@endforeach</span>
</td>
</tr>
<tr style='height:13.4pt'>
	<td width=104 style='width:77.9pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:8.75pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span style='font-size:8.0pt'> 
Function</span></p>
</td>
<td width=225 valign=top style='width:168.85pt;border:none;border-bottom:
solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:13.4pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:8.0pt'>: -</span></p>
</td>
<td width=104 style='width:77.9pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:13.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:8.75pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span style='font-size:8.0pt'>Domain</span></p>
</td>
<td width=202 colspan=2 style='width:151.8pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:13.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span style='font-size:8.0pt'>: centratamagroup.com</span></p>
</td>
</tr>
<tr style='height:16.75pt'>
	<td width=104 style='width:77.9pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:8.75pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span style='font-size:8.0pt'> 
Position</span></p>
</td>
<td width=225 valign=top style='width:168.85pt;border:none;border-bottom:
solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:8.0pt'>
							@foreach($data['employee'] as $employee)
								@if($data['transactionHeader'][0]->nik_undertakenby == $employee->number)
									@foreach($data['division'] as $division)
										@if($employee->id_job == $division->id)
										: {{ $division->description }}
										@endif
									@endforeach
								@endif
							@endforeach
</span></p>
</td>
<td width=104 style='width:77.9pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:8.75pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span style='font-size:8.0pt'>LAN
MAC/IPv4</span></p>
</td>
<td width=202 colspan=2 style='width:151.8pt;border:none;border-bottom:solid windowtext 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
<p class=MsoNormal style='margin-top:0cm;margin-right:-5.4pt;margin-bottom:
0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:normal'><span style='font-size:8.0pt'>: MAC/IP
Address</span></p>
</td>
</tr>
<tr height=0>
	<td width=83 style='border:none'></td>
	<td width=23 style='border:none'></td>
	<td width=225 style='border:none'></td>
	<td width=19 style='border:none'></td>
	<td width=104 style='border:none'></td>
	<td width=24 style='border:none'></td>
	<td width=184 style='border:none'></td>
	<td width=19 style='border:none'></td>
</tr>
</table>

<div style='border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 1.0pt 0cm'>

	<p class=MsoNormal style='margin-top:12.0pt;margin-right:0cm;margin-bottom:
	6.0pt;margin-left:0cm;line-height:normal;border:none;padding:0cm'><b><span
		style='font-size:9.0pt'>INFRASTRUCTURE</span></b><span style='font-size:9.0pt'>,
			the term Infrastructure within this document refer to the infrastructure owned
			by PT. Centratama Telekomunikasi Indonesia and subsidiaries which is/are to be
		Undertaken by User with following details*</span></p>

	</div>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><span
		lang=IN style='font-size:2.0pt;line-height:115%'>&nbsp;</span></p>

		<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
		style='border-collapse:collapse;border:none'>
		<tr>
			<td width=140 valign=top style='width:104.65pt;border:solid black 1.0pt;
			background:#00B0F0;padding:0cm 5.4pt 0cm 5.4pt'>
			<p class=MsoNormal style='margin-top:4.0pt;margin-right:0cm;margin-bottom:
			4.0pt;margin-left:0cm;line-height:normal'><b><span style='font-size:10.0pt'>Category</span></b></p>
		</td>
		<td width=548 valign=top style='width:411.05pt;border:solid black 1.0pt;
		border-left:none;background:#00B0F0;padding:0cm 5.4pt 0cm 5.4pt'>
		<p class=MsoNormal style='margin-top:4.0pt;margin-right:0cm;margin-bottom:
		4.0pt;margin-left:0cm;line-height:normal'><b><span style='font-size:10.0pt'>Model
		Description </span></b><span style='font-size:9.0pt'>(Centratama Group label
		must be attached but might be inherited to the infrastructure undertaken)</span></p>
	</td>
</tr>
@foreach($data['transactionDetailParent'] as $detailParent)
<tr>
	<td width=140 valign=top style='width:124.65pt;border:solid black 1.0pt;
	border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
	<p class=MsoNormal style='margin-top:3.0pt;margin-right:0cm;margin-bottom:
	3.0pt;margin-left:0cm;line-height:normal'><span style='font-size:9.0pt;
	font-family:"Iskoola Pota","sans-serif"'>{{ $detailParent->id_asset}}</span></p>
</td>
<td width=548 valign=top style='width:391.05pt;border-top:none;border-left:
none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt'>
<p class=MsoNormal style='margin-top:3.0pt;margin-right:0cm;margin-bottom:
3.0pt;margin-left:0cm;line-height:normal'><span style='font-size:9.0pt;
font-family:"Iskoola Pota","sans-serif"'>
	@foreach($data['assetData'] as $asset)
		@if($asset->idAsset == $detailParent->id_asset)
			{{ $asset->name }} | <b>SN :{{$asset->serial_number}}</b>
		@endif
	@endforeach
</span></p>

<p class=MsoNormal style='margin-top:3.0pt;margin-right:0cm;margin-bottom:
3.0pt;margin-left:0cm;line-height:normal'><span style='font-size:9.0pt;
font-family:"Iskoola Pota","sans-serif"'>
	@foreach($data['assetData'] as $asset)
							@foreach($data['transactionDetailChild'] as $detailChild)
								@if($asset->idAsset == $detailChild->id_asset AND $detailParent->id_asset == $detailChild->id_parent)

								<table border="0" width="100%">
									<tr width="100%">
										<td>
											{{ $asset->idAsset }}
										</td>
										<td>
											{{ $asset->name }} <br> <b>SN :{{$asset->serial_number}}</b>
										</td>
									</tr>
								</table>					
								@endif
							@endforeach
						@endforeach
</span></p>
</td>

</tr>
@endforeach
</table>

<div style='border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 1.0pt 0cm'>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	normal;border:none;padding:0cm'><span style='font-size:8.0pt'>*) print-out of
		workstation profile for related Infrastructure, e.g: Desktop
	PC/Notebook/All-in-one PC is annexed hereunder this document and User </span><i><span
		lang=IN style='font-size:8.0pt;font-family:"Trebuchet MS","sans-serif"'>have
	read, </span></i><span style='font-size:8.0pt'>understood and acknowledged
	receipt of all described specifications.</span></p>

	<p class=MsoNormal style='margin-top:12.0pt;margin-right:0cm;margin-bottom:
	6.0pt;margin-left:0cm;line-height:normal;border:none;padding:0cm'><b><span
		style='font-size:9.0pt'>UNDERTAKING</span></b><span style='font-size:9.0pt'>,
			The term Undertaking is use to define that Infrastructure is to be made in-use
			by User to support operational cycle for the advantages of PT. Centratama Telekomunikasi
		Indonesia and subsidiaries  (or Centratama Group, in short).</span></p>

	</div>

	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><span
		lang=IN style='font-size:2.0pt;line-height:115%'>&nbsp;</span></p>

		<div style='border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 1.0pt 0cm'>

			<p class=MsoNormalCxSpMiddle style='margin-top:4.0pt;margin-right:0cm;
			margin-bottom:2.0pt;margin-left:0cm;text-align:justify;text-justify:inter-ideograph;
			border:none;padding:0cm'><i><span lang=IN style='font-size:9.0pt;line-height:
			115%'>Th</span></i><i><span style='font-size:9.0pt;line-height:115%'>e User </span></i><i><span
				lang=IN style='font-size:9.0pt;line-height:115%'>ensures that </span></i><i><span
					style='font-size:9.0pt;line-height:115%'>the Infrastructure will be used on
					behalf Centratama Group for Centratama Group’s interests. By signing this form
					the User accepts and will comply to all responsibilities contained within the
					User’s PAR (Privilege for Access Rights) which are defined under Centratama
					Group policy regarding ICT Network &amp; Data Access policy, and the User
					confirm that the Infrastructure undertaken are in good condition to support the
				User’s operational functions as contained within the User’s PAR.</span></i></p>

				<p class=MsoNormalCxSpMiddle style='margin-top:4.0pt;margin-right:0cm;
				margin-bottom:2.0pt;margin-left:0cm;text-align:justify;text-justify:inter-ideograph;
				border:none;padding:0cm'><i><span lang=IN style='font-size:9.0pt;line-height:
				115%'>The User hereby <u>agree to comply</u> with the policies as stated hereunder
				in ANNEX-1 and understand that failure to comply will result in certain level
				of <u>disciplinary action</u>.</span></i></p>

				<p class=MsoNormal style='margin-top:12.0pt;margin-right:0cm;margin-bottom:
				6.0pt;margin-left:0cm;border:none;padding:0cm'><b><span style='font-size:9.0pt;
				line-height:115%'>ADDITIONAL NOTE</span></b></p>

			</div>

			<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><span
				lang=IN style='font-size:2.0pt;line-height:115%'>&nbsp;</span></p>

				<div style='border:none;border-bottom:solid windowtext 1.0pt;padding:0cm 0cm 1.0pt 0cm'>

					<p class=MsoNormalCxSpMiddle style='margin-top:4.0pt;margin-right:0cm;
					margin-bottom:2.0pt;margin-left:0cm;text-align:justify;text-justify:inter-ideograph;
					line-height:normal;border:none;padding:0cm'><span style='font-size:8.0pt;
					font-family:"Iskoola Pota","sans-serif";color:#7F7F7F'>{{ $data['transactionHeader'][0]->description }}</span></p>

					<p class=MsoNormal style='margin-top:12.0pt;margin-right:0cm;margin-bottom:
					6.0pt;margin-left:0cm;border:none;padding:0cm'><b><span style='font-size:9.0pt;
					line-height:115%'>VALIDATION</span></b></p>

				</div>

				<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=500
				style='width:400pt;border-collapse:collapse;border:none'>
				<tr>
					<td width=102 valign=top style='width:67.75pt;border:none;border-bottom:solid black 1.0pt;
					padding:0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
					text-align:center;line-height:normal'><span style='font-size:9.0pt'>Prepared
					By</span></p>
					</td>

					<td width=114 valign=top style='width:67.75pt;border:none;border-bottom:solid black 1.0pt;
					padding:0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
					text-align:center;line-height:normal'><span style='font-size:9.0pt'>Verified By</span></p>
					</td>

					<td width=19 valign=top style='width:15.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
					text-align:center;line-height:normal'><span lang=IN style='font-size:9.0pt'>&nbsp;</span></p>
					</td>

					<td width=153 valign=top style='width:110.75pt;border:none;border-bottom:solid black 1.0pt;
					padding:0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
					text-align:center;line-height:normal'><span style='font-size:9.0pt'>Received
					as per request by</span></p>

					</td>
					<td width=16 valign=top style='width:15.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
					text-align:center;line-height:normal'><span lang=IN style='font-size:9.0pt'>&nbsp;</span></p>
					</td>

					<td width=148 valign=top style='width:97.75pt;border:none;border-bottom:solid black 1.0pt;
					padding:0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
					text-align:center;line-height:normal'><span style='font-size:9.0pt'>Undertaken
					by User</span></p>
					</td>

					<td width=16 valign=top style='width:15.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
					text-align:center;line-height:normal'><span style='font-size:9.0pt'>&nbsp;</span></p>
					</td>

					<td width=144 valign=top style='width:97.75pt;border:none;border-bottom:
					solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
					text-align:center;line-height:normal'><span style='font-size:9.0pt'>Acknowledged
					by</span></p>
					</td>
				</tr>
<tr style='height:37.85pt'>
	<td width=102 valign=top style='width:15.75pt;border:none;border-bottom:solid black 1.0pt;
	padding:0cm 5.4pt 0cm 5.4pt;height:37.85pt'>
	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
	justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
	style='font-size:9.0pt'>&nbsp;</span></p>
</td>
<td width=114 valign=top style='width:15.75pt;border:none;border-bottom:solid black 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:37.85pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:9.0pt'>&nbsp;</span></p>
</td>
<td width=19 valign=top style='width:15.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
height:37.85pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:9.0pt'>&nbsp;</span></p>
</td>
<td width=153 valign=top style='width:15.75pt;border:none;border-bottom:solid black 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:37.85pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:9.0pt'>&nbsp;</span></p>
</td>
<td width=16 valign=top style='width:15.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
height:37.85pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:9.0pt'>&nbsp;</span></p>
</td>
<td width=148 valign=top style='width:57.75pt;border:none;border-bottom:solid black 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:37.85pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span
style='font-size:9.0pt'>&nbsp;</span></p>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span
style='font-size:9.0pt'>&nbsp;</span></p>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span
style='font-size:9.0pt'>&nbsp;</span></p>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span
style='font-size:9.0pt'>&nbsp;</span></p>
</td>
<td width=16 valign=top style='width:15.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
height:37.85pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span
style='font-size:9.0pt'>&nbsp;</span></p>
</td>
<td width=144 valign=top style='width:57.75pt;border:none;border-bottom:
solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:37.85pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span
style='font-size:9.0pt'>&nbsp;</span></p>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span
style='font-size:9.0pt'>&nbsp;</span></p>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span
style='font-size:9.0pt'>&nbsp;</span></p>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span
style='font-size:9.0pt'>&nbsp;</span></p>
</td>
</tr>
<tr style='height:11.35pt'>
	<td width=102 valign=top style='width:57.75pt;border:none;border-bottom:solid black 1.0pt;
	padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
	<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
	text-align:center;line-height:normal'><span style='font-size:8.0pt'>
							@foreach($data['employee'] as $employee)
								@if(Session::get('logged_in')['nik'] == $employee->number)
									{{ $employee->fullname }}
								@endif
							@endforeach
	</span></p>
</td>
<td width=114 valign=top style='width:57.75pt;border:none;border-bottom:solid black 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span lang=IN style='font-size:8.0pt'>
	@foreach($data['employee'] as $employee)
								@if($data['allSign'][0]->verifiedby == $employee->number)
									{{ $employee->fullname }}
								@endif
							@endforeach</span></p>
</td>
<td width=19 valign=top style='width:15.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
height:11.35pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:7.0pt'>&nbsp;</span></p>
</td>
<td width=153 valign=top style='width:15.75pt;border:none;border-bottom:solid black 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span lang=IN style='font-size:8.0pt'>
	@foreach($data['employee'] as $employee)
			@if($data['transactionHeader'][0]->nik_requestedby == $employee->number)
			{{ $employee->fullname }}
			@endif
			@endforeach
<!-- requested by--></span></p>
</td>
<td width=16 valign=top style='width:15.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
height:11.35pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:7.0pt'>&nbsp;</span></p>
</td>

<td width=114 valign=top style='width:57.75pt;border:none;border-bottom:solid black 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span lang=IN style='font-size:8.0pt'>
	@foreach($data['employee'] as $employee)
			@if($data['transactionHeader'][0]->nik_undertakenby == $employee->number)
			{{ $employee->fullname }}
			@endif
			@endforeach</span></p>
</td>
<td width=16 valign=top style='width:15.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
height:11.35pt'>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:7.0pt'>&nbsp;</span></p>
</td>
<td width=144 valign=top style='width:57.75pt;border:none;border-bottom:
solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span lang=IN style='font-size:8.0pt'>
	@foreach($data['employee'] as $employee)
								@if($data['allSign'][0]->acknowledgedby == $employee->number)
									{{ $employee->fullname }}
								@endif
							@endforeach</span></p>
</td>
</tr>
<tr style='height:11.35pt'>
	<td width=102 valign=top style='width:57.75pt;border:none;border-bottom:solid black 1.0pt;
	padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
	<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
	text-align:center;line-height:normal'><span style='font-size:8.0pt'>
							@foreach($data['employee'] as $employee)
								@if(Session::get('logged_in')['nik'] == $employee->number)
									@foreach($data['division'] as $division)
										@if($employee->id_job == $division->id)
										{{ $division->description }}
										@endif
									@endforeach
								@endif
							@endforeach
	</span></p>
</td>
<td width=114 valign=top style='width:57.75pt;border:none;border-bottom:solid black 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span lang=IN style='font-size:8.0pt'>
	@foreach($data['employee'] as $employee)
								@if($data['allSign'][0]->verifiedby == $employee->number)
									@foreach($data['division'] as $division)
										@if($employee->id_job == $division->id)
										{{ $division->description }}
										@endif
									@endforeach
								@endif
							@endforeach
</span></p>
</td>
<td width=19 valign=top style='width:15.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
height:11.35pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:8.0pt'>&nbsp;</span></p>
</td>
<td width=153 valign=top style='width:15.75pt;border:none;border-bottom:solid black 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:8.0pt'>
	@foreach($data['employee'] as $employee)
								@if($data['transactionHeader'][0]->nik_requestedby == $employee->number)
									@foreach($data['division'] as $division)
										@if($employee->id_job == $division->id)
										{{ $division->description }}
										@endif
									@endforeach
								@endif
							@endforeach<!-- requested by -->
</span></p>
</td>
<td width=16 valign=top style='width:15.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
height:11.35pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal'><span lang=IN
style='font-size:8.0pt'>&nbsp;</span></p>
</td>
<td width=148 valign=top style='width:57.75pt;border:none;border-bottom:solid black 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span lang=IN style='font-size:8.0pt'>
							@foreach($data['employee'] as $employee)
								@if($data['transactionHeader'][0]->nik_undertakenby == $employee->number)
									@foreach($data['division'] as $division)
										@if($employee->id_job == $division->id)
										{{ $division->description }}
										@endif
									@endforeach
								@endif
							@endforeach<!-- requested by --></span></p>
</td>
<td width=16 valign=top style='width:15.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
height:11.35pt'>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:8.0pt'>&nbsp;</span></p>
</td>
<td width=144 valign=top style='width:57.75pt;border:none;border-bottom:
solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:8.0pt'>
							@foreach($data['employee'] as $employee)
								@if($data['allSign'][0]->acknowledgedby == $employee->number)
									@foreach($data['division'] as $division)
										@if($employee->id_job == $division->id)
										{{ $division->description }}
										@endif
									@endforeach
								@endif
							@endforeach
							</span></p>
</td>
</tr>
<tr style='height:11.35pt'>
	<td width=102 valign=top style='width:57.75pt;border:none;border-bottom:solid black 1.0pt;
	padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
	<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
	normal'><span style='font-size:8.0pt'>Date : {{ date("d-m-Y") }}</span></p>
</td>
<td width=114 valign=top style='width:57.75pt;border:none;border-bottom:solid black 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:8.0pt'>Date : {{ date("d-m-Y") }}</span></p>
</td>
<td width=19 valign=top style='width:15.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
height:11.35pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=IN style='font-size:8.0pt'>&nbsp;</span></p>
</td>
<td width=153 valign=top style='width:15.75pt;border:none;border-bottom:solid black 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:8.0pt'>Date : {{ date("d-m-Y") }}</span></p>
</td>
<td width=16 valign=top style='width:11.75pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
height:11.35pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=IN style='font-size:8.0pt'>&nbsp;</span></p>
</td>
<td width=148 valign=top style='width:57.75pt;border:none;border-bottom:solid black 1.0pt;
padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:8.0pt'>Date : {{ date("d-m-Y") }}</span></p>
</td>
<td width=16 valign=top style='width:15.8pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
height:11.35pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:8.0pt'>&nbsp;</span></p>
</td>
<td width=144 valign=top style='width:57.65pt;border:none;border-bottom:
solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:11.35pt'>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:8.0pt'>Date : {{ date("d-m-Y") }}</span></p>
</td>
</tr>
</table>

<!-- -->
</p>



<!-- -->
</div>
</div>
<div class="col-md-2"></div>
</div>

@endsection
