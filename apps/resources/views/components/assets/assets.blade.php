@extends('layouts.apps')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row">
	<div class="col-md-12">
		<button id="button"></button>
		<span style="float: right;">
			<!-- LINK ADD ASSETS-->
			<a href="{{ url('/') }}/asset/createAssets" class="btn btn-primary"><i class="fa fa-plus"></i> Add Assets</a>
		</span>
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Asset</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive table-striped" id="table-assets">
						<thead>					
							<th>Asset ID</th>
							<th>Parent ID</th>
							<th>Asset's Name</th>
							<th>Serial Number</th>
							<th>Asset's Brand</th>
							<th>Asset's Vendor</th>
							<th>created_at Date</th>
							<th>tools</th>
						</thead>
					</table>
				</div>
		</div>
	</div>
</div>	
@endsection
