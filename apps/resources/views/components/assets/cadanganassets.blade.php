@extends('layouts.apps')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row">
	<div class="col-md-12">
		<span style="float: right;">
			<!-- LINK ADD ASSETS-->
			<a href="{{ url('/') }}/asset/createAssets" class="btn btn-primary"><i class="fa fa-plus"></i> Add Assets</a>
		</span>
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Assets</center></div>
			<input type="text" id="search" placeholder="search"><button id=search>search</button>
				<div class="panel-body">
					<table class="table table-bordered table-responsive" id="table-asset">
						<thead>
							<!--<th>#</th>-->
							<th width="15%">Assets ID</th>
							<th width="15%">Assets's Names</th>
							<!-- <th>PO Numbers</th>-->
							<th width="15%">Serial Number</th>
							<!--<th width="15%">Prices</th>-->
							<th width="10%">Brands</th>
							<th width="10%">Vendors</th>
							<!-- <th width="10%">Purchasing dates</th>-->
							<th width="10%">Tools</th>
						</thead>
						<?php $i=1; ?>
						@if ($data['assetData']->count() > 0)
						@foreach($data['assetData'] as $assetData)
						<tr class="okok">
							<!--<td></td>-->
							<td>{{$assetData->idAsset}}</td>
							<td>{{$assetData->name}}</td>
							<!--<td>{{$assetData->po_numbers}}</td>-->
							<td>{{$assetData->serial_number}}</td>
							<!--<td>{{$assetData->prices}}</td>-->
							<td>
								@foreach($data['assetBrand'] as $assetBrand)
								@if($assetData->id_brands == $assetBrand->id)
								{{ $assetBrand->brandName }}
								@endif
								@endforeach
							</td>
							<td>
								@foreach($data['assetVendor'] as $assetVendor)
								@if($assetData->id_brands == $assetVendor->id)
								{{ $assetVendor->vendorName }}
								@endif
								@endforeach
							</td>
							<!--<td>{{$assetData->buyingDates}}</td>-->
							<td>
								<a href="{{ url('/asset/addTransaction/'.$assetData->idAsset) }}">
									<i class="fa fa-plus-circle" style="font-size:15px;color:blue"></i>
								</a> |
								
								<a href="{{ url('/asset/editAsset/'.$assetData->idAsset) }}">
									<i class="fa fa-pencil" style="font-size:15px;color:blue"></i>
								</a> |
								<a href="{{ url('/asset/detailAsset/'.$assetData->idAsset) }}">
									<i class="fa fa-search" style="font-size:15px;color:blue"></i>
								</a> |
								<!-- <button style="font-size:15px" class="btn">Show</button> -->
								<a href="{{ url('/asset/transactionHistory/'.$assetData->idAsset) }}">
									<i class="fa fa-book" style="font-size:15px;color:blue"></i>
								</a> |
								<a href="http://assets.centratamagroupdev.com/itss/{!! $data['assetData'][0]->file_html !!}">
									<i class="fa fa-link" style="font-size:15px;color:blue"></i>
								</a> |
								<button style="font-size:10px" id="{{ $i }}" class="show{{ $i }}">SHOW<i class="fa fa-eye"></i></button>
								<button style="font-size:10px" id="{{ $i }}" class="hide{{ $i }}">HIDEN<i class="fa fa-eye-slash"></i></button>
							</td>
						</tr>
						<div style="display:none">
							<tr class="ok{{ $i }}" style="display:none">
								<td colspan="10" align="center">Embeded Asset</td>
							</tr>

							<tr class="ok{{ $i }}" style="display:none">
								<!--<td></td>-->
								<td></td>
								<td align="center">Asset's Name</td>
								<td colspan="" align="center">Category</td>
								<td colspan="2" align="center">Descriptioin</td>
								<td align="center">TOOLS</td>
							</tr>
							@foreach($data['assetDatas'] as $check)
							@if($assetData->idAsset == $check->id_parent)
								<tr  class="ok{{ $i }}" style="display:none">
									
									    <!--<td></td>-->
									    <td></td>
									    <td align="center">{{ $check->name }}</td>
										<td colspan="" align="center">
											@foreach($data['assetType'] as $assetType)
											@if($assetType->id == $check->id_types)
											{{ $assetType->typeName }}
											@endif
											@endforeach
										</td>
										<td colspan="2" align="center">{{ $check->descriptions }}</td>
										<td align="center">
											<a href="{{ url('/asset/child/deleteAssetChild/'.$check->idAsset) }}">
												<i class="fa fa-close" style="font-size:15px;color:red"></i>
											</a>
										</td>
									
								</tr>

								
							@endif
							@endforeach
							<tr class="ok{{ $i }}" style="display:none">
								<td colspan="10" align="center"></td>
							</tr>
						</div>
						<?php $i++; ?>
						@endforeach
						@endif
					</table>
					{{ $data['assetData']->links() }}
				</div>
		</div>
	</div>
</div>	

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$("button").click(function() {
    //alert(this.id);// or alert($(this).attr('id'));


    var id = this.id;
    var classs = this.className;
    var lable = $("." + classs).text().trim();
    //alert(classs);

    /*var lable = $(this).text().trim();
    
    alert(classs);
    var class = $(".show" + id).text().trim();*/
    
    if(lable == "HIDEN") {
    	$("tr.ok"+ id).hide();
    }
    else{
    	$("tr.ok"+ id).show();
    }
});
</script>	

<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#table-asset tr.okok").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
@endsection
