@extends('layouts.apps')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Create a New Asset</center></div>
				<div class="panel-body">
					<legend>Open a New Asset</legend>
					<div>
					<!-- INSERT DATA -->
					<div style="" id="form-problem">
						<p>Please fill in the form below to create New Asset</p>

						<!-- TABLE INPUT DATA ASSET -->
						<table class="table table-bordered">
							<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

							<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

							{{ csrf_field() }}
									
							<!-- ASSET'S NAME -->
							<tr>
								<th>Asset's Name :</th>
								<td><input class="form-control" type="text" name="asset[assetName]" id="assetName" required>
								</td>
							</tr>

							<!-- ASSET'S PHOTO -->
							<tr>
								<th>Asset's PHOTO :</th>
								<td>
									<input name="asset[assetPhoto]" id="assetPhoto" type="file" onchange="readURL(this);" class="form-control" name="assetPhoto">
									<img src="http://placehold.it/200x200" class="img-responsive" id="assetPhotoShow" />
								</td>
							</tr>

							<!-- ASSET'S MAC ADRESSs -->
							<tr>
								<th>Asset's MAC Adress :</th>
								<td>
									<input name="asset[assetMacAdress]" id="assetMacAdress" type="text"  class="form-control" name="assetMacAdress">
									
								</td>
							</tr>

							<!-- ASSET'S PHOTO -->
							<tr>
								<th>Asset's Berlac :</th>
								<td>
									<input name="asset[assetBerlac]" onchange="readURLBerlac(this);" id="assetBerlac" type="file" class="form-control" name="assetBerlac">
									<object data="" type="application/pdf" width="100%" height="100%" id="assetBerlacShow" >
									</object>
								</td>
							</tr>

							<!-- ASSET'S Type -->
							<tr>
								<th>Asset Type:</th>
								<td>
									<!-- Getting the data from model AssetType -->
									<select class="form-control" name="asset[assetType]" id="assetType" required>
										<option value="">--Choose--</option>
										@foreach ($data['assetType'] as $assetType)
										<option value="{{$assetType->id}}">{{$assetType->typeDescription}}</option>
										@endforeach
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>

							<!-- PO NUMBER-->
							<tr>
								<th>PO Number :</th>
								<td><input class="form-control" type="text" name="asset[poNumber]" id="poNumber" value="" required></td>
							</tr>

							<!-- PRICE-->
							<tr>
								<th>Price :</th>
								<td><input class="form-control uang" type="text" name="asset[assetPrice]" id="assetPrice" value="" required></td>
							</tr>

							<!-- COMPANY  -->
							<tr>
								<th>Company :</th>
								<td>
									<!-- Getting the data from model AssetVendor -->
									<select class="form-control" name="asset[assetCompany]" id="company" required>
										<option value="">--Choose--</option>
										@foreach ($data['company'] as $assetCompany)
										<option value="{{$assetCompany->id}}">{{$assetCompany->name}}</option>
										@endforeach
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>

							<!-- VENDORS -->
							<tr>
								<th>Vendor :</th>
								<td>
									<!-- Getting the data from model AssetVendor -->
									<select class="form-control" name="asset[assetVendor]" id="assetVendor" required>
										<option value="">--Choose--</option>
										@foreach ($data['assetVendor'] as $assetVendor)
										<option value="{{$assetVendor->id}}">{{$assetVendor->vendorName}}</option>
										@endforeach
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>

							<!-- BRANDS -->
							<tr>
								<th>Brand : </th>
								<td>
									<!-- Getting the data from model AssetVendor -->
									<select class="form-control" name="asset[assetBrand]" id="assetBrand" required>
										<option value="">--Choose--</option>
										@foreach ($data['assetBrand'] as $assetBrand)
										<option value="{{$assetBrand->id}}">{{$assetBrand->brandName}}</option>
										@endforeach
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>

							<!-- SERIAL NUMBERS -->
							<tr>
								<th>Serial Number :</th>
								<td><input class="form-control" type="text" name="asset[serialNumber]" id="serialNumber" required></td>
							</tr>

							<!-- Tanggal Penerimaan -->
							<tr>
								<th> Purchase Date </th>
								<td><input type="date" id="assetBuyingDate" name="asset[assetBuyingDate]" required></td>
							</tr>

							<!-- Guarantee Date -->
							<tr>
								<th> Guarantee Date </th>
								<td><input type="date" id="assetGuaranteeDate" name="asset[assetBuyingDate]" required></td>
							</tr>

							<!-- CONDITIONS -->
							<tr>
								<th>Condition : </th>
								<td>
									<!-- Getting the data from model AssetCondition -->
									<select class="form-control" name="asset[assetCondition]" id="assetCondition" required>
										<option value="">--Choose--</option>
										@foreach ($data['assetCondition'] as $assetCondition)
										<option value="{{$assetCondition->id}}">{{$assetCondition->conditionDescription}}</option>
										@endforeach
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>		

							<!-- STATUS REFISI -->
							<tr>
								<th>Status : </th>
								<td>
									<!-- Getting the data from model AssetStatus -->
									<select class="form-control" name="asset[assetStatus]" id="assetStatus" required>
										<option value="">--Choose--</option>
										<option value="1"> AVAILABLE </option>
										<option value="2"> ACTIVE </option>
										<option value="3"> DISPOSAL </option>
										<option value="4"> GIVEN TO EMPLOYEE </option>
										
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>

							<!-- LOCATION -->
							<tr>
								<th>Location : </th>
								<td>
									<!-- Getting the data from model AssetLocation -->
									<select class="form-control" name="asset[assetLocation]" id="assetLocation" required>
										<option value="">--Choose--</option>
										@foreach ($data['assetLocation'] as $assetLocation)
										<option value="{{$assetLocation->id}}">{{$assetLocation->locationDescription}}</option>
										@endforeach
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>	

							<!-- DESCRIPTION -->
							<tr>
								<th>Description :</th>
								<td>
									<textarea class="form-control input-lg" name="asset[assetDescription]" id="assetDescription" rows="10" required></textarea>
								</td>
							</tr>

							<!-- ID PAR -->
							<tr>
								<th>ID Parent:</th>
								<td>
									<select class="form-control" name="assetType">
										<option value="">--Choose Type--</option>
										@foreach ($data['assetType'] as $assetType)
										<option value="{{$assetType->id}}">{{$assetType->typeDescription}}</option>
										@endforeach
									</select>
									<select class="form-control" name="idParent" id="idParent" required>
										<option value="">--Choose--</option>
									</select>
								</td>
							</tr>

							<!-- SUBMIT BUTTON -->
							<tr>
								<td colspan="2" align="right">
									<input type="reset" name="reset" value="Reset" class="btn btn-danger">
									<button name="submit" class="btn btn-primary" id="btn-submit-problem">Add Asset</button>
								</td>
							</tr>


						</table>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<script type="text/javascript">
function readURL(input) 
{
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#assetPhotoShow" )
      .attr('src', e.target.result)
      .width(150)
      .height(200);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

function readURLBerlac(input) 
{
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#assetBerlacShow" )
      .attr('data', e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
  }
}
</script>



<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){
		$( '.uang' ).mask('000.000.000', {reverse: true});

		$('select[name="assetType"]').on('change', function(){
        var idType = $(this).val();
        var url = "{{url('/')}}/asset/getAssetFromType/"+idType;
        if(idType) {
            $.ajax({
                url: url,
                type:"GET",
                dataType:"json",
                success:function(data) {
                    $.each(data, function(key, value){
                        $('select[name="idParent"]').append(
                        	'<option value="'+ value +'" selected>' + key + '|' + value + '</option>'
                        	);

                    });
                },
                error: function(ts) { 
                	alert(ts.responseText);
                	
                }

            });
        } 
    });
		$('#btn-submit-problem').click(function(e){

			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();
			
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('nik', ($("#nik").val()));
			formData.append('email', ($("#email").val()));
			formData.append('assetType', ($("#assetType").val()));
			formData.append('assetName', ($("#assetName").val()));
			formData.append('idParent', ($("#idParent").val()));
			formData.append('poNumber', ($("#poNumber").val()));
			formData.append('assetPrice', ($("#assetPrice").val()));
			formData.append('assetVendor', ($("#assetVendor").val()));
			formData.append('assetBrand', ($("#assetBrand").val()));
			formData.append('serialNumber', ($("#serialNumber").val()));
			formData.append('assetCondition', ($("#assetCondition").val()));
			formData.append('assetPhoto', ($('#assetPhoto')[0].files[0]));
			formData.append('assetBerlac', ($('#assetBerlac')[0].files[0]));
			formData.append('assetStatus', ($("#assetStatus").val()));
			formData.append('assetMacAdress', ($("#assetMacAdress").val()));
			formData.append('assetDescription', ($("#assetDescription").val()));
			formData.append('assetLocation', ($("#assetLocation").val()));
			formData.append('assetBuyingDate', ($("#assetBuyingDate").val()));
			formData.append('assetGuaranteeDate', ($("#assetGuaranteeDate").val()));
			formData.append('company', ($("#company").val()));
			//formData.append('assetPhoto', ($('#assetPhoto')[0].files[0]));
			//formData.append('assetBerlac', ($('#assetBerlac')[0].files[0]));

			//SEND DATA TO ROUTES
			var url = "{{url('/')}}/asset/saveAssets";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/asset";
				}
			});
		});
	});
</script>
<!-- end -->
@endsection
