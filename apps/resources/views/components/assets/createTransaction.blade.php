@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Create a transaction</center></div>
				<div class="panel-body">
					<legend>Open a New transaction</legend>
					<div>
					<!-- INSERT DATA TRANSACTION -->
					<div style="" id="form-problem">
						<p>Please fill in the form below to create New transaction</p>

						<form name="addData" id="addData">
							<!-- TABLE INPUT DATA ASSET -->
							<table class="table table-bordered">
								<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

								<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

								{{ csrf_field()}}

								<input id="idList" value="{{ $i=1 }}" type="hidden" />

								<!-- Requested by -->
								<tr>
									<th>Requested By:</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="asset[requester]" id="requester">
											<option value="">--Choose--</option>
											@foreach ($data['employee'] as $employee)
											<option value="{{$employee->number}}">{{$employee->fullname}}</option>
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- Undertaken by-->
								<tr>
									<th>Undertaken by</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="asset[employee]" id="employee">
											<option value="">--Choose--</option>
											@foreach ($data['employee'] as $employee)
											<option value="{{$employee->number}}">{{$employee->fullname}}</option>
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- ASSET -->
								<tr>
									<th>ASSET:</th>
									<td>
										<select class="form-control" name="asset" id="idAsset">
											<option value="">--Choose--</option>
											@foreach ($data['assetData'] as $assetData)
											<option value="{{$assetData->idAsset}}">{{$assetData->idAsset}}</option>
											@endforeach
										</select>
									</td>
								</tr>

								<!-- STATUS ASSET -->
								<tr>
									<th>STATUS :</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="asset[assetStatus]" id="assetStatus">
											<option value="">--Choose--</option>
											@foreach ($data['assetStatus'] as $assetStatus)
											<option value="{{$assetStatus->id}}">
												{{$assetStatus->statusName}}
											</option>
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- RECEIVING DATE -->
								<tr>
									<th> Receiving Date </th>
									<td><input type="date" id="receivingDate" name="asset[receivingDate]"></td>
								</tr>

								<!-- RECEIVING CONDITIONS -->
								<tr>
									<th>Reciving Condition : </th>
									<td>
										<!-- Getting the data from model AssetCondition -->
										<select class="form-control" name="asset[receivingCondition]" id="receivingCondition">
											<option value="">--Choose--</option>
											@foreach ($data['assetCondition'] as $assetCondition)
											<option value="{{$assetCondition->id}}">{{$assetCondition->conditionDescription}}</option>
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>		

								<!-- LOCATION -->
								<tr>
									<th>Location : </th>
									<td>
										<!-- Getting the data from model AssetLocation -->
										<select class="form-control" name="asset[assetLocation]" id="assetLocation">
											<option value="">--Choose--</option>
											@foreach ($data['assetLocation'] as $assetLocation)
											<option value="{{$assetLocation->id}}">{{$assetLocation->locationDescription}}</option>
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>	

								<!-- DESCRIPTION -->
								<tr>
									<th>Description :</th>
									<td>
										<textarea class="form-control input-lg" name="asset[assetDescription]" id="assetDescription" rows="10"></textarea>
									</td>
								</tr>

								<!-- SUBMIT BUTTON -->
								<tr>
									<td colspan="2" align="right">
										<input type="reset" name="reset" value="Reset" class="btn btn-danger">
										<button name="submit" class="btn btn-primary" id="btn-submit-problem">ADD</button>
									</td>
								</tr>
							</table>
						</form>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){

		$('#btn-submit-problem').click(function(e){
			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();


			
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('nik', ($("#nik").val()));
			formData.append('email', ($("#email").val()));
			formData.append('idAsset', ($("#idAsset").val()));
			formData.append('employee', ($("#employee").val()));
			formData.append('requester', ($("#requester").val()));
			formData.append('receivingDate', ($("#receivingDate").val()));
			formData.append('receivingCondition', ($("#receivingCondition").val()));
			formData.append('assetDescription', ($("#assetDescription").val()));
			formData.append('assetLocation', ($("#assetLocation").val()));
			formData.append('assetStatus', ($("#assetStatus").val()));

			//SEND DATA TO ROUTES
			var url = "{{url('/')}}/transaction/saveTransaction";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/transaction";
				}
			});
		});
	});

</script>
<!-- end -->
@endsection
