@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Add a new asset to the list of {{ $idTransaction }}</center></div>
				<div class="panel-body">
					<legend></legend>
					<div>
					<!-- INSERT DATA TRANSACTION -->
					<div style="" id="form-problem">
						<p>Please fill in the form below to add new asset</p>

						<form name="addData" id="addData">
							<!-- TABLE INPUT DATA ASSET -->
							<table class="table table-bordered">
								<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

								<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

								<input type="hidden" value="{{ $idTransaction }}" id="idTransaction">

								{{ csrf_field() }}

								<!-- Requested by -->
								<tr>
									<th>Asset :</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="idAsset" id="idAsset">
											<option value="">--Choose--</option>

											@if($data['transactionHeader'][0]->id_status == 4)
												@foreach ($data['assetDataActive'] as $asset)
													<option value="{{$asset->idAsset}}">
														{{ $asset->name }} | 
														{{ $asset->brandName }} |
														{{ $asset->idAsset }} |
														{{$asset->serial_number}}
													</option>
												@endforeach
											@else
												@foreach ($data['assetDataAvailable'] as $asset)
													<option value="{{$asset->idAsset}}">
														{{ $asset->name }} | 
														{{ $asset->brandName }} |
														{{ $asset->idAsset }} |
														{{$asset->serial_number}}
													</option>
												@endforeach
											@endif
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- Condition Name -->
								<tr>
									<th>Asset Condition :</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="assetCondition" id="assetCondition">
											<option value="">--Choose--</option>
											@foreach ($data['assetCondition'] as $assetCondition)
											<option value="{{$assetCondition->id}}">{{$assetCondition->conditionDescription}}</option>
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- DESCRIPTION -->
								<tr>
									<th>Description :</th>
									<td>
										<textarea class="form-control input-lg" name="asset[assetDescription]" id="description" rows="10"></textarea>
									</td>
								</tr>

								<!-- SUBMIT BUTTON -->
								<tr>
									<td colspan="2" align="right">
										<input type="reset" name="reset" value="Reset" class="btn btn-danger">
										<button name="submit" class="btn btn-primary" id="btn-submit-problem">ADD</button>
									</td>
								</tr>
							</table>
						</form>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){

		var selectedAsset = $('#idAsset').val();

		$('#btn-submit-problem').click(function(e){
			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();

			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('nik', ($("#nik").val()));
			formData.append('email', ($("#email").val()));
			formData.append('idAsset', ($("#idAsset").val()));
			formData.append('idTransaction', ($("#idTransaction").val()));
			formData.append('assetCondition', ($("#assetCondition").val()));
			formData.append('description', ($("#description").val()));

			//SEND DATA TO ROUTES
			var url = "{{url('/')}}/transaction/saveTransactionDetail";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/transaction/detailTransaction/{{$idTransaction}}";
				}
			});
		});

		$('select[name="idAsset"]').on('change', function(){
        var idAsset = $(this).val();
        var url = "{{url('/')}}/asset/getStatus/"+idAsset;
        if(idAsset) {
            $.ajax({
                url: url,
                type:"GET",
                dataType:"json",
                success:function(data) {
                    $.each(data, function(key, value){
                        $('select[name="assetCondition"]').append(
                        	'<option value="'+ value +'" selected>' + key + '</option>'
                        	);

                    });
                },
                error: function(ts) { 
                	alert(ts.responseText);
                	
                }

            });
        } 

        else
        {
            $('select[name="state"]').empty();
        }

    	});
	});

</script>
<!-- end -->
@endsection
