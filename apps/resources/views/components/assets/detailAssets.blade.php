@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Detail Asset</center></div>
			<div class="panel-body">
				<legend>Detail Asset</legend>
				<div>
					<!-- EDIT DATA -->
					<div style="" id="form-problem">
						<p></p>

						<!-- TABLE INPUT DATA ASSET -->
						<table class="table table-bordered">
							<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

							<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

							{{ csrf_field()}}

							<input class="form-control" type="text" name="asset[idAsset]" id="idAsset" value="{{ $data['assetData'][0]->idAsset }}" disabled>


							<!-- ASSET'S NAME -->
							<tr>
								<th>Asset's Name :</th>
								<td>
									<input class="form-control" type="text" name="asset[assetName]" id="assetName" value="{{ $data['assetData'][0]->name }}" disabled>
								</td>
							</tr>

							<!-- ASSET'S PHOTO -->
							<tr>
								<th>Asset's PHOTO :</th>
								<td>
									<img src="http://assets.centratamagroupdev.com/itss/{!! $data['assetData'][0]->photo_asset !!}" id="assetPhotoShow" width="200" height="200" />
								</td>
							</tr>

							<!-- ASSET'S Type -->
							<tr>
								<th>Asset Type:</th>
								<td>
									<!-- Getting the data from model AssetType -->
									<select class="form-control" name="asset[assetType]" id="assetType" disabled>
										<option value="">--Choose--</option>
										@foreach ($data['assetType'] as $assetType)
										@if($data['assetData'][0]->id_types == $assetType->id)
										<option value="{{$assetType->id}}" selected>{{$assetType->typeDescription}}</option>
										@else
										<option value="{{$assetType->id}}">{{$assetType->typeDescription}}</option>
										@endif
										@endforeach
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>

							<!-- PO NUMBER-->
							<tr>
								<th>PO Number :</th>
								<td><input class="form-control" type="text" name="asset[poNumber]" id="poNumber" value="{{ $data['assetData'][0]->po_numbers }}" disabled></td>
							</tr>

							<!-- PRICE-->
							<tr>
								<th>Price :</th>
								<td><input class="form-control" type="text" name="asset[assetPrice]" id="assetPrice" value="{{ $data['assetData'][0]->prices }}" disabled></td>
							</tr>

							<!-- COMPANY  -->
							<tr>
								<th>Company :</th>
								<td>
									<!-- Getting the data from model AssetVendor -->
									<select class="form-control" name="asset[assetCompany]" id="company" disabled>
										<option value="">--Choose--</option>
										@foreach ($data['company'] as $assetCompany)
										@if($data['assetData'][0]->id_company == $assetCompany->id)
										<option value="{{$assetCompany->id}}" selected>{{$assetCompany->name}}</option>
										@else
										<option value="{{$assetCompany->id}}">{{$assetCompany->name}}</option>
										@endif
										@endforeach
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>

							<!-- VENDORS -->
							<tr>
								<th>Vendor :</th>
								<td>
									<!-- Getting the data from model AssetVendor -->
									<select class="form-control" name="asset[assetVendor]" id="assetVendor" disabled>
										<option value="">--Choose--</option>
										@foreach ($data['assetVendor'] as $assetVendor)
										@if($data['assetData'][0]->id_vendors == $assetVendor->id)
										<option value="{{$assetVendor->id}}" selected>{{$assetVendor->vendorName}}</option>
										@else
										<option value="{{$assetVendor->id}}">{{$assetVendor->vendorName}}</option>
										@endif
										@endforeach
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>

							<!-- BRANDS -->
							<tr>
								<th>Brand : </th>
								<td>
									<!-- Getting the data from model AssetVendor -->
									<select class="form-control" name="asset[assetBrand]" id="assetBrand" disabled>
										<option value="">--Choose--</option>
										@foreach ($data['assetBrand'] as $assetBrand)
										@if($data['assetData'][0]->id_brands == $assetBrand->id)
										<option value="{{$assetBrand->id}}" selected>{{$assetBrand->brandName}}</option>
										@else
										<option value="{{$assetBrand->id}}" >{{$assetBrand->brandName}}</option>
										@endif
										@endforeach
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>

							<!-- SERIAL NUMBERS -->
							<tr>
								<th>Serial Number :</th>
								<td><input class="form-control" type="text" name="asset[serialNumber]" id="serialNumber" value="{{$data['assetData'][0]->serial_number}}" disabled></td>
							</tr>

							<!-- Tanggal Penerimaan -->
							<tr>
								<th> Buying Date </th>
								<td><input type="date" id="assetBuyingDate" value="{{ $data['assetData'][0]->buyingDates }}" name="asset[assetBuyingDate]" disabled></td>
							</tr>

							<!-- CONDITIONS -->
							<tr>
								<th>Condition : </th>
								<td>
									<!-- Getting the data from model AssetCondition -->
									<select class="form-control" name="asset[assetCondition]" id="assetCondition" disabled>
										<option value="">--Choose--</option>
										@foreach ($data['assetCondition'] as $assetCondition)
										@if($data['assetData'][0]->id_conditions == $assetCondition->id)
										<option value="{{$assetCondition->id}}" selected>{{$assetCondition->conditionDescription}}</option>
										@else
										<option value="{{$assetCondition->id}}">{{$assetCondition->conditionDescription}}</option>
										@endif
										@endforeach
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>		

							<!-- STATUS -->
							<tr>
								<th>Status : </th>
								<td>
									<!-- Getting the data from model AssetStatus -->
									<select class="form-control" name="asset[assetStatus]" id="assetStatus" disabled>
										<option value="">--Choose--</option>
										
										@if($data['assetData'][0]->id_status == 1)
										<option value="1" selected> AVAILABLE </option>
										<option value="2"> ACTIVE </option>
										<option value="3"> DISPOSAL </option>
										@endif

										@if($data['assetData'][0]->id_status == 2)
										<option value="1"> AVAILABLE </option>
										<option value="2" selected> ACTIVE </option>
										<option value="3"> DISPOSAL </option>
										@endif

										@if($data['assetData'][0]->id_status == 3)
										<option value="1"> AVAILABLE </option>
										<option value="2"> ACTIVE </option>
										<option value="3" selected> DISPOSAL </option>
										@endif
										
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>

							<!-- LOCATION -->
							<tr>
								<th>Location : </th>
								<td>
									<!-- Getting the data from model AssetLocation -->
									<select class="form-control" name="asset[assetLocation]" id="assetLocation" disabled>
										<option value="">--Choose--</option>
										@foreach ($data['assetLocation'] as $assetLocation)
										@if($data['assetData'][0]->id_locations == $assetLocation->id)
										<option value="{{$assetLocation->id}}" selected>{{$assetLocation->locationDescription}}</option>
										@else
										<option value="{{$assetLocation->id}}">{{$assetLocation->locationDescription}}</option>
										@endif
										@endforeach
									</select>
									<!-- Finishing print data from model-->
								</td>
							</tr>

							<tr>
								<th>ID Parent:</th>
								<td>
									<select class="form-control" name="asset" id="idParent" disabled>
										<option value="">--Choose--</option>
										@foreach ($data['assetDatas'] as $assetData)
										@if( $data['assetData'][0]->id_parent == $assetData->id_parent)
										<option value="{{$assetData->idAsset}}" selected="">
											{{ $assetData->name }}|
											{{ $assetData->brandName }}|
											{{ $assetData->idAsset }}
										</option>

										@else
										<option value="{{$assetData->idAsset}}" selected="">
											{{ $assetData->name }}|
											{{ $assetData->brandName }}|
											{{ $assetData->idAsset }}
										</option>
										@endif

										@endforeach
									</select>
								</td>
							</tr>	

							<!-- DESCRIPTION -->
							<tr>
								<th>Description :</th>
								<td>
									<textarea class="form-control input-lg" name="asset[assetDescription]" id="assetDescription" rows="10" disabled>{{ $data['assetData'][0]->descriptions }}</textarea>
								</td>
							</tr>

							<!-- BARCODE -->
							<tr>
								<th>Barcode :</th>
								<td>
									{!! DNS2D::getBarcodeHTML($data['assetData'][0]->idAsset, "QRCODE") !!}
								</td>
							</tr>
						</table>
					</div>
					<!-- END INSERT DATA -->

				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function readURL(input) 
		{
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$("#assetPhotoShow" )
					.attr('src', e.target.result)
					.width(150)
					.height(200);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}
	</script>

	<!-- ajax save start -->
	<script type="text/javascript">
		$(document).ready(function(){
			$('#btn-submit-problem').click(function(e){

			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('nik', ($("#nik").val()));
			formData.append('email', ($("#email").val()));
			formData.append('assetType', ($("#assetType").val()));
			formData.append('assetName', ($("#assetName").val()));
			formData.append('poNumber', ($("#poNumber").val()));
			formData.append('assetPrice', ($("#assetPrice").val()));
			formData.append('assetVendor', ($("#assetVendor").val()));
			formData.append('assetBrand', ($("#assetBrand").val()));
			formData.append('serialNumber', ($("#serialNumber").val()));
			formData.append('assetCondition', ($("#assetCondition").val()));
			formData.append('assetStatus', ($("#assetStatus").val()));
			formData.append('assetDescription', ($("#assetDescription").val()));
			formData.append('assetLocation', ($("#assetLocation").val()));
			formData.append('assetBuyingDate', ($("#assetBuyingDate").val()));
			formData.append('idAsset', ($("#idAsset").val()));
			formData.append('company', ($("#company").val()));
			formData.append('assetPhoto', ($('#assetPhoto')[0].files[0]));


			

			//SEND DATA TO ROUTES
			var url = "{{url('/asset/saveEditAssets') }}";
			
			$.ajaxSetup({
				headers:
				{
					'X-CSRF-Token': $('input[name="_token"]').val()
				}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
				processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/assetku";
				}
			});
		});
		});
	</script>
	<!-- end -->
	@endsection
