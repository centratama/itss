@extends('layouts.apps')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Edit Asset</center></div>
			<div class="panel-body">
				<legend>Edit Asset</legend>
				<div>
					<!-- EDIT DATA -->
					<div style="" id="form-problem">
						<form name="addData" id="addData">

							<p>Please fill in the form below to edit asset</p>

							<!-- TABLE INPUT DATA ASSET -->
							<table class="table table-bordered">
								<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

								<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

								{{ csrf_field()}}

								<input class="form-control" type="text" name="asset[idAsset]" id="idAsset" value="{{ $data['assetData'][0]->idAsset }}" readonly>


								<!-- ASSET'S NAME -->
								<tr>
									<th>Asset's Name</th>
									<td>
										<input class="form-control" type="text" name="asset[assetName]" id="assetName" value="{{ $data['assetData'][0]->name }}">
									</td>
								</tr>

								<!-- ASSET'S PHOTO -->
								<tr>
									<th>Asset's PHOTO</th>
									<td>
										<input name="asset[assetPhoto]" id="assetPhoto" type="file" onchange="readURL(this);" class="form-control" name="assetPhoto" value="">
										<img src="http://assets.centratamagroupdev.com/itss/{!! $data['assetData'][0]->photo_asset !!}" id="assetPhotoShow" width="200" height="200" />
									</td>
								</tr>

								<!-- ASSET'S MAC ADRESSs -->
								<tr>
									<th>Asset's MAC Adress</th>
									<td>
										<input name="asset[assetMacAdress]" id="assetMacAdress" type="text" class="form-control" name="assetMacAdress" value="{{ $data['assetData'][0]->macAdress }}">
									</td>
								</tr>

								<!-- ASSET'S BERLAC -->
								<tr>
									<th>Asset's Berlac : {{ $data['assetData'][0]->file_html }}</th>
									<td>
										<input name="asset[assetBerlac]" id="assetBerlac" type="file" class="form-control" name="assetBerlac" value="" required>
										<object data="http://assets.centratamagroupdev.com/html/{!! $data['assetData'][0]->file_html !!}" type="application/pdf" width="100%" height="100%">
											<p><b>Example fallback content</b>: This browser does not support PDFs. Please download the PDF to view it: <a href="/pdf/sample-3pp.pdf">Download PDF</a>.</p>
										</object>

									</td>
								</tr>

								<!-- ASSET'S Type -->
								<tr>
									<th>Asset Type</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="asset[assetType]" id="assetType">
											<option value="">--Choose--</option>
											@foreach ($data['assetType'] as $assetType)
											@if($data['assetData'][0]->id_types == $assetType->id)
											<option value="{{$assetType->id}}" selected>{{$assetType->typeDescription}}</option>
											@else
											<option value="{{$assetType->id}}">{{$assetType->typeDescription}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- PO NUMBER-->
								<tr>
									<th>PO Number</th>
									<td><input class="form-control" type="text" name="asset[poNumber]" id="poNumber" value="{{ $data['assetData'][0]->po_numbers }}"></td>
								</tr>

								<!-- PRICE-->
								<tr>
									<th>Price</th>
									<td><input class="form-control uang" type="text" name="asset[assetPrice]" id="assetPrice" value="{{ $data['assetData'][0]->prices }}" ></td>
								</tr>

								<!-- COMPANY  -->
								<tr>
									<th>Company</th>
									<td>
										<!-- Getting the data from model AssetVendor -->
										<select class="form-control" name="asset[assetCompany]" id="company">
											<option value="">--Choose--</option>
											@foreach ($data['company'] as $assetCompany)
											@if($data['assetData'][0]->id_company == $assetCompany->id)
											<option value="{{$assetCompany->id}}" selected>{{$assetCompany->name}}</option>
											@else
											<option value="{{$assetCompany->id}}">{{$assetCompany->name}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
										<!-- -->
									</td>
								</tr>

								<!-- VENDORS -->
								<tr>
									<th>Vendor</th>
									<td>
										<!-- Getting the data from model AssetVendor -->
										<select class="form-control" name="asset[assetVendor]" id="assetVendor">
											<option value="">--Choose--</option>
											@foreach ($data['assetVendor'] as $assetVendor)
											@if($data['assetData'][0]->id_vendors == $assetVendor->id)
											<option value="{{$assetVendor->id}}" selected>{{$assetVendor->vendorName}}</option>
											@else
											<option value="{{$assetVendor->id}}">{{$assetVendor->vendorName}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- BRANDS -->
								<tr>
									<th>Brand</th>
									<td>
										<!-- Getting the data from model AssetVendor -->
										<select class="form-control" name="asset[assetBrand]" id="assetBrand">
											<option value="">--Choose--</option>
											@foreach ($data['assetBrand'] as $assetBrand)
											@if($data['assetData'][0]->id_brands == $assetBrand->id)
											<option value="{{$assetBrand->id}}" selected>{{$assetBrand->brandName}}</option>
											@else
											<option value="{{$assetBrand->id}}" >{{$assetBrand->brandName}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- SERIAL NUMBERS -->
								<tr>
									<th>Serial Number</th>
									<td><input class="form-control" type="text" name="asset[serialNumber]" id="serialNumber" value="{{$data['assetData'][0]->serial_number}}"></td>
								</tr>

								<!-- Tanggal Penerimaan -->
								<tr>
									<th> Purchase Date </th>
									<td><input type="date" id="assetBuyingDate" value="{{ $data['assetData'][0]->buyingDates }}" name="asset[assetBuyingDate]"></td>
								</tr>

								<!-- CONDITIONS -->
								<tr>
									<th>Condition : </th>
									<td>
										<!-- Getting the data from model AssetCondition -->
										<select class="form-control" name="asset[assetCondition]" id="assetCondition">
											<option value="">--Choose--</option>
											@foreach ($data['assetCondition'] as $assetCondition)
											@if($data['assetData'][0]->id_conditions == $assetCondition->id)
											<option value="{{$assetCondition->id}}" selected>{{$assetCondition->conditionDescription}}</option>
											@else
											<option value="{{$assetCondition->id}}">{{$assetCondition->conditionDescription}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>		

								<!-- STATUS -->
								<tr>
									<th>Status : </th>
									<td>
										<!-- Getting the data from model AssetStatus -->
										<select class="form-control" name="asset[assetStatus]" id="assetStatus">
											<option value="">--Choose--</option>

											@if($data['assetData'][0]->id_status == 1)
											<option value="1" selected> AVAILABLE </option>
											<option value="2"> ACTIVE </option>
											<option value="3"> DISPOSAL </option>
											@endif

											@if($data['assetData'][0]->id_status == 2)
											<option value="1"> AVAILABLE </option>
											<option value="2" selected> ACTIVE </option>
											<option value="3"> DISPOSAL </option>
											@endif

											@if($data['assetData'][0]->id_status == 3)
											<option value="1"> AVAILABLE </option>
											<option value="2"> ACTIVE </option>
											<option value="3" selected> DISPOSAL </option>
											@endif

										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- LOCATION -->
								<tr>
									<th>Location</th>
									<td>
										<!-- Getting the data from model AssetLocation -->
										<select class="form-control" name="asset[assetLocation]" id="assetLocation">
											<option value="">--Choose--</option>
											@foreach ($data['assetLocation'] as $assetLocation)
											@if($data['assetData'][0]->id_locations == $assetLocation->id)
											<option value="{{$assetLocation->id}}" selected>{{$assetLocation->locationDescription}}</option>
											@else
											<option value="{{$assetLocation->id}}">{{$assetLocation->locationDescription}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>	

								<!-- DESCRIPTION -->
								<tr>
									<th>Description</th>
									<td>
										<textarea class="form-control input-lg" name="asset[assetDescription]" id="assetDescription" rows="10">{{ $data['assetData'][0]->descriptions }}</textarea>
									</td>
								</tr>

								<!-- ASSET -->
								<tr>
									<th>ID Parent</th>
									<td>
										<select class="form-control" name="assetType">
											<option value="">--Choose Type--</option>
											@foreach ($data['assetType'] as $assetType)
											<option value="{{$assetType->id}}">{{$assetType->typeDescription}}</option>
											@endforeach
										</select>
										<select class="form-control" name="idParent" id="idParent" required>
											<option value="">--Choose--</option>
												@foreach ($data['assetDataAvailable'] as $assetAvailable)
												@if($assetAvailable->idAsset == $data['assetData'][0]->id_parent )
													<option value="{{$assetAvailable->idAsset}}" selected>
														{{$assetAvailable->name }} | 
														{{$assetAvailable->brandName }} |
														{{$assetAvailable->idAsset }} |
														{{$assetAvailable->serial_number}}
													</option>
												@endif
												@endforeach
										</select>
									</select>
								</td>
							</tr>

							<!-- SUBMIT BUTTON -->
							<tr>
								<td colspan="2" align="right">
									<input type="reset" name="reset" value="Reset" class="btn btn-danger">
									<button name="submit" class="btn btn-primary" id="btn-submit-problem">Edit Asset</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
				<!-- END INSERT DATA -->

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function readURL(input) 
	{
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$("#assetPhotoShow" )
				.attr('src', e.target.result)
				.width(150)
				.height(200);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){
		$( '.uang' ).mask('000.000.000', {reverse: true});

		$('select[name="assetType"]').on('change', function(){
        var idType = $(this).val();
        var url = "{{url('/')}}/asset/getAssetFromType/"+idType;
        if(idType) {
            $.ajax({
                url: url,
                type:"GET",
                dataType:"json",
                success:function(data) {
                    $.each(data, function(key, value){
                        $('select[name="idParent"]').append(
                        	'<option value="'+ value +'" selected>' + key + '|' + value + '</option>'
                        	);

                    });
                },
                error: function(ts) { 
                	alert(ts.responseText);
                	
                }

            });
        } 
    });
		$('#btn-submit-problem').click(function(e){

			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('nik', ($("#nik").val()));
			formData.append('email', ($("#email").val()));
			formData.append('idParent', ($("#idParent").val()));
			formData.append('assetType', ($("#assetType").val()));
			formData.append('assetName', ($("#assetName").val()));
			formData.append('poNumber', ($("#poNumber").val()));
			formData.append('assetPrice', ($("#assetPrice").val()));
			formData.append('assetVendor', ($("#assetVendor").val()));
			formData.append('assetBrand', ($("#assetBrand").val()));
			formData.append('serialNumber', ($("#serialNumber").val()));
			formData.append('assetCondition', ($("#assetCondition").val()));
			formData.append('assetStatus', ($("#assetStatus").val()));
			formData.append('assetDescription', ($("#assetDescription").val()));
			formData.append('assetLocation', ($("#assetLocation").val()));
			formData.append('assetBuyingDate', ($("#assetBuyingDate").val()));
			formData.append('idAsset', ($("#idAsset").val()));
			formData.append('company', ($("#company").val()));
			formData.append('assetPhoto', ($('#assetPhoto')[0].files[0]));
			formData.append('assetBerlac', ($('#assetBerlac')[0].files[0]));
			formData.append('assetMacAdress', ($("#assetMacAdress").val()));

			//SEND DATA TO ROUTES
			var url = "{{url('/asset/saveEditAssets') }}";
			
			$.ajaxSetup({
				headers:
				{
					'X-CSRF-Token': $('input[name="_token"]').val()
				}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
				processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/asset";
				}
			});
		});
	});
</script>
<!-- end -->
@endsection
