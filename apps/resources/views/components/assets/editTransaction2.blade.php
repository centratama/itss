@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center> Form pengembalian Asset For {{$idTransaction}}</center></div>
				<div class="panel-body">
					<legend></legend>
					<div>
					<!-- INSERT DATA -->
					<div style="" id="form-problem">
						<p>Please fill the returning date and returning condition</p>

						<form name="addData" id="addData">
							<!-- TABLE INPUT DATA ASSET -->
							<table class="table table-bordered">
								<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

								<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

								{{ csrf_field()}}

								<!-- ID ASSETS -->
								<input type="hidden" name="asset[idTransaction]" value="{{ $idTransaction }}" id="idTransaction">

								
								

								<!-- EMPLOYEE -->
								<tr>
									<th>Requested By:</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="asset[requester]" id="requester">
											<option value="">--Choose--</option>
											@foreach ($data['employee'] as $employee)
											@if($data['transactionData'][0]->id_requester_et == $employee->number)
											<option value="{{$employee->number}}" selected>{{$employee->fullname}}</option>
											@else
											<option value="{{$employee->number}}">{{$employee->fullname}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- EMPLOYEE -->
								<tr>
									<th>Employee:</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="asset[employee]" id="employee">
											<option value="">--Choose--</option>
											@foreach ($data['employee'] as $employee)
											@if($data['transactionData'][0]->id_employee_et == $employee->number)
											<option value="{{$employee->number}}" selected>{{$employee->fullname}}</option>
											@else
											<option value="{{$employee->number}}">{{$employee->fullname}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- ASSET -->
								<tr>
									<th>ASSET:</th>
									<td>
										<select class="form-control" name="asset[idAsset]" id="idAsset">
											<option value="">--Choose--</option>
											@foreach ($data['assetDatas'] as $dataAsset)
											@if($data['transactionData'][0]->id_asset_et == $dataAsset->idAsset)
											<option value="{{$dataAsset->idAsset}}" selected>{{$dataAsset->name}}</option>
											@endif
											@endforeach
											
											@foreach ($data['assetDataAvailable'] as $dataAsset)
											<option value="{{$dataAsset->idAsset}}">{{$dataAsset->name}}</option>
											@endforeach
										</select>
									</td>
								</tr>

								<!-- STATUS ASSET -->
								<tr>
									<th>STATUS :</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="asset[transactionStatus]" id="transactionStatus">
											<option value="">--Choose--</option>
											@foreach ($data['assetStatus'] as $assetStatus)
											@if($data['transactionData'][0]->id_status_et == $assetStatus->id)
											<option value="{{ $assetStatus->id }}" selected>{{ $assetStatus->statusName}}</option>
											@else
											<option value="{{ $assetStatus->id }}">{{ $assetStatus->statusName}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- RECEIVING DATE -->
								<tr>
									<th> Receiving Date </th>
									<td><input type="date" id="receivingDate" name="asset[receivingDate]" value="{{ $data['transactionData'][0]->receiving_date_et }}"></td>
								</tr>

								<!-- GIVINGBACK DATE -->
								<tr>
									<th> Returning Date </th>
									<td><input type="date" id="givingBackDate" name="asset[givingBackDate]" value="{{ $data['transactionData'][0]->givingBack_date_et }}"></td>
								</tr>

								<!-- RECEIVING CONDITIONS -->
								<tr>
									<th>Reciving Condition : </th>
									<td>
										<!-- Getting the data from model AssetCondition -->
										<select class="form-control" name="asset[receivingCondition]" id="receivingCondition">
											<option value="">--Choose--</option>
											@foreach ($data['assetCondition'] as $assetCondition)
											@if($data['transactionData'][0]->receiving_condition_et == $assetCondition->id)
											<option value="{{$assetCondition->id}}" selected>{{$assetCondition->conditionDescription}}</option>
											@else
											<option value="{{$assetCondition->id}}">{{$assetCondition->conditionDescription}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>	


								<!-- GIVINGBACK CONDITIONS -->
								<tr>
									<th>Returning Condition : </th>
									<td>
										<!-- Getting the data from model AssetCondition -->
										<select class="form-control" name="asset[givingBackCondition]" id="givingBackCondition">
											<option value="">--Choose--</option>
											@foreach ($data['assetCondition'] as $assetCondition)
											@if($data['transactionData'][0]->givingBack_condition_et == $assetCondition->id)
											<option value="{{$assetCondition->id}}" selected>{{$assetCondition->conditionDescription}}</option>
											@else
											<option value="{{$assetCondition->id}}">{{$assetCondition->conditionDescription}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>		

								<!-- DESCRIPTION -->
								<tr>
									<th>Description :</th>
									<td>
										<textarea class="form-control input-lg" name="asset[transactionDescription]" id="transactionDescription" rows="10">{{ $data['transactionData'][0]->description_et }}</textarea>
									</td>
								</tr>

								<!-- SUBMIT BUTTON -->
								<tr>
									<td colspan="2" align="right">
										<button name="submit" class="btn btn-primary" id="btn-submit-problem">NEXT</button>
									</td>
								</tr>
							</table>
						</form>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<script type="text/javascript">
</script>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#btn-submit-problem').click(function(e){

			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();


			
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('nik', ($("#nik").val()));
			formData.append('email', ($("#email").val()));
			formData.append('idTransaction', ($("#idTransaction").val()));
			formData.append('idAsset', ($("#idAsset").val()));
			formData.append('requester', ($("#requester").val()));
			formData.append('employee', ($("#employee").val()));
			formData.append('receivingDate', ($("#receivingDate").val()));
			formData.append('givingBackDate', ($("#givingBackDate").val()));
			formData.append('receivingCondition', ($("#receivingCondition").val()));
			formData.append('givingBackCondition', ($("#givingBackCondition").val()));
			formData.append('transactionDescription', ($("#transactionDescription").val()));
			formData.append('transactionStatus', ($("#transactionStatus").val()));

			//SEND DATA TO ROUTES
			var url = "{{url('/')}}/transaction/returningAsset";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/transaction";
				}
			});
		});
	});

	function tambahListChild() {
	   var idList = document.getElementById("idList").value;
	   var duplicate;
	   var dataList = idList - 1 + 2;

	   duplicate = 
	   "<div id='srow" + idList + "'>" +
	   "<a href='#' style=\"color:#3399FD;\" onclick='hapusElemen(\"#srow" + idList + "\"); return false;'>Hapus</a>" +
	   "<label class='sr-only'>Daftar Child</label>" +
	   "<select name='daftarChild[]' class='form-control'>" +
	   @foreach ( $data['assetData'] as $assetData )
	   "<option value=''>--Choose--</option>" +
	   "<option value='{{ $assetData->idAsset }}'> {{ $assetData->name }}</option>" +
	   @endforeach
	   "</select>" +
	   "</div>";

	   $("#divListChild").append(duplicate);
	   idList = (idList-1) + 2;
	   document.getElementById("idList").value = idList;
	}

	function hapusElemen(idPaket) {
		$(idPaket).remove();
	}

</script>
<!-- end -->
@endsection
