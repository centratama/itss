@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Data Transaction in {{ $idTransaction }}</center></div>
				<div class="panel-body">
					<legend>Data Transaction</legend>
					<div>
					<!-- INSERT DATA TRANSACTION -->
					<div style="" id="form-problem">
						<p></p>

						<form name="addData" id="addData">
							<!-- TABLE INPUT DATA ASSET -->
							<table class="table table-bordered">
								<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

								<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

								{{ csrf_field()}}

								<input id="idList" value="{{ $i=1 }}" type="hidden" />
								<input id="idTransaction" value="{{ $idTransaction }}" type="hidden" />

								<!-- Requested by -->
								<tr>
									<th>Requested By:</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="asset[requester]" id="requester" disabled>
											<option value="">--Choose--</option>
											@foreach ($data['employee'] as $employee)
											@if($data['transactionData'][0]->nik_requestedby == $employee->number)
											<option value="{{$employee->number}}" selected>{{$employee->fullname}}</option>
											@else
											<option value="{{$employee->number}}">{{$employee->fullname}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- Employee Name -->
								<tr>
									<th>Undertaken By:</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="asset[employee]" id="employee" disabled>
											<option value="">--Choose--</option>
											@foreach ($data['employee'] as $employee)
											@if($data['transactionData'][0]->nik_undertakenby == $employee->number)
											<option value="{{$employee->number}}" selected>{{$employee->fullname}}</option>
											@else
											<option value="{{$employee->number}}">{{$employee->fullname}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- STATUS TRANSAKSI -->
								<tr>
									<th>Transaction Status :</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="asset[assetStatus]" id="transactionStatus1" disabled>
											<option value="">--Choose--</option>
											@foreach ($data['assetStatus'] as $assetStatus)
											@if($data['transactionData'][0]->id_status == $assetStatus->id)
											<option value="{{$assetStatus->id}}" selected>
												{{$assetStatus->statusName}}
											</option>
											@else
											<option value="{{$assetStatus->id}}">
												{{$assetStatus->statusName}}
											</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- STATUS DOCUMENT-->
								<tr>
									<th>Document Status :</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="asset[assetStatus]" id="transactionStatus2" disabled>
											@if($data['transactionData'][0]->status == 0)
											{
												<option value="">--Choose--</option>
												<option value="0" selected >DRAFT</option>
												<option value="1">CLOSE</option>
												<option value="2">CANCEL</option>
											}

											@elseif($data['transactionData'][0]->status == 1)
											{
												<option value="">--Choose--</option>
												<option value="0">DRAFT</option>
												<option value="1"selected>CLOSE</option>
												<option value="2">CANCEL</option>
											}

											@else
											{
												<option value="">--Choose--</option>
												<option value="0">DRAFT</option>
												<option value="1">CLOSE</option>
												<option value="2"selected>CANCEL</option>
											}
											@endif
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- TRANSACTION DATE -->
								<tr>
									<th> Transaction Date </th>
									<td><input type="date" id="transactionDate" name="asset[transactionDate]" value="{{$data['transactionData'][0]->transactiondate}}" disabled></td>
								</tr>	

								<!-- LOCATION -->
								<tr>
									<th>Location : </th>
									<td>
										<!-- Getting the data from model AssetLocation -->
										<select class="form-control" name="asset[assetLocation]" id="assetLocation" disabled>
											<option value="">--Choose--</option>
											@foreach ($data['assetLocation'] as $assetLocation)
											@if($data['transactionData'][0]->id_status == $assetLocation->id)
											<option value="{{$assetLocation->id}}" selected>
												{{$assetLocation->locationName}}
											</option>
											@else
											<option value="{{$assetLocation->id}}">
												{{$assetLocation->locationName}}
											</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>	

								<!-- DESCRIPTION -->
								<tr>
									<th>Description :</th>
									<td>
										<textarea class="form-control input-lg" name="asset[assetDescription]" id="assetDescription" rows="10" disabled>{{$data['transactionData'][0]->description}}</textarea>
									</td>
								</tr>

								<!-- SUBMIT BUTTON -->
								<tr>
									<td colspan="2" align="right">
										<a href="{{ url('/') }}/transaction" class="btn btn-primary"><i class="fa fa-plus"></i>CLOSE</a>
									</td>
								</tr>
							</table>
						</form>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){

		$('#btn-submit-problem').click(function(e){
			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();


			
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('nik', ($("#nik").val()));
			formData.append('email', ($("#email").val()));
			formData.append('idTransaction', ($("#idTransaction").val()));
			formData.append('employee', ($("#employee").val()));
			formData.append('requester', ($("#requester").val()));
			formData.append('transactionDate', ($("#transactionDate").val()));
			formData.append('assetDescription', ($("#assetDescription").val()));
			formData.append('assetLocation', ($("#assetLocation").val()));
			formData.append('transactionStatus1', ($("#transactionStatus1").val()));
			formData.append('transactionStatus2', ($("#transactionStatus2").val()));

			//SEND DATA TO ROUTES
			var url = "{{url('/')}}/transaction/saveEditTransaction1hhgig";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/transaction";
				}
				error: function() { 
					window.location.href = "{{url('/')}}/transaction";
				}
			});
		});
	});

</script>
<!-- end -->
@endsection
