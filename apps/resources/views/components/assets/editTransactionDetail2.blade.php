@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Edit the list of {{ $idTransaction }}</center></div>
				<div class="panel-body">
					<legend></legend>
					<div>
					<!-- INSERT DATA TRANSACTION -->
					<div style="" id="form-problem">
						<p>Please fill in the form below to add new asset</p>

						<form name="addData" id="addData">
							<!-- TABLE INPUT DATA ASSET -->
							<table class="table table-bordered">
								<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

								<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

								<input type="hidden" value="{{ $idTransaction }}" id="idTransaction">

								{{ csrf_field() }}

								<!-- Requested by -->
								<tr>
									<th>Asset :</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" name="asset[requester]" id="idAsset" disabled>
											<option value="">--Choose--</option>
											@foreach ($data['assetDataAvailable'] as $asset)
											@if( $data['transactionData'][0]->id_asset == $asset->idAsset)
												<option value="{{$asset->idAsset}}" selected>{{$asset->name}} / {{ $asset->serial_number }}</option>
											@else
												<option value="{{$asset->idAsset}}">{{$asset->name}} /
												{{ $asset->serial_number }}</option>
											@endif
											
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- Employee Name -->
								<tr>
									<th>Asset Condition :</th>
									<td>
										<!-- Getting the data from model AssetType -->
										<select class="form-control" id="assetCondition" disabled>
											<option value="">--Choose--</option>
											@foreach ($data['assetCondition'] as $condition)
											@if( $data['transactionData'][0]->id_asset_condition == $condition->id)
												<option value="{{$condition->id}}" selected>{{$condition->conditionName}}</option>
											@else
												<option value="{{$condition->id}}">{{$condition->conditionName}}</option>
											@endif
											@endforeach
										</select>
										<!-- Finishing print data from model-->
									</td>
								</tr>

								<!-- DESCRIPTION -->
								<tr>
									<th>Description :</th>
									<td>
										<textarea class="form-control input-lg" name="asset[assetDescription]" id="description" rows="10" disabled>{{$data['transactionData'][0]->description_detail}}</textarea>
									</td>
								</tr>

								<!-- SUBMIT BUTTON -->
								<tr>
									<td colspan="2" align="right">
										<a href="{{ url('/') }}/transaction/detailTransaction/{{$idTransactionHeader}}" class="btn btn-primary"><i class="fa fa-plus"></i> CLOSE</a>
									</td>
								</tr>
							</table>
						</form>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>
<!-- end -->
@endsection
