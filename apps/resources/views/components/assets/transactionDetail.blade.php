@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<center>
			<table border=0 class="table table-bordered table-responsive">
				<tr>
					<td colspan="2" align="center">HEADER TRANSACTION</td>
				</tr>
				<tr>
					<td>ID Transaction</td>
					<td align="center">{{$idTransaction}}</td>
				</tr>
				<tr>
					<td>NIK Requested By</td>
					@foreach($data['employee'] as $employee)
						@if($employee->number == $data['transactionData'][0]->nik_requestedby)
							<td align="center">{{$data['transactionData'][0]->nik_requestedby}} / {{$employee->fullname}}</td>
						@endif
					@endforeach
					
				</tr>
				<tr>
					<td>NIK Undertaken By</td>
						@foreach($data['employee'] as $employee)
						@if($employee->number == $data['transactionData'][0]->nik_undertakenby)
							<td align="center">{{$data['transactionData'][0]->nik_undertakenby}} / {{$employee->fullname}}</td>
						@endif
					@endforeach
				</tr>
				<tr>
					<td>Status Transaction</td>
					<td align="center">
						@foreach($data['assetStatus'] as $status)
						@if($status->id == $data['transactionData'][0]->id_status)
							{{$status->statusName}}
						@endif
						@endforeach
					</td>
				</tr>
				<tr>
					<td>Status Document</td>
					<td align="center">
						@if($data['transactionData'][0]->status == 0)
							<b>DRAFT</b>
						@elseif($data['transactionData'][0]->status == 1)
							<b>CLOSE</b>
						@else
							<b>CANCEL</b>
						@endif
					</td>
				</tr>
				<tr>
					<td>Transaction Date</td>
					<td align="center">
						{{$data['transactionData'][0]->transactiondate}}
					</td>
				</tr>
			</table>
			</center>
		</div>
	</div>

	<div class="col-md-12">
		<span style="float: right;">
			<!-- LINK ADD ASSETS-->
			@if($data['transactionData'][0]->status == 0 )
				<a href="{{ url('/') }}/transaction/createTransactionDetail/{{ $idTransaction }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Asset</a>
			@endif
		</span>
		<br><br>
	</div>
	<input type="hidden" value="{{ $idTransaction }}" id="idTransaction">
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Asset</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive" id="table-transactionDetail">
						<thead>
							<th align="center">Line Number</th>
							<th align="center">ID Asset</th>
							<th align="center">ID Parent</th>
							<th align="center">Asset's Name</th>
							<th align="center">Brand Name</th>
							<th align="center">Asset Condition</th>
							<th align="center">Description</th>
							<th align="center">Tools</th>
						</thead>
					</table>
				</div>
		</div>
	</div>
</div>
@endsection
