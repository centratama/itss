@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List History {{ $idAsset }} </center></div>
			<input type="hidden" value="{{ $idAsset }}" id="idAssetHistory">
				<div class="panel-body">
					<table class="table table-bordered table-responsive" id="table-history">
						<thead>
							<th>#</th>
							<th>NIK</th>
							<th>Employee</th>
							<th>Document Status</th>
							<th>Asset Condition</th>
							<th>Asset Location</th>
							<th>Transaction Date</th>
						</thead>
					</table>
				</div>
		</div>
	</div>
</div>
@endsection
