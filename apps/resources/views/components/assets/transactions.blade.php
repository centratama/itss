@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		<span style="float: right;">
			<!-- LINK ADD ASSETS-->
			<a href="{{ url('/') }}/transaction/createTransaction" class="btn btn-primary"><i class="fa fa-plus"></i> Add Transaction</a>
			<a href="{{ url('/') }}/transaction/createTransaction1" class="btn btn-primary"><i class="fa fa-plus"></i> Add Transaction Baru</a>
		</span>
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Transaction</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive table-striped">
						<thead>
							<th>#</th>
							<th align="center">IFU Number</th>
							<th align="center">NIK</th>
							<th align="center">Employee's Name</th>
							<th align="center">Tools</th>
						</thead>
						<?php $i=1; ?>
						@foreach($data['transactionData'] as $parent)
						<tr>
							<td></td>
							<td align="center">{{$parent->id}}</td>
							<td align="center">{{$parent->id_employee}}</td>
							<td align="center">
								@foreach($data['employee'] as $employee)
								@if($parent->id_employee == $employee->number)
								{{ $employee->fullname }}
								@endif
								@endforeach
							</td>
							<td align="center">
								<a href="{{ url('/transaction/transactionReport/'.$parent->id) }}"><i class="fa fa-print"></i></a>
								<a href="{{ url('/transaction/addTransaction/'.$parent->id.'/'.$parent->id_employee) }}">
									<i class="fa fa-plus-circle"></i>
								</a>
								<button style="font-size:9px" id="{{ $i }}" class="show{{ $i }}">SHOW<i class="fa fa-eye"></i></button>
								<button style="font-size:9px" id="{{ $i }}" class="hiden{{ $i }}">HIDEN<i class="fa fa-eye-slash"></i></button>
							</td>
						</tr>

						<div style="display:none">
							<tr class="ok{{ $i }}" style="display:none">
								<td colspan="4" align="center"> ASSET </td>
								<td colspan="4" align="center"> EMBEDED ASSET</td>
							</tr>
							@foreach($data['assetEmployee'] as $employee)
							@if($employee->idTransaction_et == $parent->id AND $employee->id_parent_et == null)
								@foreach($data['asset'] as $check)
								
									@if($employee->id_asset_et == $check->idAsset)
									
									<tr class="ok{{ $i }}" style="display:none">
										<td colspan="4" align="center">
											{{ $check->name}} <br> {{ $check->idAsset}}<br>
											<a href="{{ url('/transaction/editTransaction2/'.$employee->id_et) }}">
												<i class="fa fa-close" style="font-size:15px;color:red"></i>
											</a>

										</td>
										<td colspan="4" align="center">
											@foreach($data['assetEmployee'] as $employee2)
												@if($employee2->id_parent_et == $employee->id_asset_et)
													@foreach($data['asset'] as $check3)
													@if($employee2->id_asset_et == $check3->idAsset)

													{{$check3->name}} / 
													{{$check3->idAsset}}
													<br>
													@endif

													@endforeach

												@endif
											@endforeach
										</td>
									</tr>
									@endif

								@endforeach
							@endif
							@endforeach
							<tr class="ok{{ $i }}" style="display:none">
									<td colspan="10" align="center"></td>
							</tr>
						</div>

						<?php $i++; ?>
						@endforeach
					</table>
				</div>
		</div>
	</div>
</div>

<script>
$("button").click(function() {
    //alert(this.id);// or alert($(this).attr('id'));


    var id = this.id;
    var classs = this.className;
    var lable = $("." + classs).text().trim();
    //alert(classs);

    /*var lable = $(this).text().trim();
    
    alert(classs);
    var class = $(".show" + id).text().trim();*/
    
    if(lable == "HIDEN") {
    	$("tr.ok"+ id).hide();
    }
    else{
    	$("tr.ok"+ id).show();
    }
});
</script>	
@endsection
