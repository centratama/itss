@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		<span style="float: right;">
			<!-- LINK ADD ASSETS-->
			<a href="{{ url('/') }}/transaction/createTransaction1" class="btn btn-primary"><i class="fa fa-plus"></i> Add Transaction</a>
		</span>
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Transaction</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive table-striped" id="table-transaction">
						<thead>
							<th align="center">ID</th>
							<th align="center">NIK Requested by</th>
							<th align="center">NIK Undertaking by</th>
							<th align="center">Transaction Status</th>
							<th align="center">Document Status</th>
							<th align="center">Location</th>
							<th align="center">Tansaction Date</th>
							<th align="center">Description</th>
							<th align="center">Tools</th>
						</thead>
					</table>
				</div>
		</div>
	</div>
</div>
@endsection
