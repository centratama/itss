@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		<span style="float: right;">
			<!-- LINK ADD ASSETS-->
			<a href="{{ url('/') }}/transaction/createTransaction" class="btn btn-primary"><i class="fa fa-plus"></i> Add Transaction</a>
		</span>
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Transaction</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive">
						<thead>
							<th>#</th>
							<th>NIK</th>
							<th>Employee's Name</th>
							<th>Assets's Name</th>
							<th>Receiving Date</th>
							<th>Returning Date</th>
							<th>Status</th>
							<th>Tools</th>
						</thead>
						<?php $i=1; ?>
						@foreach($data['transactionData'] as $parent)
						<tr>
							<td></td>
							<td align="center">{{$parent->id_employee}}</td>
							<td align="center">
								@foreach($data['employee'] as $employee)
								@if($parent->id_employee == $employee->number)
								{{ $employee->fullname }}
								@endif
								@endforeach
							</td>
							<td align="center">
								@foreach($data['asset'] as $asset)
								@if($parent->id_asset == $asset->idAsset)
								{{ $asset->name }}
								@endif
								@endforeach
								
							</td>
							
							<td align="center">
								{{ $parent->receiving_date }}
							</td>
							<td align="center">
								@if( $parent->givingBack_date == '1970-01-01')
								-
								@else
								{{ $parent->givingBack_date }}
								@endif
							</td>
							<td align="center">
								@foreach($data['assetStatus'] as $assetStatus)
								@if($parent->id_status == $assetStatus->id)
								{{ $assetStatus->statusName }}
								@endif
								@endforeach
							</td>
							<td>
								<button style="font-size:15px" id="{{ $i }}" class="show{{ $i }}">SHOW<i class="fa fa-eye"></i></button>
								<button style="font-size:15px" id="{{ $i }}" class="hiden{{ $i }}">HIDEN<i class="fa fa-eye-slash"></i></button>
								<a href="{{ url('/transaction/transactionReport/'.$parent->id) }}"><button>PRINT</button></a>
								<a href="{{ url('/transaction/editTransaction/'.$parent->id) }}"><button>EDIT</button></a>
								<a href="{{ url('/transaction/addTransaction/'.$parent->id.'/'.$parent->id_employee) }}"><button>ADD</button></a>
							</td>
						</tr>

						<div style="display:none">
							@foreach($data['assetEmployee'] as $employee)
							@if($employee->idTransaction_et == $parent->id)
								@foreach($data['asset'] as $check)
								
									@if($employee->id_asset_et == $check->idAsset)
									<tr class="ok{{ $i }}" style="display:none">
										<td colspan="5" align="center"> ASSET </td>
										<td colspan="5" align="center"> EMBEDED ASSET</td>
									</tr>
									<tr class="ok{{ $i }}" style="display:none">
										<td colspan="5" align="center">
											{{ $check->name}} <br> {{ $check->idAsset}}
										</td>
										<td colspan="5" align="center"></td>
									</tr>
										
											<tr class="ok{{ $i }}" style="display:none">
												<td colspan="10" align="center">Asset</td>
											</tr>

											<tr class="ok{{ $i }}" style="display:none">
												<td></td>
												<td></td>
												<td align="center">Asset's Name</td>
												<td colspan="2" align="center">Category</td>
												<td colspan="3" align="center">Descriptioin</td>
											</tr>
											<tr  class="ok{{ $i }}" style="display:none">
												<td></td>
												<td></td>
												<td align="center">{{ $check->name }}</td>
												<td colspan="2" align="center">
													@foreach($data['assetType'] as $assetType)
													@if($assetType->id == $check->id_types)
													{{ $assetType->typeName }}
													@endif
													@endforeach
												</td>
												<td colspan="3" align="center">{{ $check->descriptions }}</td>
											</tr>

											<tr class="ok{{ $i }}" style="display:none">
												<td colspan="10" align="center"></td>
											</tr>
									@endif

								@endforeach
							@endif
							
							<tr class="ok{{ $i }}" style="display:none">
								<td colspan="10" align="center">Embeded Asset</td>
							</tr>

								<tr class="ok{{ $i }}" style="display:none">
									<td></td>
									<td></td>
									<td align="center">Asset's Name</td>
									<td colspan="2" align="center">Category</td>
									<td colspan="3" align="center">Descriptioin</td>
								</tr>

							@foreach($data['assetEmployee'] as $check2)
							@if($parent->id_asset == $check2->id_parent)
								
								<tr  class="ok{{ $i }}" style="display:none">
									
		
									    <td></td>
									    <td></td>
									    <td align="center">{{ $check->name }}</td>
										<td colspan="2" align="center">
											@foreach($data['assetType'] as $assetType)
											@if($assetType->id == $check->id_types)
											{{ $assetType->typeName }}
											@endif
											@endforeach
										</td>
										<td colspan="3" align="center">{{ $check->descriptions }}</td>
									
								</tr>
							@endif
							@endforeach
							@endforeach
							<tr class="ok{{ $i }}" style="display:none">
									<td colspan="10" align="center"></td>
							</tr>
						</div>

						<?php $i++; ?>
						@endforeach
					</table>
				</div>
		</div>
	</div>
</div>

<script>
$("button").click(function() {
    //alert(this.id);// or alert($(this).attr('id'));


    var id = this.id;
    var classs = this.className;
    var lable = $("." + classs).text().trim();
    //alert(classs);

    /*var lable = $(this).text().trim();
    
    alert(classs);
    var class = $(".show" + id).text().trim();*/
    
    if(lable == "HIDEN") {
    	$("tr.ok"+ id).hide();
    }
    else{
    	$("tr.ok"+ id).show();
    }
});
</script>	
@endsection
