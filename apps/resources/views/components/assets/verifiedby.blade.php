@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center> Who Will Verify The Transaction Report ?</center></div>
				<div class="panel-body">
					<legend></legend>
					<div>
					<!-- INSERT DATA -->
					<div style="" id="form-problem">
						<p>Please fill this form</p>
							<!-- TABLE INPUT DATA ASSET -->
							<table class="table table-bordered">
								<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

								<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

								{{ csrf_field()}}

								<!-- ASSET -->
								<tr>
									<th>Verified By :</th>
									<td>
										<select class="form-control" name="asset[verifiedby]" id="verifiedby">
											<option value="">--Choose--</option>
											@foreach ($data['employee'] as $employee)
											@if($data['allSign'][0]->verifiedby == $employee->number)
											<option value="{{$employee->number}}" selected>{{$employee->fullname}}</option>
											@else
											<option value="{{$employee->number}}">{{$employee->fullname}}</option>
											@endif
											@endforeach
										</select>
									</td>
								</tr>

								<tr>
									<th>Acknowledged By :</th>
									<td>
										<select class="form-control" name="asset[know]" id="know">
											<option value="">--Choose--</option>
											@foreach ($data['employee'] as $employee)
											@if($data['allSign'][0]->acknowledgedby == $employee->number)
											<option value="{{$employee->number}}" selected>{{$employee->fullname}}</option>
											@else
											<option value="{{$employee->number}}">{{$employee->fullname}}</option>
											@endif
											@endforeach
										</select>
									</td>
								</tr>


						

								<!-- SUBMIT BUTTON -->
								<tr>
									<td colspan="2" align="right">
										<input type="reset" name="reset" value="Reset" class="btn btn-danger">
										<button name="submit" class="btn btn-primary" id="btn-submit-problem">SAVE</button>
									</td>
								</tr>
							</table>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<script type="text/javascript">
</script>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#btn-submit-problem').click(function(e){

			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();


			
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('verifiedby', ($("#verifiedby").val()));
			formData.append('acknowledgedby', ($("#know").val()));
			//SEND DATA TO ROUTES
			var url = "{{url('/')}}/asset/signReport";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/transaction";
				}
			});
		});
	});

</script>
<!-- end -->
@endsection
