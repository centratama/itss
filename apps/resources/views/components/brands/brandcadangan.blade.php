@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		<span style="float: right;">
			<!-- LINK ADD Brand -->
			<a href="{{ url('/') }}/asset/brands/createBrand" class="btn btn-primary"><i class="fa fa-plus"></i> Add Brands</a>
		</span>
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Brands</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive table-striped" id="table-tickets">
						<thead>
							<th>#</th>
							<th>Brand ID</th>
							<th>Brand's Names</th>
							<th>Description</th>
							<th>Tools</th>
						</thead>
						@foreach($data['assetBrand'] as $data)
						<tr>
							<td></td>
							<td>{{$data->id}}</td>
							<td>{{$data->brandName}}</td>
							<td>{{$data->brandDescription}}</td>
							<td>
								<a href="{{ url('/asset/brands/editBrand/'.$data->id) }}">EDIT</a>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
		</div>
	</div>
</div>	
@endsection
