@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Edit Asset's Brand</center></div>
				<div class="panel-body">
					<legend>Edit Asset's Brand</legend>
					<div>
					<!-- EDIT DATA -->
					<div style="" id="form-problem">
						<p>Please fill in the form below to edit asset's Brand</p>

						<!-- TABLE INPUT DATA ASSET -->
						<table class="table table-bordered">
							<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

							<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

							{{ csrf_field()}}

							<input class="form-control" type="text" name="brand[idBrand" id="idBrand" value="{{ $data['assetBrand'][0]->id }}">
									
							<!-- BRAND'S NAME -->
							<tr>
								<th>Brand's Name :</th>
								<td>
									<input class="form-control" type="text" name="brand[brandName]" id="brandName" value="{{ $data['assetBrand'][0]->brandName }}">
								</td>
							</tr>

							<!-- BRAND'S NAME -->
							<tr>
								<th>Brand's Description :</th>
								<td>
									<input class="form-control" type="text" name="brand[brandDescription]" id="brandDescription" value="{{ $data['assetBrand'][0]->brandDescription }}">
								</td>
							</tr>

							<!-- SUBMIT BUTTON -->
							<tr>
								<td colspan="2" align="right">
									<input type="reset" name="reset" value="Reset" class="btn btn-danger">
									<button name="submit" class="btn btn-primary" id="btn-submit-problem">Edit Brand</button>
								</td>
							</tr>
						</table>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<script type="text/javascript">
</script>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#btn-submit-problem').click(function(e){

			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('brandName', ($("#brandName").val()));
			formData.append('brandDescription', ($("#brandDescription").val()));
			
			//SEND DATA TO ROUTES
			var url = "{{url('/asset/brands/saveEditBrand/'. $idBrand) }}";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/asset/brands";
				}
			});
		});
	});
</script>
<!-- end -->
@endsection
