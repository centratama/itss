@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		{{-- <span style="float: right;">
			<a href="{{ url('/') }}/employees/create" class="btn btn-primary"><i class="fa fa-plus"></i> Add Employee</a>
		</span> --}}
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Employees</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive table-striped" id="table-employees">
						<thead>
							<th>#</th>
							<th>NIK</th>
							<th>Name</th>
							<th>Job</th>
							<th>Organization</th>
							<th>E-Mail</th>
							{{-- <th>Tools</th> --}}
						</thead>
					</table>
				</div>
		</div>
	</div>
</div>	
@endsection