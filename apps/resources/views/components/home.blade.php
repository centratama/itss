@extends('layouts.apps')
@section('content')
<?php
	$this->rowLabels = $data['officer'];
	$this->colCountProgress = $data['count']['progress'];
	$this->colCountClosed = $data['count']['closed'];

	foreach ($this->rowLabels as $rowKey => $rowVal) {
		foreach ($this->colCountProgress as $colProg => $valProg) {
			$this->cellValProg[$rowVal->number] = 0;
		}
		foreach ($this->colCountClosed as $colClos => $valClos) {
			$this->cellValClos[$rowVal->number] = 0;
		}
	}
	
	foreach ($data['count']['progress'] as $key => $countProg) {
		$row_id = $countProg->assigned_to;
		$this->cellValProg[$row_id] = $countProg->count;
	}
	foreach ($data['count']['closed'] as $key => $countClos) {
		$row_id = $countClos->assigned_to;
		$this->cellValClos[$row_id] = $countClos->count;
	}

?>
<div id="container">
	
</div>

<script type="text/javascript">
	var chart = new Highcharts.chart('container', {
		chart: {
			type: 'column',
		},
		title: {
			text: 'Summary Report'
		},
		xAxis: {
			categories: [
				@foreach ($data['officer'] as $data)
					"{{$data->fullname}}",
				@endforeach
			],
			crosshair: true
		},
		yAxis: {
			min: 0,
			allowDecimals: false,
			tickInterval: 1,
			title: {
                text: 'Count'

            },
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        	pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        	footerFormat: '</table>',
        	shared: true,
        	useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: [
			{
				name: "On Progress",
				data: [
					<?php
						foreach ($this->rowLabels as $row => $officer) {
							$row_id = $officer->number;
					?>
						{{$this->cellValProg[$row_id]}},
					<?php
						}
					?>
				]
			},{
				name: "Closed",
				data: [
					<?php
						foreach ($this->rowLabels as $row => $officer) {
							$row_id = $officer->number;
					?>
						{{$this->cellValClos[$row_id]}},
					<?php
						}
					?>
				]
			}		
    	]
	});
</script>
@endsection

