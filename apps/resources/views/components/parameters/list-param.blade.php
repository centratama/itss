@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		<span style="float: right;">
			<a href="{{ url('/') }}/parameter/create" class="btn btn-primary"><i class="fa fa-plus"></i> Add Parameter</a>
		</span>
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Parameter</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive table-striped" id="table-parameters">
						<thead>
							<th>#</th>
							<th>Type</th>
							<th>Value</th>
							<th>Description</th>
							<th>Tools</th>
						</thead>
					</table>
				</div>
		</div>
	</div>
</div>	
@endsection
