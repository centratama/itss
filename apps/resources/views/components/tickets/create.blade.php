@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Create a New Ticket</center></div>
				<div class="panel-body">
					<legend>Open a New Ticket</legend>
					<div><p>Please choose the type of ticket that you want to create.</p></div>
					<table class="table" width="80%">
						<tr>
							<th>Ticket Type:</th>
							<td>
								<select class="form-control" name="ticket[ticket_type]" id="ticket_type">
									<option value="">--Choose--</option>
									@foreach ($data['ticket_type'] as $ticketType)
									<option value="{{$ticketType->lookup_value}}">{{$ticketType->lookup_desc}}</option>
									@endforeach
								</select>
							</td>
						</tr>
					</table>
					<form enctype="multipart/form-data">
						<div id="form"></div>
					</form>
				</div>
		</div>
	</div>
</div>

<div id="form-un-use"></div>
<!--  -->
<div style="display: none;" id="form-problem"><p>Please fill in the form below to open a new ticket.</p>
	<table class="table table-bordered">
			<input type="hidden" name="ticket[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">
			<input type="hidden" name="ticket[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">
			{{ csrf_field()}}
		@if(Session::get('logged_in')['user_role'] == "USER")		
		<tr>
			<th>Full Name:</th>
			<td><input class="form-control" type="text" name="ticket[fullname]" id="fullname" value="{{Session::get('logged_in')['namalengkap']}}" readonly="readonly">
			</td>
		</tr>
		<tr>
			<th>E-Mail Address:</th>
			<td><input class="form-control" type="email" name="ticket[email]" id="email" value="{{Session::get('logged_in')['email']}}" readonly="readonly"></td>
		</tr>
		@endif
		<tr>
			<th>Case Type:</th>
			<td>
				<select class="form-control" name="ticket[problem_type]" id="problem_type">
					<option value="">--Choose--</option>
					@foreach ($data['case_type'] as $caseType)
					<option value="{{$caseType->lookup_value}}">{{$caseType->lookup_desc}}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr>
			<th>Subject:</th>
			<td><input class="form-control" type="text" name="ticket[subject]" id="subject"></td>
		</tr>
		<tr>
			<th>Message:</th>
			<td>
				<textarea class="form-control input-lg" name="ticket[message]" id="message" rows="10"></textarea>
			</td>
		</tr>
		@if(in_array(Session::get('logged_in')['user_role'], array("SUPPORT", "ADMIN")))
		<tr>
			<th>Informer:</th>
			<td>
				<select class="form-control selectpicker" data-live-search="true" name="ticket[informer]" id="informer">
					<option value="">--Choose--</option>
					@foreach ($data['employee'] as $employee)
					<option value="{{$employee->number}}">{{$employee->fullname}}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr id="ticket-level">
			<th>Level:</th>
			<td>
				<select class="form-control" name="ticket[level]" id="level">
					<option value="">--Choose--</option>
					@foreach ($data['level'] as $level)
					<option value="{{$level->lookup_value}}">{{$level->lookup_desc}}</option>
					@endforeach
				</select>
			</td>
		</tr>
		@endif
		<tr>
			<th>Attachment:</th>
			<td>
				<input type="file" name="ticket[attach]" id="attach" onchange="return fileValidation()">
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				<input type="reset" name="reset" value="Reset" class="btn btn-danger">
				<button name="submit" class="btn btn-primary" id="btn-submit-problem" disabled="disabled">Create Ticket</button>
			</td>
		</tr>
	</table>
</div>
<!--  -->

<!--  -->
<div style="display: none;" id="form-request"><p>Please fill in the form below to open a new ticket.</p>
	<table class="table table-bordered">
		
		<tr>
			<th>Request Type:</th>
			<td>
				<select class="form-control" name="ticket[request_type]" id="request_type">
					<option value="">--Choose--</option>
				</select>
			</td>
		</tr>				
		<tr>
			<th>Full Name:</th>
			<td><input class="form-control" type="text" name="ticket[fullname]" id="fullname" value="{{Session::get('logged_in')['namalengkap']}}" readonly="readonly">
			<input type="hidden" name="ticket[nik]" value="{{Session::get('logged_in')['nik']}}">
			{{ csrf_field()}}</td>
		</tr>
		<tr>
			<th>E-Mail Address:</th>
			<td><input class="form-control" type="email" name="ticket[email]" id="email" value="{{Session::get('logged_in')['email']}}" readonly="readonly"></td>
		</tr>
		<tr>
			<th>Problem Type:</th>
			<td>
				<select class="form-control" name="ticket[problem_type]" id="problem_type">
					<option value="">--Choose--</option>
				</select>
			</td>
		</tr>
		<tr>
			<th>Subject:</th>
			<td><input class="form-control" type="text" name="ticket[subject]" id="subject"></td>
		</tr>
		<tr>
			<th>Message:</th>
			<td>
				<textarea class="form-control input-lg" name="ticket[message]" id="message" rows="10"></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				<input type="reset" name="reset" value="Reset" class="btn btn-danger">
				<input type="submit" name="submit" value="Create Ticket" class="btn btn-primary" id="btn-submit">
			</td>
		</tr>
	</table>
</div>
<!--  -->

<script type="text/javascript">
</script>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#btn-submit-problem').click(function(e){
		// $("#form-problem").submit(function(){
			e.preventDefault();
			
			var formData = new FormData();
			formData.append('file', ($("#attach")[0].files[0]));
			formData.append('nik', ($("#nik").val()));
			formData.append('email', ($("#email").val()));
			formData.append('problem_type', ($("#problem_type").val()));
			formData.append('subject', ($("#subject").val()));
			formData.append('message', ($("#message").val()));
			formData.append('ticket_type', 0);
			formData.append('informer', ($("#informer").val()));
			formData.append('level', ($("#level").val()));

			var url = "{{url('/')}}/ticket/save";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/ticket/view/"+data['ticket_id'];
				}
			});
		});

		var problem_type = $("#problem_type");
		var subject = $("#subject");
		var message = $("#message");

		problem_type.change(function(){
			if ($(this).val() != "") {
				subject.keyup(function(){
					if ($(this).val() != "") {
						message.keyup(function(){
							if ($(this).val() != "") {
								document.getElementById('btn-submit-problem').removeAttribute('disabled');
							} else {
								document.getElementById('btn-submit-problem').setAttribute('disabled', 'disabled');
							}
						});
					}
				});
			}
		});

		$('#ticket_type').on('change', function () {
			if (this.value == "") {
				$('#form-problem').hide();
				$('#form-request').hide();
				$('#form-problem').appendTo('#form-un-use');
				$('#form-request').appendTo('#form-un-use');
			} else if (this.value == 0) {
				$('#form-problem').appendTo('#form');
				$('#form-problem').show();
				$('#form-request').hide();
				$('#form-request').appendTo('#form-un-use');
			} else {
				$('#form-request').appendTo('#form');
				$('#form-request').show();
				$('#form-problem').hide();
				$('#form-problem').appendTo('#form-un-use');
			}
		});

		var role = $("#user_role").val();
		if (role == "ADMIN") {
			document.getElementById("ticket-level").style.display = "none";
		}
	});

	function fileValidation(){
    	var fileInput = document.getElementById('attach');
    	var filePath = fileInput.value;
    	var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif|\.pdf)$/i;
    	if(!allowedExtensions.exec(filePath)){
        	alert('Please upload file having extensions .jpeg/.jpg/.png/.gif/.pdf only.');
        	fileInput.value = '';
        	return false;
    	} // else {
     //    	//Image preview
     //    	if (fileInput.files && fileInput.files[0]) {
     //        	var reader = new FileReader();
     //        	reader.onload = function(e) {
     //            	document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
     //        	};
     //        	reader.readAsDataURL(fileInput.files[0]);
     //    	}
    	// }
	}
</script>
<!-- end -->
@endsection
