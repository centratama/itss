@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		<span style="float: right;">
			<a href="{{ url('/') }}/ticket/create" class="btn btn-primary"><i class="fa fa-plus"></i> Add Ticket</a>
		</span>
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Ticket</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive table-striped" id="table-tickets">
						<thead>
							<th>#</th>
							<th>Ticket ID</th>
							<th>Case Type</th>
							<th>Subject</th>
							<th>Status</th>
							<th>Informer</th>
							<th>Level</th>
							<th>Created At</th>
							<th>Tools</th>
						</thead>
					</table>
				</div>
		</div>
	</div>
</div>	
@endsection
