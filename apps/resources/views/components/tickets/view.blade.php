@extends('layouts.apps')
@section('content')

<?php
	$case_type = $data['ticket'][0]->case_type;
	$no = 0;
	// $planned_finish = date('d M, Y',strtotime(substr($data['ticket'][0]->planned_finish,0,10)));
	if ($data['ticket'][0]->is_external == 1) {
		if ($data['trans_external'][0]->estimation_type == 1)
		{
			$estimation = $data['trans_external'][0]->estimation_desc." Days";
		} else {
			$estimation = $data['trans_external'][0]->estimation_desc." Hours";
		}

		if ($data['trans_external'][0]->status == 0) {
			$status_ext = "<span class='label label-warning' style='display:block;'>On Progress</span>";
		} else {
			$status_ext = "<span class='label label-success' style='display:block;'>Done</span>";
		}
	}

	if ($data['ticket'][0]->status == 0) {
		$labelStatus = "<sup><span class='label label-danger'>Open</span></sup>";
	} elseif ($data['ticket'][0]->status == 1) {
		$labelStatus = "<sup><span class='label label-warning'>On Progress by Officer.</span></sup>";
	} else {
		$labelStatus = "<sup><span class='label label-success'>Ticket Closed.</span></sup>";
	}
	// dd($data['ticket']);
?>

<div class="row">
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>View Ticket</center></div>
				<div class="panel-body">
				@if (in_array(Session::get('logged_in')['user_role'], array('ADMIN', 'SUPPORT')))
					@if ($data['ticket'][0]->is_external == 0)
						<button class="btn btn-success" {{$isDisabled}} id="btn-update" data-toggle="modal" data-target="#modals-form">Update Ticket</button>
					@endif
				@endif

				<button class="btn btn-info" data-toggle="modal" data-target="#table-history">History Ticket</button>
				@if (in_array(Session::get('logged_in')['user_role'], array('ADMIN', 'SUPPORT')))
					@if ($data['ticket'][0]->description == NULL)
					<button class="btn btn-danger" {{$isDisabled}} data-toggle="modal" data-target="#modals-form" id="btn-close">Close Ticket</button>
					@endif
				@endif

				<span style="float: right;">
					<button class="btn btn-warning" id="btn-takeIt" style="visibility: hidden;">Take it</button>
				</span>

				<input type="hidden" name="role" id="role" value="{{Session::get('logged_in')['user_role']}}">
				<input type="hidden" name="isSelf" id="isSelf" value="{{Session::get('logged_in')['nik']}}">
				<input type="hidden" name="ticket-status" id="ticket-status" value="{{$data['ticket'][0]->status}}">
				<center>
					<legend> Ticket &raquo; <b>{{$data['ticket'][0]->id}}</b> {!! $labelStatus !!}</legend>
				</center>
					<table class="table table-striped" width="50%">
						<tr>
							<th>Case Type:</th>
							<td>{{$data['case_type'][$case_type]->lookup_desc}}</td>
							<th>PIC:</th>
							<td>{{$data['ticket'][0]->informer}}</td>
						</tr>
						<tr>
							<th>Subject:</th>
							<td>{{$data['ticket'][0]->subject}}</td>
							<th>Message:</th>
							<td width="30%">{!! $data['ticket'][0]->message !!}</td>
						</tr>
						<tr>
							<th>Level Ticket:</th>
							<td>{!! $levelLabel !!}</td>
							<th>Assigned to:</th>
							<td>{!! $getPerson !!}</td>
						</tr>
						<tr>
							<th>Last Updated:</th>
							<td colspan="3">{{$data['ticket'][0]->updated_at}}</td>
						</tr>
					</table>
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a data-toggle="tab" href="#attach">Attachments</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade in active" id="attach">{!! $iframe !!}</div>
					</div>
				</div>
		</div>
	</div>
</div>

<!-- Modals assigned ticket by Admin -->
<div class="modal fade" id="modals-form" role="dialog" style="display: none;" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<form id="form-update">
			<div class="modal-content">
				<div class="modal-header">
                	<button type="button" class="close" data-dismiss="modal">&times;</button>
                	<h4 class="modal-title">Update Ticket</h4>
                	<input type="hidden" name="ticket_id" id="ticket_id" value="{{$data['ticket'][0]->id}}">
                	<input type="hidden" name="type" id="type">
            	</div>
            	<!-- Assigned to -->
            	<div class="modal-body" id="assigned">
            		<div class="row">
            			<div class="col-md-6">
            				<div class="form-group">
            					<label for="passwd">Level</label>
            					<select name="level" id="level" class="form form-control" required>
            						<option value="">--Choose--</option>
									@foreach($data['ticket_level'] as $level)
										<option value="{{$level->lookup_value}}">{{$level->lookup_desc}}</option>
									@endforeach		
            					</select>
            				</div>
            			</div>
            			<div class="col-md-6">
            				<div class="form-group">
            					<label for="passwd">Assigned to</label>
            					<select name="assigned_to" id="assigned_to" class="form form-control" required>
            						<option value="">--Choose--</option>
            						@foreach($data['admin'] as $admin)
										<option value="{{$admin->number}}">{{$admin->fullname}}</option>
									@endforeach
            					</select>
            				</div>
            			</div>
            		</div>
            	</div>

            	<!-- Handover -->
            	<div class="modal-body" id="handover">
            		<div class="row">
            			<div class="col-md-12">
            				<div class="form-group">
            					<label for="other_crew">Handover to</label>
            					<select name="other_crew" id="other_crew" class="form form-control" onchange="isExternalOrInternal(this)">
            						<option value="">--Choose--</option>
            						<optgroup label="Internal">
									@foreach($data['other_crew'] as $other)
										<option value="{{$other->number}}">{{$other->fullname}}</option>
									@endforeach
									</optgroup>
									<optgroup label="External">
										<option value="13">Pokoknya External dah</option>
									</optgroup>		
            					</select>
            				</div>
            			</div>
            			<div class="col-md-12">
            				<div class="form-group">
            					<label for="reason">Description</label>
            					<textarea name="reason" class="form form-control" id="reason" style="resize: none;" rows="10" required></textarea>
            				</div>
            			</div>
            		</div>
            		<div class="row" id="addon-external" style="display: none;">
            			<div class="col-md-12">
            				<div class="form-group">
            					<label for="external_side">External Side Name</label>
            					<input type="text" name="external_side" id="external_side" class="form form-control">
            				</div>
            			</div>
            			<div class="col-md-6">
            				<div class="form-group">
            					<label for="estimation_type">Estimation Type</label>
            					<select name="estimation_type" id="estimation_type" class="form form-control">
            						<option value="">--Choose--</option>
            						@foreach($data['estimation_type'] as $type)
            							<option value="{{$type->lookup_value}}">{{$type->lookup_desc}}</option>
            						@endforeach
            					</select>
            				</div>
            			</div>
            			<div class="col-md-6">
            				<div class="form-group">
            					<label for="estimation">Estimation(only number)</label>
            					<input type="text" name="estimation" id="estimation" class="form form-control" onkeypress="return isNumberKey(event)">
            				</div>
            			</div>
            		</div>
            	</div>

            	<!-- Close ticket -->
            	<div class="modal-body" id="close-ticket">
            		<div class="row">
            			<div class="col-md-12" id="level-close">
            				<div class="form-group">
            					<label for="passwd">Level</label>
            					<select name="level" id="level-close-by-admin" class="form form-control" required>
            						<option value="">--Choose--</option>
									@foreach($data['ticket_level'] as $level)
										<option value="{{$level->lookup_value}}">{{$level->lookup_desc}}</option>
									@endforeach		
            					</select>
            				</div>
            			</div>
            			<div class="col-md-12">
            				<div class="form-group">
            					<label for="message">Message</label>
            					<textarea name="message" class="form form-control" id="message" style="resize: none;" rows="10" required></textarea>
            				</div>
            			</div>
            		</div>
            	</div>

            	<div class="modal-footer">
					<input type="reset" name="reset" value="Reset" class="btn btn-danger">
					<!-- <button class=" btn btn-info" id="btn-submit">Kirim</button> -->
					<input type="submit" name="send" value="Submit" class="btn btn-info" id="btn-submit-update-ticket">
            	</div>
			</div>
		</form>
	</div>
</div>

<!-- Ajax -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#btn-submit-update-ticket').click(function(e){
			e.preventDefault();

			var formData = new FormData();
			var id = $("#ticket_id").val();
			var splitId = id.split("#");

			var role = $("#role").val();
			var type = $("#type").val();
			var level = $("#level").val();
			if (role == "ADMIN") {
				if(type == "close-ticket") {
					level = $("#level-close-by-admin").val();
				}
				// level = $("#level-close-by-admin").val();
				// level = $("#level").val();
			}

			formData.append('level', level);
			formData.append('assigned_to', ($("#assigned_to").val()));
			formData.append('other_crew', ($("#other_crew").val()));
			formData.append('reason', ($("#reason").val()));
			formData.append('type', ($("#type").val()));
			formData.append('id', id);
			formData.append('message', ($("#message").val()));
			formData.append('external_side', ($("#external_side").val()));
			formData.append('estimation', ($("#estimation").val()));
			formData.append('estimation_type', ($("#estimation_type").val()));
			formData.append('trans_id', {{$data['ticket'][0]->ticket_trans_id}});

			var url = "{{url('/')}}/ticket/update/"+splitId[1];

			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});

			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/ticket/view/"+splitId[1];
					// window.location.href = "{{url('/')}}/ticket";
				}
			});

		});

		var role = $("#role").val();
		var isSelf = $("#isSelf").val();

		$('#btn-close').click(function(){

			$('#handover').hide();
			$('#assigned').hide();
			$('#close-ticket').show();
			document.getElementById("type").setAttribute("value", "close-ticket");
			document.getElementsByClassName("modal-title")[0].innerHTML = "Close Ticket \u00bb {{$data['ticket'][0]->id}}";
			if (role != "ADMIN") {
				$('#level-close').hide();
			}
		});

		$('#btn-update').click(function(){
			document.getElementsByClassName("modal-title")[0].innerHTML = "Update Ticket";
			if (role == "SUPPORT") {
				$('#handover').show();
				$('#assigned').hide();
				$('#close-ticket').hide();
				document.getElementById("type").setAttribute("value", "handover");
			} else {
				$('#handover').hide();
				$('#assigned').show();
				$('#close-ticket').hide();
				document.getElementById("type").setAttribute("value", "assigned");
			}
		});

		var ticket_status = $("#ticket-status").val();

		if (ticket_status == 2) {
			// $('#btn-close').hide();
			$('#btn-update').hide();
			// document.getElementById('btn-close').disabled = true;
			document.getElementById('btn-update').disabled = true;
		}

		$("#btn-takeIt").click(function(e){
			e.preventDefault();
			swal({
				title: "Are you sure?",
				text: "After you take this ticket, you are not able to handover this ticket to the other.",
				type: "warning",
				showCancelButton: true,
            	confirmButtonColor: "#DD6B55",
            	confirmButtonText: "Yes, pretty sure!",
            	cancelButtonText: "No, please cancelled it!",
            	closeOnConfirm: false,
            	closeOnCancel: false,
            	closeOnClickOutside: false,
			},
			function (isConfirm) {
				if (isConfirm) {
					swal({
						title: "Excellence",
						text: "Be faster to complete this issue.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					}, function(){
						var ticket_id = "{{$data['ticket'][0]->id}}";
						var splitId = ticket_id.split("#");
						$.ajaxSetup({
    						headers:
    						{
        						'X-CSRF-Token': $('input[name="_token"]').val()
    						}
						});

						$.ajax(
							{
								type: "post",
								url: "{{url('/')}}/ticket/update/"+splitId[1],
								data: "id="+splitId[1],
								success: function(data){

								}
							}
						)
					});
				} else {
					swal({
						title: "Canceled",
						text: "Please, choose another crew.<br> Choose Update Ticket Button",
						type: "warning",
						timer: 1500,
						showConfirmButton: false
					});
				}
			}
			);
		});

	});

	// IF HANDOVER TO EXTERNAL SIDE
	function isExternalOrInternal(selectObject) {
		var other_crew = selectObject.value;
		if (other_crew == 13) {
			document.getElementById("type").setAttribute("value", "external");
			$('#addon-external').show();
		} else {
			document.getElementById("type").setAttribute("value", "handover");
			$('#addon-external').hide();
		}
	}

	<!--
	function isNumberKey(evt)
	{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;

		return true;
	}
	//-->
</script>


<!-- Modals Ticket History -->
<div class="modal fade" id="table-history" role="dialog" style="display: none;" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
                	<button type="button" class="close" data-dismiss="modal">&times;</button>
                	<h4 class="modal-title">Ticket's History</h4>
            	</div>
            	<div class="modal-body" id="assigned">
            		<div class="row">
            			<div class="col-md-12">
            					<table class="table table-responsive">
            						<thead>
            							<th>#</th>
            							<th>PIC</th>
            							<th>Start Date</th>
            							<th>Description</th>
            							<th>Finish Date</th>
            							<th>Status</th>
            						</thead>
            						<tbody>
            							@if ($data['history']['count'] == 0)
            								<tr>
            									<td colspan="5">Data is not available.</td>
            								</tr>
            							@else
            								@foreach($data['history']['list'] as $history)
            									<tr class="bg-info">
            										<th>{!! ++$no !!}.</th>
            										<td>{!! $history->namalengkap !!}</td>
            										<td>{!! $history->start_date !!}</td>
            										<td>{!! $history->description !!}</td>
            										<td>{!! $history->finish_date !!}</td>
            										<td>
            											@if ($history->is_active == 0)
            												<span class="label label-danger">Diverted</span>
            											@elseif ($history->is_active == 1 && $history->description == NULL)
            												@if ($data['ticket'][0]->is_external == 1)
            													<span class="label label-warning">On Going by {{$history->external}}</span>
            													<button data-toggle="collapse" data-target="#external-{{$history->ticket_trans_id}}" class="btn btn-link"><i class="fa fa-search"></i></button>
            												@else
            													<span class="label label-warning">On Going</span>
            												@endif
            											@else
            												<span class="label label-success">Finished</span>
            												@if ($data['ticket'][0]->is_external == 1)
            													<button data-toggle="collapse" data-target="#external-{{$history->ticket_trans_id}}" class="btn btn-link"><i class="fa fa-search"></i></button>
            												@endif
            											@endif
            										</td>
            									</tr>
            									@if ($data['ticket'][0]->is_external == 1)
            										@if ($history->ticket_trans_id == $data['trans_external'][0]->trans_id)
            										<tr class="default">
            											<td colspan="6">
            												<div id="external-{{$history->ticket_trans_id}}" class="collapse">
            													<div class="row">
            														<div class="col-md-2"><b>External Side</b>:</div>
            														<div class="col-md-2">{{$data['trans_external'][0]->external_side_name}}</div>
            														<div class="col-md-1"><b>Estimation</b>:</div>
            														<div class="col-md-1">{{$estimation}}</div>
            														<div class="col-md-2"><b>Created at</b>:</div>
            														<div class="col-md-4">{{$data['trans_external'][0]->created_at}}</div>
            													</div>
            													<hr>
            													<div class="row">
            														<div class="col-md-2"><b>Description</b>:</div>
            														<div class="col-md-10">{{$data['trans_external'][0]->description}}</div>
            													</div>
            													<hr>
            													<div class="row">
            														<div class="col-md-2"><b>Finished at</b>:</div>
            														<div class="col-md-4">{{$data['trans_external'][0]->finished_at}} (Approx.)</div>
            														<div class="col-md-6">{!! $status_ext !!}</div>
            													</div>
            												</div>
            											</td>
            										</tr>
            										@endif
            									@endif
            								@endforeach
            							@endif
            						</tbody>
            					</table>
            			</div>
            		</div>
            	</div>
            	<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            	</div>
			</div>
	</div>
</div>
@endsection
