@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Welcome to the IT Support System</center></div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<p>
									<legend>Open a New Ticket</legend>
									<span style="float: left;">Please provide as much detail as possible so we can best assist you.</span>
								</p>
							</div>
							<div class="col-md-6">
								<p>
									<legend>Check Ticket Status</legend>
									<span style="float: right;">We provide archives and history of all your current and past support requests complete with responses.</span>
								</p>
							</div>
						</div>
					</div>
					<ul class="divider"></ul>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<a href="{{url('/ticket/create')}}" class="btn btn-lg btn-success">Open a New Ticket</a>
							</div>
							<div class="col-md-6">
								<a href="#" disabled class="btn btn-lg btn-warning pull-right">Check Ticket Status</a>
							</div>
							
						</div>
					</div>
				</div>
		</div>
	</div>
</div>


<!-- Modals Ticket History -->
<div id="table-users-ticket">
<br>
            				<table class="table table-responsive" id="table-ticket-user">
		 						<thead>
		 							<tr>
		 								<th>#</th>
		 								<th>Ticket ID</th>
		 								<!-- <th>Ticket Type</th> -->
		 								<th>Case Type</th>
		 								<th>Subject</th>
		 								<th>Officer</th>
		 								<th>Status</th>
		 								<th>Last Updated</th>
		 								<th>Tools</th>
		 							</tr>
		 						</thead>
		 						<tbody>
		 						<?php
		 							$no = 0;
		 						?>
		 						@foreach ($data['tickets'] as $ticket)
		 						<?php
		 							$id = explode("#", $ticket->id);
		 							$class = "";
		 							$notes = "";
		 							if ($ticket->description != NULL) {
		 								$class = "bg-danger";
		 								$notes = "<p id='pop-{$id[1]}' data-toggle='tooltip' title='Please close the ticket.'><u><i>Notes</i></u></p>";
		 							} else {
		 								$class = "bg-info";
		 								$notes = "On Progress";
		 							}
		 						?>
		 							<tr class="{{$class}}">
		 								<th>{{++$no}}.</th>
		 								<td><b>{{$ticket->id}}</b></td>
		 								<!-- <td>{{$data['ticket_type'][$ticket->ticket_type]->lookup_desc}}</td> -->
		 								<td>{{$data['case_type'][$ticket->case_type]->lookup_desc}}</td>
		 								<td>{{$ticket->subject}}</td>
		 								<td>{{$ticket->pic}}</td>
		 								<td><span class="{{$arrLabel[$ticket->status]}}">{{$data['ticket_status'][$ticket->status]->lookup_desc}}</span></td>
		 								<td>{{$ticket->updated_at}}</td>
		 								<td><a href="{{ url('/ticket/view') }}/{{$id[1]}}" class="btn btn-link"><i class="fa fa-search"></i></a></td>
		 							</tr>
		 						@endforeach
		 						</tbody>
		 					</table>
</div>
@endsection
