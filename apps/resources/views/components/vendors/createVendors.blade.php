@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Create a New Vendor</center></div>
				<div class="panel-body">
					<legend>Open a New Vendor</legend>
					<div>
					<!-- INSERT DATA -->
					<div style="" id="form-problem">
						<p>Please fill in the form below to create New Vendor</p>

						<!-- TABLE INPUT DATA ASSET -->
						<table class="table table-bordered">
							<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

							<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

							{{ csrf_field()}}

							<!-- VENDOR'S ID -->
							<tr>
								<th>Vendor's ID :</th>
								<td><input class="form-control" type="text" name="vendor[idVendor]" id="idVendor" value="">
								</td>
							</tr>
									
							<!-- BRAND'S NAME -->
							<tr>
								<th>Vendor's Name :</th>
								<td><input class="form-control" type="text" name="vendor[vendorName]" id="vendorName" value="">
								</td>
							</tr>

							<tr>
								<th>Phone Number :</th>
								<td><input class="form-control" type="number" name="vendor[vendorName]" id="vendorPhoneNumber" value="">
								</td>
							</tr>

							<tr>
								<th>Vendor's Adress :</th>
								<td><input class="form-control" type="text" name="vendor[vendorName]" id="vendorAdress" value="">
								</td>
							</tr>

							<!-- DESCRIPTION -->
							<tr>
								<th>Vendor's Description :</th>
								<td>
									<textarea class="form-control input-lg" name="vendor[vendorDescription]" id="vendorDescription" rows="10"></textarea>
								</td>
							</tr>

							<!-- SUBMIT BUTTON -->
							<tr>
								<td colspan="2" align="right">
									<input type="reset" name="reset" value="Reset" class="btn btn-danger">
									<button name="submit" class="btn btn-primary" id="btn-submit-problem">Add Vendor</button>
								</td>
							</tr>
						</table>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<script type="text/javascript">
</script>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#btn-submit-problem').click(function(e){

			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();
			
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('vendorName', ($("#vendorName").val()));
			formData.append('vendorDescription', ($("#vendorDescription").val()));
			formData.append('idVendor', ($("#idVendor").val()));
			formData.append('vendorPhoneNumber', ($("#vendorPhoneNumber").val()));
			formData.append('vendorAdress', ($("#vendorAdress").val()));

			//SEND DATA TO ROUTES
			var url = "{{url('/')}}/asset/vendors/saveVendor";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/asset/vendors";
				}
			});
		});
	});
</script>
<!-- end -->
@endsection
