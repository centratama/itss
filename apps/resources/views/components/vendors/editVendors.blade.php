@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>Edit Data Vendor</center></div>
				<div class="panel-body">
					<legend>Edit Data Vendor</legend>
					<div>
					<!-- EDIT DATA -->
					<div style="" id="form-problem">
						<p>Please fill in the form below to edit data Vendor</p>

						<!-- TABLE INPUT DATA ASSET -->
						<table class="table table-bordered">
							<input type="hidden" name="asset[nik]" value="{{Session::get('logged_in')['nik']}}" id="nik">

							<input type="hidden" name="asset[user_role]" value="{{Session::get('logged_in')['user_role']}}" id="user_role">

							{{ csrf_field()}}

							<input class="form-control" type="text" name="vendor[idVendor]" id="idVendor" value="{{ $data['assetVendor'][0]->id }}" readonly>
									
							<!-- BRAND'S NAME -->
							<tr>
								<th>Vendor's Name :</th>
								<td>
									<input class="form-control" type="text" name="vendor[vendorName]" id="vendorName" value="{{ $data['assetVendor'][0]->vendorName }}">
								</td>
							</tr>

							<tr>
								<th>Phone Number :</th>
								<td><input class="form-control" type="number" name="vendor[vendorName]" id="vendorPhoneNumber" value="{{ $data['assetVendor'][0]->vendorPhoneNumber }}">
								</td>
							</tr>

							<tr>
								<th>Vendor's Adress :</th>
								<td><input class="form-control" type="text" name="vendor[vendorName]" id="vendorAdress" value="{{ $data['assetVendor'][0]->vendorAdress }}">
								</td>
							</tr>

							<!-- BRAND'S NAME -->
							<tr>
								<th>Vendor's Description :</th>
								<td>
									<input class="form-control" type="text" name="vendor[vendorDescription]" id="vendorDescription" value="{{ $data['assetVendor'][0]->vendorDescription }}">
								</td>
							</tr>

							<!-- SUBMIT BUTTON -->
							<tr>
								<td colspan="2" align="right">
									<input type="reset" name="reset" value="Reset" class="btn btn-danger">
									<button name="submit" class="btn btn-primary" id="btn-submit-problem">Edit Vendor</button>
								</td>
							</tr>
						</table>
					</div>
					<!-- END INSERT DATA -->

				</div>
		</div>
	</div>
</div>

<script type="text/javascript">
</script>

<!-- ajax save start -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#btn-submit-problem').click(function(e){

			//$("#form-problem").submit(function(){
			//PREVENT DEFAULT ACTION
			e.preventDefault();
			//GET DATA FROM INPUT FORM
			var formData = new FormData();
			formData.append('vendorName', ($("#vendorName").val()));
			formData.append('vendorDescription', ($("#vendorDescription").val()));
			formData.append('vendorPhoneNumber', ($("#vendorPhoneNumber").val()));
			formData.append('vendorAdress', ($("#vendorAdress").val()));
			
			//SEND DATA TO ROUTES
			var url = "{{url('/asset/vendors/saveEditVendor/'. $idVendor) }}";
			
			$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = "{{url('/')}}/asset/vendors";
				}
			});
		});
	});
</script>
<!-- end -->
@endsection
