@extends('layouts.apps')
@section('content')
<div class="row">
	<div class="col-md-12">
		<span style="float: right;">
			<!-- LINK ADD VENDOR-->
			<a href="{{ url('/') }}/asset/vendors/createVendor" class="btn btn-primary"><i class="fa fa-plus"></i> Add Vendors</a>
		</span>
		<br><br>
	</div>
	<div class="col-md-12 ">
		<div class="panel panel-primary">
			<div class="panel-heading"><center>List Vendors</center></div>
				<div class="panel-body">
					<table class="table table-bordered table-responsive table-striped" id="table-tickets">
						<thead>
							<th>#</th>
							<th>Vendor's ID</th>
							<th>Vendor's Names</th>
							<th>Vendor's Description</th>
							<th>Tools</th>
						</thead>
						@foreach($data['assetVendor'] as $data)
						<tr>
							<td></td>
							<td>{{$data->id}}</td>
							<td>{{$data->vendorName}}</td>
							<td>{{$data->vendorDescription}}</td>
							<td><a href="{{ url('/asset/vendors/editVendor/'.$data->id) }}">EDIT</a>
						</tr>
						@endforeach
					</table>
				</div>
		</div>
	</div>
</div>	
@endsection
