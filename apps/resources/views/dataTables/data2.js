function getBaseURL()
{
	return location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/";
}

function format( d ){
	//'d', is the original data object for the row
	return '<table> </table>' +
					foreach(d.embeded as items){ +


					'<tr>items->idAsset</tr>'+
					'<tr>items->serial_number</tr>'+
					'<tr>items->name</tr>'+
					'<tr>items->brand</tr>'+
				} +
			'</table>';
}

$(document).ready(function() {

	$('#table-assets').DataTable({
		"ServerSide": true,
		"ajax": getBaseURL()+"itss/asset/getList",
		"columns": [
				{ "data" : "idAsset" },
				{ "data" : "name" },
				{ "data" : "serial_number" },
				{ "data" : "brand" },
				{ "data" : "vendor" },
				{ "data" : "tool" },

		]
	});

	$('#table-tickets').DataTable({
		"ServerSide": true,
		"ajax": getBaseURL()+"/ticket/getList",
		"columns": [
				{ "data" : "no" },
				{ "data" : "ticket_id" },
				{ "data" : "case_type" },
				{ "data" : "subject" },
				{ "data" : "status" },
				{ "data" : "pic" },
				{ "data" : "level" },
				{ "data" : "created_at" },
				{ "data" : "view" },
		]
	});

	$('#table-parameters').DataTable({
		"ServerSide": true,
		"ajax": getBaseURL()+"/parameter/getList",
		"columns": [
				{ "data" : "no" },
				{ "data" : "lookup_type" },
				{ "data" : "lookup_value" },
				{ "data" : "lookup_desc" },
				{ "data" : "view" },
		]
	});

	$('#table-employees').DataTable({
		"ServerSide": true,
		"ajax": getBaseURL()+"/employee/getList",
		"columns": [
				{ "data" : "no" },
				{ "data" : "nik" },
				{ "data" : "name" },
				{ "data" : "position" },
				{ "data" : "department" },
				{ "data" : "email" },
				// { "data" : "view" },
		]
	});
});

$(document).ready(function() {

	$('.details-asset').click(function()
	{
		alert('ko');
		/*var tr = $this.closest('tr');
		var row = table.row(tr);

		if(row.child.isShown()){
			//this row already open - colse it
			row.child.hide();
			tr.removeClass('shown');
		} 
		else{
			//open this row
			row.child( format(row.data()) ).shown();
			tr.addClass('shown');
		}*/
	});
});


