function getBaseURL()
{
	return location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/";
}

function format( d ){
	var x = d.embeded;
	for(var key in x ) {
    	if (x.hasOwnProperty(key)) {
	        //alert(x[key].name);
	        return '<table class="table table-bordered table-responsive" border=1 align="center" width=100%>'+
				        '<tr >' +
				        	'<td colspan="3" align="center">Embeded Asset</td>'+
				        '</tr>' +
				        '<tr>' +
				        	'<td align="center">Nama Asset</td>'+
				        	'<td align="center">Serial Number</td>'+
				        	'<td align="center"> tools </td>'+
				        '</tr>' +
				            '<td align="center">'+x[key].name+'</td>'+
				            '<td align="center">'+x[key].serial_number+'</td>'+
				            '<td align="center">'+x[key].tool+'</td>'+
				        '</tr>'+
	    			'</table>';
    	}
	} 
}

$(document).ready(function() {

	var table = $('#table-assets').DataTable({
		//"processing": true,
		"ServerSide": true,
        //"deferLoading": 57,
        "aaSorting": [[6,'desc']],
        "deferRender": true,

		"ajax": getBaseURL()+"asset/getList",
		"columns": [
				{ "data" : "idAsset" },
				{ "data" : "id_parent" },
				{ "data" : "name" },
				{ "data" : "serial_number" },
				{ "data" : "brand" },
				{ "data" : "vendor" },
				{ "data" : "created_at" },
				{ "data" : "tool" },
		]
	});

	$('#table-brands').DataTable({
		//"processing": true,
		"ServerSide": true,
        //"deferLoading": 57,
        "deferRender": true,
		"ajax": getBaseURL()+"asset/brands/getList",
		"columns": [
				{ "data" : "idBrand" },
				{ "data" : "brandName" },
				{ "data" : "brandDesc" },
				{ "data" : "tool" },
		]
	});

	var idAssetHistory = $("#idAssetHistory").val();

	$('#table-history').DataTable({
		//"processing": true,
		"ServerSide": true,
        //"deferLoading": 57,
        "deferRender": true,
		"ajax": getBaseURL()+"transaction/getListHistory/" + idAssetHistory,
		"columns": [
				{ "data" : "number" },
				{ "data" : "nik" },
				{ "data" : "employee" },
				{ "data" : "documentStatus"},
				{ "data" : "assetCondition" },
				{ "data" : "assetLocation" },
				{ "data" : "transactionDate" },
		]
	});

	var idTransaction = $("#idTransaction").val();

	$('#table-transaction').DataTable({
		//"processing": true,
		"ServerSide": true,
        //"deferLoading": 57,
        "deferRender": true,
        "aaSorting": [[6,'desc'], [0,'asc']],
		"ajax": getBaseURL()+"transaction/getList",
		"columns": [
				{ "data" :"id" },
				{ "data" :"nik_requestedby" },
				{ "data" :"nik_undertakenby" },
				{ "data" :"transactionStatus" },
				{ "data" :"documentStatus" },
				{ "data" :"location" },
				{ "data" :"transactiondate"},
				{ "data" :"description" },
				{ "data" :"tools" },
		]
	});

	$('#table-transactionDetail').DataTable({
		//"processing": true,
		"ServerSide": true,
        //"deferLoading": 57,
        "deferRender": true,
		"ajax": getBaseURL()+"transaction/getListDetail/"+idTransaction,
		"columns": [
				{ "data" :"id" },
				{ "data" :"id_asset" },
				{ "data" :"id_parent"},
				{ "data" :"asset_name" },
				{ "data" :"brand_name" },
				{ "data" :"asset_condition" },
				{ "data" :"description" },
				{ "data" :"tools" },
		]
	});

	$('#table-vendors').DataTable({
		//"processing": true,
		"ServerSide": true,
        //"deferLoading": 57,
        "deferRender": true,
		"ajax": getBaseURL()+"asset/vendors/getList",
		"columns": [
				{ "data" : "idVendor" },
				{ "data" : "vendorName" },
				{ "data" : "vendorDesc" },
				{ "data" : "tool" },
		]
	});


	$('#table-tickets').DataTable({
		"ServerSide": true,
		"ajax": getBaseURL()+"/ticket/getList",
		"columns": [
				{ "data" : "no" },
				{ "data" : "ticket_id" },
				{ "data" : "case_type" },
				{ "data" : "subject" },
				{ "data" : "status" },
				{ "data" : "pic" },
				{ "data" : "level" },
				{ "data" : "created_at" },
				{ "data" : "view" },
		]
	});

	$('#table-parameters').DataTable({
		"ServerSide": true,
		"ajax": getBaseURL()+"/parameter/getList",
		"columns": [
				{ "data" : "no" },
				{ "data" : "lookup_type" },
				{ "data" : "lookup_value" },
				{ "data" : "lookup_desc" },
				{ "data" : "view" },
		]
	});

	$('#table-employees').DataTable({
		"ServerSide": true,
		"ajax": getBaseURL()+"/employee/getList",
		"columns": [
				{ "data" : "no" },
				{ "data" : "nik" },
				{ "data" : "name" },
				{ "data" : "position" },
				{ "data" : "department" },
				{ "data" : "email" },
				// { "data" : "view" },
		]
	});

	$('#table-assets tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
        	//alert('ok');
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
        	//alert('ok1');
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
});


