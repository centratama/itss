@extends('layouts.emails')
@section('content')
<h3>Hi {{$data['informer'][0]->fullname}}.</h3>

Ticket ID <b>{{$data['ticket'][0]->id}}</b> has been closed by <b>{{$data['crew'][0]->fullname}}</b>.
<br>
<br>
Here's the detail of your ticket.
<table class="table table-bordered">
	<tr>
		<th>From:</th>
		<td>{{$data['informer'][0]->fullname}} - {{$data['informer'][0]->email}}</td>
	</tr>
</table>
<br>
<blockquote>
	{{$data['ticket'][0]->message}}
	<footer>From {{$data['informer'][0]->fullname}}</footer>
</blockquote>
<br>
<hr>

To view or keep eyes on to the ticket, please <a href="http://itss.centratamagroup.com/login">Login</a> to the ITSS.
<br>
<br>

<i>Information Technology Support System</i>

@endsection
