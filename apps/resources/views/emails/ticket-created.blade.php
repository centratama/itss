@extends('layouts.emails')
@section('content')
<h3>Hi {{$data['toName']}}.</h3>
<br>

New ticket <b>{{$data['ticket_id']}}</b> created.
<br>

<table class="table table-bordered">
	<tr>
		<th>From:</th>
		<td>{{$data['informer']}} - {{$data['informerMail']}}</td>
	</tr>
	<tr>
		<th>Department:</th>
		<td>{{$data['department']}}</td>
	</tr>
</table>
<br>
<blockquote>
	{{$data['message']}}
	<footer>From {{$data['informer']}}</footer>
</blockquote>
<br>
<hr>

To view or keep eyes on to the ticket, please <a href="http://itss.centratamagroup.com/login">Login</a> to the ITSS.
<br>
<br>

<i>Information Technology Support System</i>

@endsection
