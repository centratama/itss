<!doctype html>
<?php $app_name = "ITSS"; $app_version = "1.0";?>
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- <meta charset="utf-8"> -->
        <meta charset="ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>ITSS - Information Technology Support System</title>

        <!-- Fonts -->
        {!! Html::style('assets/fa/css/font-awesome.min.css') !!}
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel="stylesheet" type="text/css">

        {!! Html::style('assets/css/bootstrap.min.css') !!}
        {!! Html::style('assets/swal/dist/sweetalert.css') !!}
        {!! Html::style('assets/dataTables/css/dataTables.bootstrap.min.css') !!}
        
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        {!! Html::script('assets/Highcharts/js/highcharts.js') !!}
        {!! Html::script('assets/Highcharts/js/highcharts-more.js') !!}
        {!! Html::script('assets/Highcharts/js/modules/exporting.js') !!}
        {!! Html::script('assets/js/bootstrap.min.js') !!}
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
        <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
    </head>
    <body id="app-layout">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding image -->
                    <a class="navbar-brand" href="{{url('/')}}">{{$app_name}}</a>

                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left side of navbar -->
                    @if (session()->has('logged_in'))
                        @if (in_array(Session::get('logged_in')['user_role'], array('ADMIN', 'SUPPORT')))
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Settings <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('parameter') }}">Parameters</a></li>
                                <li><a href="{{ url('employee') }}">Employees</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Manage <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{url('/ticket')}}">Troubletickets</a></li>
                                @if(Session::get('logged_in')['user_role'] == "ADMIN" OR Session::get('logged_in')['user_role'] == "SUPPORT")
                                <li><a href="{{url('/transaction')}}"> Transactions </a></li>
                                @endif
                            </ul>
                        </li>

                        @if(Session::get('logged_in')['user_role'] == "ADMIN" OR Session::get('logged_in')['user_role'] == "SUPPORT")
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Master Data <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{url('/asset')}}"> Assets </a></li>
                                <li><a href="{{url('/asset/brands')}}">Brands</a></li>
                                <li><a href="{{url('/asset/vendors')}}">Vendors</a></li>
                                <li><a href="{{url('/asset/status')}}">Transaction Status</a></li>
                                <li><a href="{{url('/asset/types')}}">Asset's Type</a></li>
                                <li><a href="{{url('/asset/condition')}}">Asset's Condition</a></li>
                                <li><a href="{{url('/asset/location')}}">Asset's Location</a></li>
                                <li><a href="{{url('/asset/signReport')}}">Signed By</a></li>
                                
                            </ul>
                        </li>
                        @endif
                    </ul>
                        @else
                    
                        @endif

                    <!-- Right side of navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{Session::get('logged_in')['namalengkap']}} <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#"><i class="fa fa-btn fa-user-circle"></i>My account</a></li>
                                <li><a href="#"><i class="fa fa-btn fa-key"></i>Change password</a></li>
                                <li class="divider"></li>
                                <li><a href="{{url('/logout')}}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    @else
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#" data-toggle="modal" data-target="#register-form">Register</a>
                        </li>
                    </ul>
                    @endif
                </div>
            </div>
        </nav>

        <div class="" style="margin-top:90px">
            <div class="container">
                <center>
                    <h3><i>Information Technology Support System - Centratama Group</i></h3>
                </center>
                <br><br><br>
                
                @yield('content')
            </div>
        </div><br><br><br><br><br>
        
        
    	<!-- JavaScripts -->
        {!! Html::script('assets/swal/dist/sweetalert.min.js') !!}
        {!! Html::script('assets/dataTables/js/dataTables.bootstrap.js') !!}
        {!! Html::script('assets/dataTables/js/jquery.dataTables.min.js') !!}
        {!! Html::script('assets/dataTables/js/dataTables.bootstrap.min.js') !!}
        {!! Html::script('apps/resources/views/dataTables/dataTabless.js') !!}

        <!-- Include this after the sweet alert js file -->
        @include('sweet::alert')
    </body>

    <footer">
        <div class="container">
            <p>
            <span>
                {{$app_name}} {{$app_version}} &copy; <?php echo date('Y')?> <a href="http://www.centratamagroup.com" target="_blank">Centratama Group</a>
            </span>
            <span style="float: right;">
                <i>Information Technology Support System</i>
            </span>
            </p>
        </div>
    </footer>

<!-- form modals register -->
  <!-- Modal -->
    <div class="modal fade" id="register-form" role="dialog" style="display: none;" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            {!! Form::open(array('route' => array('users.register'), 'method' => 'POST', 'id' => 'register-form')) !!}
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Register Form</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nik">NIK</label>
                                <input type="text" name="regis[nik]" class="form-control" id="nik">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" name="regis[username]" class="form-control" id="username">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="passwd">Password</label>
                                <input type="password" name="regis[password]" class="form-control" id="passwd">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="confirm_pass">Confirm Password</label>
                                <input type="password" name="regis[confirm_pass]" class="form-control" id="confirm_pass">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="reset" name="reset" value="Reset" class="btn btn-danger">
                    <!-- <button class=" btn btn-info" id="btn-submit">Kirim</button> -->
                    <input type="submit" name="send" value="Submit" class="btn btn-info" id="btn-submit">
                </div>
            </div>
        {!! Form::close() !!}
        </div>
  </div>
  <script type="text/javascript">
    @if (session()->has('logged_in'))
        var IDLE_TIMEOUT = 60*60*2;
        var _idleSecondsCounter = 0;
        document.onclick = function() {
            _idleSecondsCounter = 0;
        };
        document.onmousemove = function() {
            _idleSecondsCounter = 0;
        };
        document.onkeypress = function() {
            _idleSecondsCounter = 0;
        };
    
        window.setInterval(CheckIdleTime, 1000);
        function CheckIdleTime() {
            _idleSecondsCounter++;
            // var oPanel = document.getElementById("SecondsUntilExpire");
            // if (oPanel)
            //     oPanel.innerHTML = (IDLE_TIMEOUT - _idleSecondsCounter) + "";
            if (_idleSecondsCounter >= IDLE_TIMEOUT) {
                swal({
                    title: "Session expired!",
                    text: "Your session has expired",
                    type: "error",
                    button: true,
                },
                function (isConfirm){
                    window.location.href = "{{url('/')}}/logout";
                }
                );
            }
        }
    @else
        setTimeout(function(){
            window.location.href = "{{url('/')}}";
        }, 1000*60);
    @endif
  // @if (session()->has('logged_in'))
  //   setTimeout(function(){
  //       window.location.href = "{{url('/')}}/logout";
  //   }, 1000*60*60*2);
  // @endif

    $(document).ready(function(){
    $('#btn-submit').click(function(e){
        e.preventDefault();

            var $post = {};
            $post.nik = $("#nik").val();
            $post.username = $("#username").val();
            $post.password = $("#passwd").val();
            $post.confirm_pass = $("#confirm_pass").val();
            $post._token = $('input[name="_token"]').val();
            var url = "{{ url('/') }}/action-register";
            console.log(url);
            $.ajax({
                type: "POST",
                url: url,
                data: $post,
                cache: false,
                success: function(data) {
                        window.location.href=window.location.href;
                    }
                }
            );
    });
});
</script>

</div>
</html>
