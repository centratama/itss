<!doctype html>
<?php $app_name = "ITSS"; $app_version = "1.0";?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40" lang="{{ app()->getLocale() }}">
    <body id="app-layout">
        <div class="" style="margin-top:0px">
            <div class="container">
                @yield('content')
            </div>
        </div>
        
    	<!-- JavaScripts -->
        {!! Html::script('assets/swal/dist/sweetalert.min.js') !!}
        {!! Html::script('assets/dataTables/js/dataTables.bootstrap.js') !!}
        {!! Html::script('assets/dataTables/js/jquery.dataTables.min.js') !!}
        {!! Html::script('assets/dataTables/js/dataTables.bootstrap.min.js') !!}
        {!! Html::script('apps/resources/views/dataTables/dataTables.js') !!}

        <!-- Include this after the sweet alert js file -->
        @include('sweet::alert')
    </body>

<!-- form modals register -->
  <!-- Modal -->
    <div class="modal fade" id="register-form" role="dialog" style="display: none;" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            {!! Form::open(array('route' => array('users.register'), 'method' => 'POST', 'id' => 'register-form')) !!}
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Register Form</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nik">NIK</label>
                                <input type="text" name="regis[nik]" class="form-control" id="nik">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" name="regis[username]" class="form-control" id="username">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="passwd">Password</label>
                                <input type="password" name="regis[password]" class="form-control" id="passwd">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="confirm_pass">Confirm Password</label>
                                <input type="password" name="regis[confirm_pass]" class="form-control" id="confirm_pass">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="reset" name="reset" value="Reset" class="btn btn-danger">
                    <!-- <button class=" btn btn-info" id="btn-submit">Kirim</button> -->
                    <input type="submit" name="send" value="Submit" class="btn btn-info" id="btn-submit">
                </div>
            </div>
        {!! Form::close() !!}
        </div>
  </div>
  <script type="text/javascript">
    @if (session()->has('logged_in'))
        var IDLE_TIMEOUT = 60*60*2;
        var _idleSecondsCounter = 0;
        document.onclick = function() {
            _idleSecondsCounter = 0;
        };
        document.onmousemove = function() {
            _idleSecondsCounter = 0;
        };
        document.onkeypress = function() {
            _idleSecondsCounter = 0;
        };
    
        window.setInterval(CheckIdleTime, 1000);
        function CheckIdleTime() {
            _idleSecondsCounter++;
            // var oPanel = document.getElementById("SecondsUntilExpire");
            // if (oPanel)
            //     oPanel.innerHTML = (IDLE_TIMEOUT - _idleSecondsCounter) + "";
            if (_idleSecondsCounter >= IDLE_TIMEOUT) {
                swal({
                    title: "Session expired!",
                    text: "Your session has expired",
                    type: "error",
                    button: true,
                },
                function (isConfirm){
                    window.location.href = "{{url('/')}}/logout";
                }
                );
            }
        }
    @else
        setTimeout(function(){
            window.location.href = "{{url('/')}}";
        }, 1000*60);
    @endif
  // @if (session()->has('logged_in'))
  //   setTimeout(function(){
  //       window.location.href = "{{url('/')}}/logout";
  //   }, 1000*60*60*2);
  // @endif

    $(document).ready(function(){
    $('#btn-submit').click(function(e){
        e.preventDefault();

            var $post = {};
            $post.nik = $("#nik").val();
            $post.username = $("#username").val();
            $post.password = $("#passwd").val();
            $post.confirm_pass = $("#confirm_pass").val();
            $post._token = $('input[name="_token"]').val();
            var url = "{{ url('/') }}/action-register";
            console.log(url);
            $.ajax({
                type: "POST",
                url: url,
                data: $post,
                cache: false,
                success: function(data) {
                        window.location.href=window.location.href;
                    }
                }
            );
    });
});
</script>

</div>
</html>
