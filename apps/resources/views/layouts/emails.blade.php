<html>
	<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        {!! Html::style('assets/fa/css/font-awesome.min.css') !!}
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel="stylesheet" type="text/css">

        {!! Html::style('assets/css/bootstrap.min.css') !!}
        {!! Html::style('assets/swal/dist/sweetalert.css') !!}
        {!! Html::style('assets/dataTables/css/dataTables.bootstrap.min.css') !!}
        {!! Html::script('assets/jquery.min.js') !!}
        {!! Html::script('assets/Highcharts/js/highcharts.js') !!}
        {!! Html::script('assets/Highcharts/js/highcharts-more.js') !!}
        {!! Html::script('assets/Highcharts/js/modules/exporting.js') !!}
        {!! Html::script('assets/js/bootstrap.min.js') !!}
        <style>
        body {
            font-family: 'Lato';
        	}

        	.fa-btn {
            	margin-right: 6px;
        	}
    	</style>

    	<body id="app-layout">
    		@yield('content')
    	</body>
	</head>
</html>