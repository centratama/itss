<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/login', function () {
	return view('components.login');
});

// Register
Route::post('/action-register', array('as' => 'users.register', 'uses' => 'UsersController@actionRegister'));

// Login
Route::post('/action-login', 'UsersController@doLogin');
// logout
Route::get('/logout', 'UsersController@doLogout');


//ROUTE ASSETS
Route::group(['prefix'=>'/asset'], function()
{
	//Route::get('dropdown',array('as'=>'myform','uses'=>'HomeController@myform'));
	Route::get('/getStatus/{id}',['uses'=>'AssetsController@depedency']);
	Route::get('/getAssetFromType/{idType}',['uses'=>'AssetsController@depedencyCreateAsset']);

	//view all assets and detail asset
	Route::get('/', 'AssetsController@viewAssets');
	Route::get('/getList', 'AssetsController@getListJson')->name('ajax.dataTable');
	Route::get('/detailAsset/{id}', ['uses'=>'AssetsController@detailAsset']);

	//viewing history asset's transaction
	Route::get('/transactionHistory/{id}', ['uses'=>'AssetsController@viewTransactionHistory']);

	//viewing berlac asset pdf
	Route::get('/berlacAsset/{fileName}', ['uses'=>'AssetsController@viewBerlacFile']);

	//creating and saving assets
	Route::get('/createAssets', 'AssetsController@createAssets');
	Route::post('/saveAssets', 'AssetsController@saveAssets');

	//editing and saving assets
	Route::get('/editAsset/{id}', ['uses'=>'AssetsController@editAsset']);
	Route::post('/saveEditAssets', 'AssetsController@saveEditAsset');

	//adding and saving transaction data of an asset
	Route::get('/addTransaction/{idAsset}', ['uses'=>'AssetsController@addTransaction1']);
	Route::post('/saveAddTransaction', 'TransactionController@saveAddTransaction');

	//deleting embeded asset
	Route::get('/child/deleteAssetChild/{id}', ['uses'=>'AssetsController@deleteAssetChild']);

	//MASTER DATA ASSET'S CONDITION
	Route::group(['prefix'=>'/condition'], function()
	{
		Route::get('/', 'AssetConditionController@viewCondition');
		Route::get('/editCondition/{id}', ['uses'=>'AssetConditionController@editCondition']);
		Route::get('/createCondition', 'AssetConditionController@createCondition');
		Route::post('/saveCondition', 'AssetConditionController@saveCondition');
		Route::post('/saveEditCondition/{id}', ['uses'=>'AssetConditionController@saveEditCondition']);
	});

	//MASTER DATA ASSET'S BRANDS
	Route::group(['prefix'=>'/brands'], function()
	{
		Route::get('/', 'AssetBrandController@viewBrands');
		Route::get('/editBrand/{id}', ['uses'=>'AssetBrandController@editBrand']);
		Route::get('/createBrand', 'AssetBrandController@createBrand');
		Route::post('/saveBrand', 'AssetBrandController@saveBrand');
		Route::post('/saveEditBrand/{id}', ['uses'=>'AssetBrandController@saveEditBrand']);
		Route::get('/getList', 'AssetBrandController@getListJson')->name('ajax.dataTable');
		Route::get('/asset/{id}', ['uses'=>'AssetBrandController@thisAsset']);
	});

	//MASTER DATA ASSET'S VENDORS
	Route::group(['prefix'=>'/vendors'], function()
	{
		//ROUTE VENDORS
		Route::get('/', 'AssetVendorController@viewVendors');
		Route::get('/editVendor/{id}', ['uses'=>'AssetVendorController@editVendor']);
		Route::get('/createVendor', 'AssetVendorController@createVendor');
		Route::post('/saveVendor', 'AssetVendorController@saveVendor');
		Route::post('/saveEditVendor/{id}', ['uses'=>'AssetVendorController@saveEditVendor']);
		Route::get('/getList', 'AssetVendorController@getListJson')->name('ajax.dataTable');
		Route::get('/asset/{id}', ['uses'=>'AssetBrandController@thisAsset']);
	});

	//MASTER DATA ASSET'S STATUS
	Route::group(['prefix'=>'/status'], function()
	{
		Route::get('/', 'AssetStatusController@viewStatus');
		Route::get('/editStatus/{id}', ['uses'=>'AssetStatusController@editStatus']);
		Route::get('/createStatus', 'AssetStatusController@createStatus');
		Route::post('/saveStatus', 'AssetStatusController@saveStatus');
		Route::post('/saveEditStatus/{id}', ['uses'=>'AssetStatusController@saveEditStatus']);
	});

	//MASTER DATA ASSET'S TYPES
	Route::group(['prefix'=>'/types'], function()
	{
		Route::get('/', 'AssetTypeController@viewType');
		Route::get('/editType/{id}', ['uses'=>'AssetTypeController@editType']);
		Route::get('/createType', 'AssetTypeController@createType');
		Route::post('/saveType', 'AssetTypeController@saveType');
		Route::post('/saveEditType/{id}', ['uses'=>'AssetTypeController@saveEditType']);
	});

	//MASTER DATA ASSET'S LOCATION
	Route::group(['prefix'=>'/location'], function()
	{
		Route::get('/', 'AssetLocationController@viewLocation');
		Route::get('/editLocation/{id}', ['uses'=>'AssetLocationController@editLocation']);
		Route::get('/createLocation', 'AssetLocationController@createLocation');
		Route::post('/saveLocation', 'AssetLocationController@saveLocation');
		Route::post('/saveEditLocation/{id}', ['uses'=>'AssetLocationController@saveEditLocation']);
	});
});

//ROUTE TRANSACTION
Route::group(['prefix'=>'/transaction'], function()
{
	Route::get('/getList', 'TransactionController@getListJson')->name('ajax.dataTable');
	Route::get('/getListDetail/{idTransaction}', ['uses' =>'TransactionController@getListJsonDetail'])->name('ajax.dataTable');
	Route::get('/getListHistory/{idAsset}', ['uses' =>'TransactionController@getListJsonHistory'])->name('ajax.dataTable');

	//Route::get('/', 'TransactionController@viewTransaction');
	Route::get('/', 'TransactionController@viewTransaction1');
	Route::get('/addTransaction/{idTransaction}/{nik}', ['uses'=>'TransactionController@addTransactionEmployee']);
	Route::post('/saveAddTransaction/', 'TransactionController@saveAddTransactionEmployee');


	//creating and saving new transaction for employee
	Route::get('/createTransaction', 'TransactionController@createTransaction');
	Route::post('/saveTransaction', 'TransactionController@saveTransaction');

	Route::get('/createTransaction1', 'TransactionController@createTransaction1');
	Route::get('/editTransaction1/{idTransaction}', ['uses'=>'TransactionController@editTransaction1']);
	Route::get('/closeTransaction1/{idTransaction}', ['uses'=>'TransactionController@closeTransaction1']);
	Route::get('/cancelTransaction1/{idTransaction}', ['uses'=>'TransactionController@cancelTransaction1']);
	Route::get('/detailTransaction/{idTransaction}', ['uses'=>'TransactionController@detailTransaction']);
	Route::get('/returnTransaction/{idTransaction}', ['uses'=>'TransactionController@returnTransaction']);

	//transaction detail
	Route::get('/createTransactionDetail/{idTransaction}', ['uses'=>'TransactionController@createTransactionDetail']);
	Route::get('/editTransactionDetail/{idTransaction}/{idTransactionHeader}', ['uses'=>'TransactionController@editTransactionDetail']);
	Route::get('/deleteTransactionDetail/{idTransaction}', ['uses'=>'TransactionController@deleteTransactionDetail']);
	Route::post('/saveTransactionDetail', 'TransactionController@saveTransactionDetail');
	Route::post('/saveEditTransactionDetail', 'TransactionController@saveEditTransactionDetail');

	Route::post('/saveTransaction1', 'TransactionController@saveTransaction1');
	Route::post('/saveEditTransaction1', 'TransactionController@saveEditTransaction1');

	//editing and saving a transaction data
	Route::get('/editTransaction/{idTransaction}', ['uses'=>'TransactionController@editTransaction']);
	Route::post('/saveEditTransaction', 'TransactionController@saveEditTransaction');

	Route::get('/editTransaction2/{idTransaction}', ['uses'=>'TransactionController@editTransaction2']);
	Route::post('/returningAsset','TransactionController@returningAsset');

	//reporting a transaction data
	Route::get('/reportAssets', 'TransactionController@reportAssets');

	//view printing preview of a transaction data
	Route::get('/transactionReport/{idTransaction}', ['uses'=>'TransactionController@printTransactionReport']);

	//downloading a transaction data formating in PDF
	Route::get('/downloadTransactionPDF/{id}', ['uses'=>'TransactionController@downloadPDF']);
});

Route::get('/asset/signReport', 'AssetsController@signReport');
Route::post('/asset/signReport', 'AssetsController@saveSignReport');

// ticket
Route::get('/ticket', function(){
	if (Session()->has('logged_in')) {
		return view('components.tickets.list-ticket');
	} else {
		return Redirect::to('/login');
	}
});
//Route::get('/asset/addTransaction/{id}', ['uses'=>'AssetsController@addTransaction']);

// List JSON Ticket
Route::get('/ticket/getList', 'TicketsController@getListJson');
// Create new ticket
Route::get('/ticket/create', 'TicketsController@create');
Route::post('/ticket/save', 'TicketsController@store');
// view ticket
Route::get('/ticket/view/{id}', 'TicketsController@view');
// update ticket
Route::post('/ticket/update/{id}', 'TicketsController@update');

// Reset Password
Route::get('/password/reset', function(){
	return view('components.reset-password');
});
Route::post('/password/reset', 'UsersController@resetPassword');

// Parameter
Route::get('/parameter', function(){
	if (Session()->has('logged_in')) {
		return view('components.parameters.list-param');
	} else {
		return Redirect::to('/login');
	}
});
// List JSON Parameter
Route::get('/parameter/getList', 'SysLookupValController@getListJson');

// Employee
Route::get('/employee', function(){
	if (Session()->has('logged_in')) {
		return view('components.employees.list-emplo');
	} else {
		return Redirect::to('/login');
	}
});
// List JSON Parameter
Route::get('/employee/getList', 'EmployeesController@getListJson');

// mail
Route::get('/sendmail', function (){
		Mail::send('emails.ticket-created', ['name' => "EE"], function($m){
            $m->to('amar.fadhillah@centratamagroup.com', 'Amar')->subject('Subjet of the email');
        });
	});
